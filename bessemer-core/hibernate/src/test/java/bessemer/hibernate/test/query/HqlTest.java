package bessemer.hibernate.test.query;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.hibernate.hql.HqlExpressionResult;
import bessemer.hibernate.query.StructuredQuery;
import bessemer.hibernate.hql.HqlUtils;
import bessemer.hibernate.hql._Hql;
import bessemer.cornerstone.collection._Maps;
import bessemer.cornerstone.expression._Expressions;
import bessemer.hibernate.hql.HqlUtils;
import bessemer.hibernate.hql._Hql;

public class HqlTest {
    interface TestEntity {

    }

    @Test
    public void testBuildQuery() {
        StructuredQuery<TestEntity> query = _Hql.selectFrom(TestEntity.class, "entity")
                .where(_Expressions.equals("entity.value", "Test Values"));

        HqlExpressionResult result = HqlUtils.resolveQuery(query);

        Assertions.assertEquals("select entity from alloy.hibernate.test.query.HqlTest$TestEntity as entity where (entity.value = :param_0)", result.getExpressionString());
        Assertions.assertEquals(_Maps.of("param_0", "Test Values"), result.getParameters());
    }
}
