package bessemer.hibernate.repository

import bessemer.cornerstone.data.Lexicon
import bessemer.cornerstone.data.PagingDataSet
import bessemer.cornerstone.data.PagingLexicon
import bessemer.cornerstone.data.pagination.PageData
import bessemer.cornerstone.data.pagination.PageRequest
import bessemer.cornerstone.data.pagination.Pagination
import bessemer.cornerstone.expression._Expressions
import bessemer.cornerstone.memento.MementoMetadata
import bessemer.cornerstone.repository.Repository
import bessemer.hibernate.hql._Hql
import bessemer.hibernate.query.StructuredQuery
import bessemer.hibernate.util.HibernateUtils
import java.util.stream.Stream
import javax.persistence.EntityManager
import kotlin.reflect.KProperty1

// TODO specify pagination size
open class HibernateRepository<T: Any, KEY: Any>: Repository<T, KEY> {
    private val elementType: Class<T>
    private val keyType: Class<KEY>
    private val entityManager: EntityManager
    private val primaryKey: KProperty1<T, *>
    private val primaryLexicon: PagingLexicon<KEY, T>
    private val mementoMetadata: MementoMetadata<T, KEY>

    constructor(elementType: Class<T>,
                keyType: Class<KEY>,
                entityManager: EntityManager) {

        this.elementType = elementType
        this.keyType = keyType
        this.entityManager = entityManager
        this.primaryKey = HibernateUtils.getPrimaryKey(elementType)!!

        print("Found primary key $primaryKey")

        val structuredQuery = _Hql.selectFrom(elementType);

        mementoMetadata = MementoMetadata(
                mementizer = {
                    val memento = primaryKey.get(it)
                    @Suppress("UNCHECKED_CAST")
                    memento as KEY
                },
                elementType = elementType,
                mementoType = keyType,
                mementoField = _Expressions.field(structuredQuery.alias + '.' + primaryKey.name)
        )

        primaryLexicon = _Hql.buildLexicon(
                entityManager,
                _Hql.selectFrom(elementType),
                mementoMetadata
        )
    }

    override fun findEach(keys: Stream<KEY>): Stream<Pair<KEY, T>> = primaryLexicon.findEach(keys)

    override fun findAll(): PagingLexicon<KEY, T> = primaryLexicon

    override fun <S : T> saveStream(entities: Stream<S>): Stream<S> {
        return entities.map { entityManager.merge(it) }
    }

    override fun delete(entities: Iterable<T>) {
        entities.map { entityManager.remove(it) }
    }

    override fun remove(keys: Stream<KEY>) {
        val queryString = "delete * from ${elementType.name} entity where ${mementoMetadata.mementoField.field} in :keys"
        val query = entityManager.createQuery(queryString)
        query.setParameter("keys", keys)
        query.executeUpdate()
    }

    override fun removeAll() {
        val queryString = "delete * from ${elementType.name} entity"
        val query = entityManager.createQuery(queryString)
        query.executeUpdate()
    }
}