package bessemer.hibernate.repository

import bessemer.cornerstone.memento.Identifiable
import javax.persistence.EntityManager

interface Order: Identifiable {
    val id: Long
}

//class OrderRepository(entityManager: EntityManager) :
//        HibernateRepository<Long, Order>(
//                Order::class.java,
//                Long::class.java,
//                entityManager)

data class ExternalIdentifier(val externalSource: String, val externalId: String)

//interface GloballyIdentifiableRepository<T: Identifiable>: IdentifiableRepository<T> {
//    fun byExternalId(): PagingDao<ExternalIdentifier, T>
//}
//
//class SimpleOrderRepository: RepositoryIndex<Long, Order>, RepositoryIndexAdapter<Long, Order> {
//    override val context: QueryContext<Order>
//        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
//
//    override val mementoMetadata: MementoMetadata<Order, Long>
//        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
//
//    override fun query(query: HqlQuery<Order>): Lexicon<Long, Order> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun <K : Any> query(mementoMetadata: MementoMetadata<Order, K>): Lexicon<K, Order> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//}
//
//class PlatformOrderRepository: IdentifiableRepository<Order> {
//    override fun pageLexicon(): Lexicon<PageRequest, PageData<Order>> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun entries(): DataSet<Pair<Long, Order>> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun findEach(mementos: Stream<Long>): Stream<Pair<Long, Order>> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun query(query: HqlQuery<Order>): PagingLexicon<Long, Order> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun <K : Any> query(mementoMetadata: MementoMetadata<Order, K>): Lexicon<Long, Order> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun removeAll(keys: Stream<Long>) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun putAll(elements: Stream<Pair<Long, Order>>) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//}

//class PlatformOrderDaoImpl : PlatformOrderRepository, DelegatingDao<Order, Long> {
//    lateinit var entityManager: EntityManager
//    override lateinit var delegate: Repository<Order, Long>
//
//    @PostConstruct
//    fun init() {
//        delegate = _Queries.buildDao(entityManager, Order::class)
//    }
//}

//lateinit var orderDao: PlatformOrderRepository