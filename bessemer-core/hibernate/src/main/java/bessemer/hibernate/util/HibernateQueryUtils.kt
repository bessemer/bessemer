package bessemer.hibernate.util

import bessemer.hibernate.hql.HqlExpressionResolver
import bessemer.hibernate.query.StructuredQuery
import bessemer.hibernate.hql.HqlUtils
import bessemer.cornerstone.collection._Iterables
import bessemer.cornerstone.util._Ranges
import bessemer.cornerstone.util.ifPresent
import com.google.common.collect.BoundType
import org.apache.commons.lang3.StringUtils
import org.hibernate.jpa.QueryHints
import javax.persistence.EntityManager
import javax.persistence.TypedQuery

object HibernateQueryUtils {
    private const val CACHEABLE_KEY = "org.hibernate.cacheable"

    @JvmStatic
    fun <T> getResults(query: TypedQuery<T>): List<T> {
        return query.resultList
    }

    @JvmStatic
    fun <T> getResult(query: TypedQuery<T>): T? = _Iterables.only(query.resultList)

    fun <T> buildPlatformQuery(query: StructuredQuery<T>, entityManager: EntityManager, resolver: HqlExpressionResolver = HqlExpressionResolver()): TypedQuery<T> {
        val queryResult = HqlUtils.resolveQuery(query, resolver)
        StructuredQuery.logger.trace { "Built hql query string: ${queryResult.expressionString}" }

        val platformQuery = entityManager.createQuery(queryResult.expressionString, query.select.type)
        for (param in queryResult.parameters.entries) {
            platformQuery.setParameter(param.key, param.value)
        }

        if(query.cacheRegion.isPresent) {
            cache(platformQuery, query.cacheRegion.value)
        }

        _Ranges.size(query.bounds).ifPresent { platformQuery.maxResults = it }

        if(!query.bounds.contains(0)) {
            if(query.bounds.lowerBoundType() == BoundType.OPEN) {
                platformQuery.firstResult = query.bounds.lowerEndpoint() + 1
            }
            else {
                platformQuery.firstResult = query.bounds.lowerEndpoint()
            }
        }

        return platformQuery
    }

    @JvmStatic
    fun cache(query: TypedQuery<*>) {
        query.setHint(CACHEABLE_KEY, true)
    }

    @JvmStatic
    fun cache(query: TypedQuery<*>, cacheRegion: String?) {
        query.setHint(CACHEABLE_KEY, true)

        if (StringUtils.isNotEmpty(cacheRegion)) {
            query.setHint(QueryHints.HINT_CACHE_REGION, cacheRegion)
        }
    }
}