package bessemer.hibernate.util

import org.hibernate.CacheMode
import org.hibernate.CacheMode.REFRESH
import org.hibernate.FlushMode
import org.hibernate.Session
import javax.persistence.CacheRetrieveMode.BYPASS
import javax.persistence.CacheRetrieveMode.USE
import javax.persistence.CacheStoreMode
import javax.persistence.EntityManager
import javax.persistence.Id
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

object HibernateUtils {
    @JvmStatic
    fun setCacheMode(entityManager: EntityManager, cacheMode: CacheMode) {
        val session = entityManager.unwrap(Session::class.java)
        session.cacheMode = cacheMode
        if (cacheMode.isGetEnabled) {
            entityManager.setProperty("javax.persistence.cache.retrieveMode", USE)
        } else {
            entityManager.setProperty("javax.persistence.cache.retrieveMode", BYPASS)
        }
        if (cacheMode.isPutEnabled) {
            if (REFRESH == cacheMode) {
                entityManager.setProperty("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH)
            } else {
                entityManager.setProperty("javax.persistence.cache.storeMode", CacheStoreMode.USE)
            }
        } else {
            entityManager.setProperty("javax.persistence.cache.storeMode", CacheStoreMode.BYPASS)
        }
    }

    @JvmStatic
    fun setFlushMode(entityManager: EntityManager, flushMode: FlushMode?) {
        val session = entityManager.unwrap(Session::class.java)
        session.hibernateFlushMode = flushMode
    }

    @JvmStatic
    fun <T: Any> getPrimaryKey(clazz: Class<T>): KProperty1<T, *>? {
        val kclass: KClass<T> = clazz.kotlin
        val property = kclass.memberProperties.find { property ->
            property.annotations.find { it is Id } != null
        }

        if(property != null) {
            property.isAccessible = true
        }

        return property
    }
}