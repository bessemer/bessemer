package bessemer.hibernate.hql

import bessemer.hibernate.query.QueryFragment
import bessemer.hibernate.query.QueryIndexContext
import bessemer.hibernate.query.QueryJoin
import bessemer.hibernate.query.QueryLexicon
import bessemer.hibernate.query.QueryResolver
import bessemer.hibernate.query.QuerySelect
import bessemer.hibernate.query.StructuredQuery
import bessemer.cornerstone.data.pagination.KeySetPaginationStrategy
import bessemer.cornerstone.data.pagination.Pagination
import bessemer.cornerstone.expression.ExpressionField
import bessemer.cornerstone.memento.MementoMetadata
import bessemer.cornerstone.repository.RepositoryOperations
import bessemer.hibernate.util.HibernateQueryUtils
import bessemer.cornerstone.function._Functions
import bessemer.cornerstone.memento.Identifiable
import javax.persistence.EntityManager
import kotlin.reflect.KClass

/**
 * Created by jlutteringer on 5/2/18.
 */
object _Hql {
    fun <T: Any> select(type: KClass<T>, alias: String, expression: String = alias) =
            select(type.java, alias, expression)

    @JvmStatic
    fun <T> select(type: Class<T>, alias: String, expression: String = alias) =
            StructuredQuery(alias = alias, select = QuerySelect(type, expression))

    fun <T: Any> selectFrom(type: KClass<T>, alias: String = "entity") = selectFrom(type.java, alias)

    @JvmStatic
    fun <T> selectFrom(type: Class<T>, alias: String = "entity") =
            StructuredQuery(alias = alias, select = QuerySelect(type, alias))
                    .join(type, alias).simple()

    @JvmStatic
    fun fragment(type: Class<*>, alias: String) = QueryFragment().join(QueryJoin(expression = type.name, alias = alias))

    @JvmStatic
    fun <T> getResults(query: StructuredQuery<T>, entityManager: EntityManager) = HibernateQueryUtils.getResults(HibernateQueryUtils.buildPlatformQuery(query, entityManager))

    @JvmStatic
    fun <T> getResult(query: StructuredQuery<T>, entityManager: EntityManager) = HibernateQueryUtils.getResult(HibernateQueryUtils.buildPlatformQuery(query, entityManager))

    @JvmStatic
    fun <T: Identifiable> buildLexicon(entityManager: EntityManager, query: StructuredQuery<T>, mementoField: ExpressionField, pageSize: Int = Pagination.DEFAULT_PAGE_SIZE) =
            buildLexicon(
                    context = QueryIndexContext(query, buildResolver(entityManager), MementoMetadata.of(query.select.type, mementoField)),
                    pageSize = pageSize
            )

    @JvmStatic
    fun <N: Any, T: Any> buildLexicon(entityManager: EntityManager, query: StructuredQuery<T>, mementoMetadata: MementoMetadata<T, N>, pageSize: Int = Pagination.DEFAULT_PAGE_SIZE) =
            buildLexicon(
                    context = QueryIndexContext(query, buildResolver(entityManager), mementoMetadata),
                    pageSize = pageSize
            )

    // TODO right now, the MementoMetadata.mementoField used for pagination is absolute - meaning it must match the alias of that table in the query itself... there should probably be a better way of handling this:
    // ExpressionField likely needs to be a more sophisticated object which supports some kind of relative field naming that can be resolved in tandem with a given query...
    fun <N: Any, T: Any> buildLexicon(context: QueryIndexContext<N, T>, pageSize: Int = Pagination.DEFAULT_PAGE_SIZE): QueryLexicon<N, T> =
            QueryLexicon(
                    context = context,
                    keyPaginationStrategy = KeySetPaginationStrategy(
                            pageSize = pageSize,
                            keyMetadata = MementoMetadata(_Functions::identity, context.mementoMetadata.mementoType, context.mementoMetadata.mementoType, context.mementoMetadata.mementoField)
                    ),
                    valuePaginationStrategy = KeySetPaginationStrategy(
                            pageSize = pageSize,
                            keyMetadata = context.mementoMetadata
                    )
            )

    fun <T: bessemer.cornerstone.memento.Identifiable> buildDao(entityManager: EntityManager, clazz: KClass<T>) = buildDao(entityManager, clazz.java)

    fun <T: bessemer.cornerstone.memento.Identifiable> buildDao(entityManager: EntityManager, clazz: Class<T>): RepositoryOperations<T, Long> {
        TODO()
    }

    fun buildResolver(entityManager: EntityManager): QueryResolver {
        return object : QueryResolver {
            override fun <T> resolve(query: StructuredQuery<T>): List<T> = getResults(query, entityManager)
        }
    }
}