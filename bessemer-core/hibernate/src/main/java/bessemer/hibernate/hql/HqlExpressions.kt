package bessemer.hibernate.hql

import bessemer.hibernate.query.StructuredQuery
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.expression.CustomExpression
import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression.ExpressionContext
import bessemer.cornerstone.expression.ExpressionField
import bessemer.cornerstone.expression.ExpressionValue
import bessemer.cornerstone.expression.OperationExpression
import bessemer.cornerstone.expression.OperationType
import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import java.util.function.Function
import java.util.stream.Collectors

class HqlExpressionResolver {
    var fieldResolver: Function<ExpressionField, String> = Function { it.field.toString() }
}

class HqlExpressionState {
    var parameterIndex = 0
    val parameters: BiMap<String, Any> = HashBiMap.create()
}

class HqlExpressionContext(
        val resolver: HqlExpressionResolver = HqlExpressionResolver(),
        val state: HqlExpressionState = HqlExpressionState()): ExpressionContext {
}

data class HqlExpressionResult(val expressionString: String, val parameters: Map<String, Any>);

object HqlExpressions {
    fun subquery(query: StructuredQuery<Any>) = SubqueryExpression(query)

    fun resolveExpression(expression: Expression, resolver: HqlExpressionResolver = HqlExpressionResolver()): HqlExpressionResult {
        val context = HqlExpressionContext(resolver = resolver)
        val expressionString = resolveExpression(expression, context)
        return HqlExpressionResult(expressionString = expressionString, parameters = _Maps.copy(context.state.parameters))
    }

    fun resolveExpression(expression: Expression, context: HqlExpressionContext): String {
        if (expression is OperationExpression) {
            return resolveExpression(expression, context)
        }

        if (expression is ExpressionField) {
            return resolveExpression(expression, context)
        }

        if (expression is ExpressionValue) {
            return resolveExpression(expression, context)
        }

        throw IllegalArgumentException("Unexpected expression type: " + expression.javaClass)
    }

    private fun resolveExpression(expression: OperationExpression, context: HqlExpressionContext): String {
        if (OperationType.AND == expression.type) {
            return "(" + expression.arguments.stream().map{ exp -> resolveExpression(exp, context) }.collect(Collectors.joining(" AND ")) + ")"
        }

        if (OperationType.OR == expression.type) {
            return "(" + expression.arguments.stream().map{ exp -> resolveExpression(exp, context) }.collect(Collectors.joining(" OR ")) + ")"
        }

        if (OperationType.NOT == expression.type) {
            return "NOT" + resolveExpression(expression.arguments[0], context) + ""
        }

        if (OperationType.EQUALS == expression.type) {
            return if (expression.arguments[1] is ExpressionValue && (expression.arguments[1] as ExpressionValue).value == null) {
                "(" + resolveExpression(expression.arguments[0], context) + " IS NULL)"
            } else {
                "(" + resolveExpression(expression.arguments[0], context) + " = " + resolveExpression(expression.arguments[1], context) + ")"
            }
        }

        if (OperationType.LESS_THAN == expression.type) {
            return "(" + resolveExpression(expression.arguments[0], context) + " < " + resolveExpression(expression.arguments[1], context) + ")"
        }

        if (OperationType.GREATER_THAN == expression.type) {
            return "(" + resolveExpression(expression.arguments[0], context) + " > " + resolveExpression(expression.arguments[1], context) + ")"
        }

        if (OperationType.LESS_THAN_EQUALS == expression.type) {
            return "(" + resolveExpression(expression.arguments[0], context) + " <= " + resolveExpression(expression.arguments[1], context) + ")"
        }

        if (OperationType.GREATER_THAN_EQUALS == expression.type) {
            return "(" + resolveExpression(expression.arguments[0], context) + " >= " + resolveExpression(expression.arguments[1], context) + ")"
        }

        if (OperationType.CONTAINS == expression.type) {
            return "(" + resolveExpression(expression.arguments[0], context) + " IN (" + resolveExpression(expression.arguments[1], context) + "))"
        }

        if (OperationType.IS_EMPTY == expression.type) {
            return "(" + resolveExpression(expression.arguments[0], context) + " IS EMPTY)"
        }

        throw IllegalArgumentException("Unexpected operation type: " + expression.type)
    }

    private fun resolveExpression(field: ExpressionField, context: HqlExpressionContext): String {
        return context.resolver.fieldResolver.apply(field)
    }

    private fun resolveExpression(expression: ExpressionValue, context: HqlExpressionContext): String {
        if (!context.state.parameters.inverse().containsKey(expression.value)) {
            val parameterIndex = context.state.parameterIndex
            val parameterName = "param_$parameterIndex"

            context.state.parameterIndex = parameterIndex + 1
            context.state.parameters[parameterName] = expression.value

            return ":$parameterName"
        } else {
            return ":" + context.state.parameters.inverse()[expression.value]
        }
    }
}

data class SubqueryExpression(val query: StructuredQuery<Any>): CustomExpression<HqlExpressionContext> {
    override fun resolve(context: HqlExpressionContext): String = HqlUtils.resolveQuery(query, context);
}