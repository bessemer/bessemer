package bessemer.hibernate.hql

import bessemer.hibernate.query.QueryJoinType.INNER
import bessemer.hibernate.query.QueryJoinType.LEFT
import bessemer.hibernate.query.StructuredQuery
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.collection._Sets
import bessemer.cornerstone.data.pagination.KeySetPageRequest
import bessemer.cornerstone.data.pagination.PageRequest
import bessemer.cornerstone.data.pagination.SimplePageRequest
import bessemer.cornerstone.domain.OrderEnum
import bessemer.cornerstone.domain.Ordering
import bessemer.cornerstone.expression.ExpressionField
import bessemer.cornerstone.expression.OperationType
import bessemer.cornerstone.expression._Expressions
import bessemer.cornerstone.util._Ranges
import com.google.common.collect.Range
import java.lang.IllegalArgumentException

object HqlUtils {
    @JvmStatic
    @JvmOverloads
    fun resolveQuery(query: StructuredQuery<*>, resolver: HqlExpressionResolver = HqlExpressionResolver()): HqlExpressionResult {
        val context = HqlExpressionContext(resolver = resolver);
        val queryString = resolveQuery(query, context)
        return HqlExpressionResult(expressionString = queryString, parameters = _Maps.copy(context.state.parameters))
    }

    @JvmStatic
    fun resolveQuery(query: StructuredQuery<*>, context: HqlExpressionContext): String {
        val queryString = StringBuilder()

        queryString.append("select ")
        if(query.select.distinct) {
            queryString.append("distinct ")
        }
        queryString.append(query.select.expression).append(" from ")

        var addJoin = false
        for (join in query.fragment.joins) {
            if (addJoin) {
                when (join.type) {
                    INNER -> queryString.append("join ")
                    LEFT -> queryString.append("left join ")
                }
            } else {
                addJoin = true
            }

            queryString.append(join.expression).append(" as ").append(join.alias).append(" ")
        }

        val implicitJoinQualifiers = query.fragment.joins.stream()
                .filter{ join -> join.qualifier != null }
                .map{ join -> join.qualifier!! }
                .collect(_Sets.collect())

        val compiledWhereExpression = _Expressions.and(_Lists.append(implicitJoinQualifiers, query.fragment.where))
        if (compiledWhereExpression != _Expressions.NULL) {
            queryString.append("where ")

            val expressionString = HqlExpressions.resolveExpression(compiledWhereExpression, context)
            queryString.append(expressionString)
        }

        if (query.orderings.isNotEmpty()) {
            queryString.append(" order by ")

            query.orderings.forEach { ordering ->
                queryString.append((ordering.expression as ExpressionField).field)
                queryString.append(" ")
                queryString.append(if (ordering.order == OrderEnum.ASCENDING) "asc" else "desc")

                if (_Lists.last(query.orderings) != ordering) {
                    queryString.append(", ")
                }
            }
        }

        return queryString.toString()
    }
}