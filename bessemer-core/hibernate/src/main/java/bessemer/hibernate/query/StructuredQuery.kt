package bessemer.hibernate.query

import bessemer.hibernate.query.QueryJoinType.INNER
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.domain.Ordering
import bessemer.cornerstone.domain.Referent
import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression.ExpressionField
import bessemer.cornerstone.expression._Expressions
import bessemer.cornerstone.util._Ranges
import bessemer.hibernate.query.QueryFragment.Companion
import com.google.common.base.Preconditions
import com.google.common.collect.Range
import mu.KLogging

data class StructuredQuery<T>(
        val alias: String,
        val select: QuerySelect<T>,
        val fragment: QueryFragment = QueryFragment.EMPTY_FRAGMENT,
        val bounds: Range<Int> = _Ranges.POSITIVE_INTEGERS,
        val cacheRegion: Referent<String> = Referent.absent()) : QueryBuilderAdapter<StructuredQuery<T>> {

    companion object: KLogging()

    override val where: Expression
        get() = fragment.where

    override val joins: List<QueryJoin>
        get() = fragment.joins

    override val orderings: List<Ordering>
        get() = fragment.orderings

    fun <N> withSelect(select: QuerySelect<N>) = StructuredQuery(alias, select, this.fragment, this.bounds, this.cacheRegion)

    fun withFragment(fragment: QueryFragment) = this.copy(fragment = fragment)

    override fun withWhere(expression: Expression): StructuredQuery<T> {
        return this.withFragment(fragment.withWhere(expression))
    }

    override fun withJoins(joins: List<QueryJoin>): StructuredQuery<T> {
        return this.withFragment(fragment.withJoins(joins))
    }

    override fun withOrderings(orderings: List<Ordering>): StructuredQuery<T> {
        return this.withFragment(fragment.withOrderings(orderings))
    }

    fun limit(limit: Int?): StructuredQuery<T> {
        return this.bounds(bounds = _Ranges.limit(this.bounds, limit))
    }

    fun offset(offset: Int?): StructuredQuery<T> {
        return this.bounds(bounds = _Ranges.offset(this.bounds, offset))
    }

    fun bounds(bounds: Range<Int>): StructuredQuery<T> {
        Preconditions.checkArgument(_Ranges.POSITIVE_INTEGERS.encloses(bounds))
        return this.copy(bounds = bounds)
    }

    fun cache(cache: Boolean = true): StructuredQuery<T> {
        return if (cache) {
            this.copy(cacheRegion = Referent.present(null))
        } else {
            this.copy(cacheRegion = Referent.absent())
        }
    }

    fun cache(cacheRegion: String): StructuredQuery<T> {
        return this.copy(cacheRegion = Referent.present(cacheRegion))
    }
}

interface QueryBasis {
    val where: Expression

    val joins: List<QueryJoin>

    val orderings: List<Ordering>

    fun withWhere(expression: Expression): QueryBasis

    fun where(expression: Expression): QueryBasis

    fun withJoins(joins: List<QueryJoin>): QueryBasis

    fun join(join: QueryJoin): QueryBasis

    fun join(expression: String, alias: String): QueryBasis

    fun join(type: Class<*>, alias: String): QueryJoinBuilder<out QueryBasis>

    fun withOrderings(orderings: List<Ordering>): QueryBasis

    fun order(orderings: List<Ordering>): QueryBasis
}

interface QueryBuilderAdapter<T : QueryBasis> : QueryBasis {
    override val where: Expression

    override val joins: List<QueryJoin>

    override val orderings: List<Ordering>

    override fun withWhere(expression: Expression): T

    override fun where(expression: Expression): T {
        return this.withWhere(_Expressions.and(this.where, expression))
    }

    override fun withJoins(joins: List<QueryJoin>): T

    override fun join(join: QueryJoin): T {
        return this.withJoins(_Lists.append(this.joins, join))
    }

    override fun join(expression: String, alias: String): T {
        return this.join(QueryJoin(expression, alias))
    }

    override fun join(type: Class<*>, alias: String): QueryJoinBuilder<T> {
        return QueryJoinBuilder(this as T, QueryJoin(expression = type.name, alias = alias))
    }

    override fun withOrderings(orderings: List<Ordering>): T

    fun order(ordering: Ordering): T {
        return this.order(_Lists.of(ordering))
    }

    override fun order(orderings: List<Ordering>): T {
        return this.withOrderings(_Lists.concat(orderings, this.orderings))
    }
}

data class QueryJoinBuilder<T : QueryBasis>(val query: T, val join: QueryJoin) {
    fun on(joinExpression: Expression) = query.join(join.copy(qualifier = joinExpression)) as T

    fun simple() =  query.join(join) as T
}

data class QueryFragment(
        override val joins: List<QueryJoin> = _Lists.of(),
        override val where: Expression = _Expressions.NULL,
        override val orderings: List<Ordering> = _Lists.of()) : QueryBuilderAdapter<QueryFragment> {

    companion object {
        val EMPTY_FRAGMENT = QueryFragment()
    }

    override fun withWhere(expression: Expression) = this.copy(where = expression)

    override fun withJoins(joins: List<QueryJoin>) = this.copy(joins = joins)

    override fun withOrderings(orderings: List<Ordering>) = this.copy(orderings = orderings)
}

data class QuerySelect<T>(
        val type: Class<T>,
        val expression: String,
        val distinct: Boolean = false) {

    companion object {
        fun <T> of(type: Class<T>, expression: String, distinct: Boolean = false): QuerySelect<T> = QuerySelect(type, expression, distinct)

        fun <T> of(type: Class<T>, expression: ExpressionField, distinct: Boolean = false): QuerySelect<T> = of(type, expression.field.toString(), distinct)
    }
}

data class QueryJoin(
        val expression: String,
        val alias: String,
        val type: QueryJoinType = INNER,
        val qualifier: Expression? = null,
        val fetch: Boolean = false) {

    fun withQualifier(qualifier: Expression?) = this.copy(qualifier = qualifier)
    fun withFetch(fetch: Boolean) = this.copy(fetch = fetch)
}

enum class QueryJoinType {
    INNER, LEFT
}