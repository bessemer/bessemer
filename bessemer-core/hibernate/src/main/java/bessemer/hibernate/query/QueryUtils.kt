package bessemer.hibernate.query

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.data.pagination.KeySetPageRequest
import bessemer.cornerstone.data.pagination.PageRequest
import bessemer.cornerstone.data.pagination.SimplePageRequest
import bessemer.cornerstone.domain.OrderEnum
import bessemer.cornerstone.domain.Ordering
import bessemer.cornerstone.expression.OperationType
import bessemer.cornerstone.expression._Expressions
import bessemer.cornerstone.util._Ranges
import com.google.common.collect.Range
import java.lang.IllegalArgumentException

object QueryUtils {
    @JvmStatic
    fun <T> buildPageQuery(originalQuery: StructuredQuery<T>, pageRequest: PageRequest): StructuredQuery<T> {
        // FUTURE make this "pluggable"
        if(pageRequest is SimplePageRequest) {
            val pageRange = Range.closedOpen(pageRequest.size * pageRequest.page, pageRequest.size * pageRequest.page + pageRequest.size)
            if(!originalQuery.bounds.isConnected(pageRange)) {
                return originalQuery.bounds(_Ranges.EMPTY_INTEGERS)
            }

            return originalQuery.bounds(originalQuery.bounds.intersection(pageRange))
        }
        else if(pageRequest is KeySetPageRequest) {
            if(originalQuery.bounds.hasUpperBound()) {
                throw IllegalArgumentException("Key Set pagination does not support queries which have an existing limit.")
            }

            if(!originalQuery.bounds.contains(0)) {
                throw IllegalArgumentException("Key Set pagination does not support queries which have an existing offset.")
            }

            // TODO not sure this check works, need to test query with keyset ordering
            // If we have a specified ordering, we need to see if it is still possible to proceed under keyset pagination
            val reverseMode = when (originalQuery.orderings) {
                _Lists.empty<Ordering>() -> false
                _Lists.of(Ordering.ascending(pageRequest.keyField)) -> false
                _Lists.of(Ordering.descending(pageRequest.keyField)) -> true
                else -> {
                    throw IllegalArgumentException("Key Set pagination does not support queries with custom orderings.")
                }
            }

            val ordering = if(!reverseMode) {
                Ordering.ascending(pageRequest.keyField)
            }
            else {
                Ordering.descending(pageRequest.keyField)
            }

            var pageQuery = originalQuery.limit(pageRequest.size).withOrderings(_Lists.of(ordering))

            if(pageRequest.key != null) {
                var operationType = OperationType.GREATER_THAN
                if(ordering.order == OrderEnum.DESCENDING) {
                    operationType = OperationType.LESS_THAN
                }

                val operation = _Expressions.operation(pageRequest.keyField, operationType, pageRequest.key!!)
                pageQuery = pageQuery.where(operation)
            }

            return pageQuery
        }
        else {
            throw IllegalArgumentException("Unsupported page request: $pageRequest")
        }
    }
}