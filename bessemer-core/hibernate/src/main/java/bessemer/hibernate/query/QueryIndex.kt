package bessemer.hibernate.query

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Streams
import bessemer.cornerstone.collection.toList
import bessemer.cornerstone.data.Index
import bessemer.cornerstone.data.PagingDataSet
import bessemer.cornerstone.data.PagingDataSetAdapter
import bessemer.cornerstone.data.PagingLexicon
import bessemer.cornerstone.data.pagination.KeySetPageRequest
import bessemer.cornerstone.data.pagination.KeySetPaginationStrategy
import bessemer.cornerstone.data.pagination.PageData
import bessemer.cornerstone.data.pagination.PageRequest
import bessemer.cornerstone.data.pagination.Pagination
import bessemer.cornerstone.data.pagination.PaginationStrategy
import bessemer.cornerstone.expression._Expressions
import bessemer.cornerstone.function._Functions
import bessemer.cornerstone.memento.MementoMetadata
import bessemer.cornerstone.util.map
import java.util.Optional
import java.util.Spliterator
import java.util.Spliterators.AbstractSpliterator
import java.util.concurrent.atomic.AtomicReference
import java.util.function.Consumer
import java.util.stream.Stream
import java.util.stream.StreamSupport

interface QueryResolver {
    fun <T> resolve(query: StructuredQuery<T>): List<T>
}

open class QueryContext<T>(
        val query: StructuredQuery<T>,
        val resolver: QueryResolver) {

    fun <N> withQuery(query: StructuredQuery<N>) = QueryContext(query, resolver)
}

open class QueryIndexContext<N: Any, T: Any>(
        query: StructuredQuery<T>,
        resolver: QueryResolver,
        val mementoMetadata: MementoMetadata<T, N>): QueryContext<T>(query, resolver) {

    fun withQuery(query: StructuredQuery<T>) = QueryIndexContext(query, resolver, mementoMetadata)
}

class MementizedQueryIndex<N: Any, T: Any>(
        private val context: QueryIndexContext<N, T>,
        private val pageSize: Int = Pagination.DEFAULT_PAGE_SIZE): Index<N, T> {

    override fun findEach(keys: Stream<N>): Stream<Pair<N, T>> {
       return  _Streams.partition(keys, pageSize)
               .flatMap {
                    val localQuery = context.query.where(_Expressions.contains(context.mementoMetadata.mementoField, it.toList()))
                   context.resolver.resolve(localQuery).stream()
               }
               .map {
                   context.mementoMetadata.mementizer(it) to it
               }
    }
}

class PaginatedQueryIndex<T>(private val context: QueryContext<T>): Index<PageRequest, PageData<T>> {
    override fun findEach(keys: Stream<PageRequest>): Stream<Pair<PageRequest, PageData<T>>> {
        return keys.map { resolve(it) }.filter { it.isPresent }.map { it.get() }.map { it.pageRequest to it }
    }

    private fun resolve(pageRequest: PageRequest): Optional<PageData<T>> {
        if(pageRequest is KeySetPageRequest) {
            val localQuery = QueryUtils.buildPageQuery(context.query, pageRequest)
            val results = context.resolver.resolve(localQuery)

            if(results.isEmpty()) {
                return Optional.empty()
            }

            return Optional.of(PageData(pageRequest = pageRequest, data = results))
        }
        else {
            throw IllegalArgumentException("Page request type not supported $pageRequest")
        }
    }
}

// TODO need to implement size such that it is efficient
// TODO pagination strategy needs to be further genericised here
class QueryDataSet<T: Any>(
        private val context: QueryContext<T>,
        private val paginationStrategy: PaginationStrategy<T>): PagingDataSetAdapter<T> {

    private val elementIndex = PaginatedQueryIndex(context)

    private val pageIndex = PaginatedQueryIndex(context.withQuery(query = context.query.withSelect(QuerySelect.of((paginationStrategy as KeySetPaginationStrategy<T>).keyType, paginationStrategy.keyField))))

    override fun findPages(keys: Stream<PageRequest>): Stream<Pair<PageRequest, PageData<T>>> = elementIndex.findEach(keys)

    override fun streamPageData(): Stream<Pair<PageRequest, PageData<T>>> = streamPageDataInternal().map { it.pageRequest to it }

    // TODO when streaming pages we take all of the ids and then take the last... this could be more efficient by letting the DB handle with MAX(...)
    override fun streamPages(): Stream<PageRequest> {
        val lastKey = AtomicReference<Any>(null)

        val spliterator = object : AbstractSpliterator<PageRequest>(java.lang.Long.MAX_VALUE, Spliterator.ORDERED and Spliterator.NONNULL) {
            override fun tryAdvance(action: Consumer<in PageRequest>): Boolean {
                val key = lastKey.get().map { _Expressions.value(it) }
                val pageRequest = KeySetPageRequest(key, (paginationStrategy as KeySetPaginationStrategy<T>).keyField, paginationStrategy.pageSize)
                val pageData = pageIndex.find(pageRequest)

                if(pageData != null) {
                    lastKey.set(_Lists.last(pageData.data))
                    action.accept(pageRequest)
                    return true
                }
                else {
                    return false
                }
            }
        }

        return StreamSupport.stream(spliterator, false)
    }

    private fun streamPageDataInternal(): Stream<PageData<T>> {
        val lastKey = AtomicReference<Any>(null)

        val spliterator = object : AbstractSpliterator<PageData<T>>(java.lang.Long.MAX_VALUE, Spliterator.ORDERED and Spliterator.NONNULL) {
            override fun tryAdvance(action: Consumer<in PageData<T>>): Boolean {
                val key = lastKey.get().map { _Expressions.value(it) }
                val pageRequest = KeySetPageRequest(key, (paginationStrategy as KeySetPaginationStrategy<T>).keyMetadata.mementoField, paginationStrategy.pageSize)
                val pageData = elementIndex.find(pageRequest)

                if(pageData != null) {
                    lastKey.set(paginationStrategy.getKey(_Lists.last(pageData.data)!!))
                    action.accept(pageData)
                    return true
                }
                else {
                    return false
                }
            }
        }

        return StreamSupport.stream(spliterator, false)
    }
}

class MappingDataSet<T: Any, N: Any>(
        private val dataSet: PagingDataSet<T>,
        private val transformer: (List<T>) -> List<N>): PagingDataSetAdapter<N> {

    override fun findPages(keys: Stream<PageRequest>): Stream<Pair<PageRequest, PageData<N>>> {
        return dataSet.pageLexicon().findEach(keys).map { it.first to PageData(it.second.pageRequest, transformer(it.second.data)) }
    }

    override fun streamPageData(): Stream<Pair<PageRequest, PageData<N>>> {
        return dataSet.pageLexicon().entries().stream().map { it.first to PageData(it.second.pageRequest, transformer(it.second.data)) }
    }

    override fun streamPages(): Stream<PageRequest> {
        return dataSet.pageLexicon().keys().stream()
    }
}

class QueryLexicon<N: Any, T: Any>: PagingLexicon<N, T> {
    private val context: QueryIndexContext<N, T>
    private val valuePaginationStrategy: PaginationStrategy<T>
    private val keyPaginationStrategy: PaginationStrategy<N>?

    private val index: Index<N, T>
    private val dataSet: MappingDataSet<T, Pair<N, T>>
    private val keyDataSet: PagingDataSet<N>

    @JvmOverloads
    constructor(
            context: QueryIndexContext<N, T>,
            valuePaginationStrategy: PaginationStrategy<T>,
            keyPaginationStrategy: PaginationStrategy<N>? = null) {

        this.context = context
        this.keyPaginationStrategy = keyPaginationStrategy
        this.valuePaginationStrategy = valuePaginationStrategy

        this.index = MementizedQueryIndex(context, valuePaginationStrategy.pageSize)

        val valueDataSet = QueryDataSet(
                context = context,
                paginationStrategy = valuePaginationStrategy
        )

        this.dataSet = MappingDataSet(
                dataSet = valueDataSet,
                transformer = _Functions.multiplex { context.mementoMetadata.mementizer(it) to it })

        if(keyPaginationStrategy != null) {
            val keysetQuery = context.query.withSelect(QuerySelect.of(context.mementoMetadata.mementoType, context.mementoMetadata.mementoField))

            this.keyDataSet = QueryDataSet(
                    context = context.withQuery(keysetQuery),
                    paginationStrategy = keyPaginationStrategy)
        }
        else {
            this.keyDataSet = MappingDataSet(
                    dataSet = valueDataSet,
                    transformer = _Functions.multiplex { context.mementoMetadata.mementizer(it) })
        }
    }

    override fun findEach(keys: Stream<N>) = index.findEach(keys)

    override fun keys() = keyDataSet

    override fun entries() = dataSet

    fun withQuery(query: StructuredQuery<T>) = QueryLexicon(this.context.withQuery(query), this.valuePaginationStrategy, this.keyPaginationStrategy)

    // TODO
    fun <K: Any> withMemento(mementoMetadata: MementoMetadata<T, K>) = QueryLexicon(this.context, this.valuePaginationStrategy)
}