package alloy.solr

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.util.Timer
import alloy.search.SearchCriteria
import alloy.search.SearchProvider
import alloy.search.SearchResultDto
import alloy.search.SearchService
import mu.KLogging
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.SolrRequest
import org.apache.solr.client.solrj.response.QueryResponse

class SolrSearchProvider(
        private val client: SolrClient,
        private val searchResolver: SolrSearchResolver): SearchProvider {

    companion object: KLogging()

    override fun <T: Any> performSearch(searchCriteria: SearchCriteria, searchService: SearchService<T>): SearchResultDto<T> {
        val solrQuery = SolrSearchUtil.buildSolrQuery(searchCriteria, searchService.searchIndexDetail, searchResolver)
        var response = this.executeQuery(solrQuery)
        val data = this.loadData(response, searchService)

        return SearchResultDto(data = data)
    }

    private fun executeQuery(solrQuery: SolrQuery): QueryResponse {
        SolrSearchUtil.toString(solrQuery)
        logger.debug { "Solr query: " + SolrSearchUtil.toString(solrQuery) }
        val response = Timer.time { client.query(solrQuery, SolrRequest.METHOD.POST) }
        logger.trace { "Solr took " + response.second + " to process query" }

        return response.first
    }

    private fun <T: Any> loadData(response: QueryResponse, searchService: SearchService<T>): List<T> {
        val responseDocuments = SolrSearchUtil.getResponseDocuments(response)
        val mementos = responseDocuments.map { it.getFieldValue(searchResolver.resolveField(searchService.searchIndexDetail.idField)).toString().toLong() }

        val results = searchService.backingLexicon.findEach(mementos)
        return _Lists.copy(results.values)
    }
}