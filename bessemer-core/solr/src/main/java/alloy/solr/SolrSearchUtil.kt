package alloy.solr

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.domain.OrderEnum
import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression._Expressions
import bessemer.cornerstone.string._Strings
import bessemer.cornerstone.util._Json
import bessemer.cornerstone.util._Optionals
import alloy.search.FacetCategory
import alloy.search.FacetDetail
import alloy.search.SearchCriteria
import alloy.search.SearchFieldFacet
import alloy.search.SearchIndexDetail
import alloy.search.SearchIndexNamespace
import alloy.search.SearchIntervalFacet
import alloy.search.SearchQueryFacet
import alloy.solr.expression.SolrExpressions
import org.apache.commons.lang3.ClassUtils
import org.apache.commons.lang3.StringUtils
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.response.QueryResponse
import org.apache.solr.client.solrj.util.ClientUtils
import org.apache.solr.common.SolrDocument
import java.math.BigDecimal
import java.net.URLDecoder
import java.util.*
import java.util.stream.Collectors

object SolrSearchUtil {
    @JvmStatic
    fun buildSolrQuery(searchCriteria: SearchCriteria, searchIndexDetail: SearchIndexDetail, searchResolver: SolrSearchResolver): SolrQuery {
        val solrQuery = SolrQuery()
        val queryExpression = searchCriteria.queryExpression
        solrQuery.query = searchResolver.resolveExpression(queryExpression)
        solrQuery.set("defType", "edismax")
        solrQuery.setFields(searchResolver.resolveField(searchIndexDetail.idField), "score")

        if(searchIndexDetail.namespace != null) {
            val namespace = searchIndexDetail.namespace as SearchIndexNamespace
            val matchNamespaceExpression = _Expressions.equals(namespace.field, namespace.value)
            solrQuery.addFilterQuery(searchResolver.resolveExpression(matchNamespaceExpression))
        }

        // TODO is there a better way to pass this?
        // solrQuery.set("q.op", searchCriteria.getSearchParameters().getOperator().name())

        val pageSize = searchCriteria.pageSize
        val start = if (searchCriteria.page <= 0) 0 else searchCriteria.page - 1

        if (pageSize != null) {
            solrQuery.setRows(pageSize).start = start * pageSize
        }

        for (sort in searchCriteria.sorts) {
            val solrOrder = if (sort.order == OrderEnum.ASCENDING) SolrQuery.ORDER.asc else SolrQuery.ORDER.desc
            solrQuery.addSort(searchResolver.resolveExpression(sort.expression), solrOrder)
        }

        solrQuery.addSort("score", SolrQuery.ORDER.desc)

        if (!searchCriteria.filterExpressions.isEmpty()) {
            val resolvedFilters = searchCriteria.filterExpressions.stream().map(searchResolver::resolveExpression).toArray { Array(it) { "" } }
            solrQuery.addFilterQuery(*resolvedFilters)
        }

        val queryFields = this.buildQueryFields(searchCriteria, searchResolver)
        if(!queryFields.isEmpty()) {
            solrQuery.set("qf", StringUtils.join(queryFields, " "))
        }

        // Attach facets to the solr query
        val facetMap = this.buildFacetMap(searchCriteria.facets, searchResolver)
        if(!facetMap.isEmpty()) {
            solrQuery.set("json.facet", _Json.marshall(facetMap))
        }

        val activeFilterQueriesByCategory = this.buildFacetCategoryMap(searchCriteria.facets.filter { it.active }, searchResolver)
        _Maps.stream(activeFilterQueriesByCategory).forEach { pair ->
            val facetCategory = pair.first
            // tag the category so the filter can be excluded for facets and "OR" all of the expressions together so all options apply
            val categoryExpression = SolrExpressions.tag(facetCategory.tags, _Expressions.or(pair.second))
            solrQuery.addFilterQuery(searchResolver.resolveExpression(categoryExpression))
        }

        return solrQuery
    }

    @JvmStatic
    fun getResponseDocuments(response: QueryResponse): List<SolrDocument> {
        val docs: MutableList<SolrDocument>

        if (response.groupResponse == null) {
            docs = response.results
        } else {
            docs = ArrayList()
            val gr = response.groupResponse
            for (gc in gr.values) {
                for (g in gc.values) {
                    docs.addAll(g.result)
                }
            }
        }

        return docs
    }

    private fun buildQueryFields(searchCriteria: SearchCriteria, searchResolver: SolrSearchResolver): List<String> {
        val queryFieldStrings = _Lists.ofMutable<String>()

        for (queryField in searchCriteria.searchFields) {

            //TODO support fuzzy field resolution somehow
//            if (searchCriteria.getSearchParameters().isFuzzyMatchingEnabled()) {
//                searchField = SearchFieldUtils.convertToFuzzySearchField(searchField)
//            }

            val searchField = queryField.field
            val fieldValue = searchResolver.resolveField(searchField)
            val boostValue = if (queryField.boost != null) "^" + queryField.boost.toString() else ""
            queryFieldStrings.add(fieldValue + boostValue)
        }

        return queryFieldStrings
    }

    private fun buildFacetMap(facetDetails: Set<FacetDetail>, searchResolver: SolrSearchResolver): Map<String, Any> {
        val dehydratedFacetMap = _Maps.ofMutable<String, Any>()

        facetDetails.forEach { facetDetail ->
            val facet = facetDetail.facet
            val jsonFacet = _Maps.ofMutable<String, Any>()

            when (facet) {
                is SearchQueryFacet -> {
                    jsonFacet["type"] = "query"
                    jsonFacet["q"] = searchResolver.resolveExpression(facet.query)
                }
                is SearchFieldFacet -> {
                    jsonFacet["type"] = "terms"
                    jsonFacet["field"] = searchResolver.resolveField(facet.field)
                }
                is SearchIntervalFacet -> {
                    val resolvedField = searchResolver.resolveField(facet.field)
                    jsonFacet["type"] = "query"
                    jsonFacet["q"] = "$resolvedField:*"
                    jsonFacet["facet"] = _Maps.of(
                            "minValue" to "min($resolvedField)",
                            "maxValue" to "max($resolvedField)"
                    )
                }
                else -> throw IllegalArgumentException("Unsupported facet $facet")
            }

            // exclude filter queries tagged with the same category to always return all results
            jsonFacet["domain"] = _Maps.of("excludeTags", facetDetail.facetDefinition.facetCategory.tags)
            dehydratedFacetMap[facetDetail.facetDefinition.key] = jsonFacet
        }

        return dehydratedFacetMap
    }

    fun buildFacetCategoryMap(activeFacets: Collection<FacetDetail>, searchResolver: SolrSearchResolver): Map<FacetCategory, Expression> {
        val activeFilterQueriesByCategory = _Maps.ofMutable<FacetCategory, Expression>()
        activeFacets.forEach { facetDetail ->
            val expression: Expression
            when {
                facetDetail.facet is SearchQueryFacet -> {
                    val queryFacet = facetDetail.facet as SearchQueryFacet
                    expression = queryFacet.query
                }
                facetDetail.facet is SearchFieldFacet -> {
                    val fieldFacet = facetDetail.facet as SearchFieldFacet
                    val fieldExpressions = facetDetail.activeValues.map { value -> _Expressions.equals(searchResolver.resolveField(fieldFacet.field), value) }

                    expression = _Expressions.or(fieldExpressions)
                }
                facetDetail.facet is SearchIntervalFacet -> {
                    val intervalFacet = facetDetail.facet as SearchIntervalFacet

                    val fieldExpressions = facetDetail.activeValues.stream()
                            .map { parseRangeValues(it) }
                            .flatMap { _Optionals.stream(it) }
                            .map { pair -> SolrExpressions.to(searchResolver.resolveField(intervalFacet.field), _Strings.toString(pair.first), _Strings.toString(pair.second)) }
                            .collect(Collectors.toList())

                    expression = _Expressions.or(fieldExpressions)
                }
                else -> throw IllegalArgumentException("Unsupported facet $facetDetail")
            }

            activeFilterQueriesByCategory[facetDetail.facetDefinition.facetCategory] = expression
        }

        return activeFilterQueriesByCategory
    }

    fun parseRangeValues(facetValue: String?): Optional<Pair<BigDecimal, BigDecimal>> {
        if (facetValue == null || !facetValue.contains("range[")) {
            return Optional.empty()
        }
        val rangeValue = facetValue.substring(facetValue.indexOf('[') + 1, facetValue.indexOf(']'))
        val rangeValues = StringUtils.split(rangeValue, ':')
        val minValue = parseRangeValue(rangeValues[0])
        val maxValue = if (rangeValues.size > 1) parseRangeValue(rangeValues[1]) else Optional.empty<BigDecimal>()
        return if (!minValue.isPresent && !maxValue.isPresent) {
            Optional.empty()
        } else Optional.of(minValue.orElse(null) to maxValue.orElse(null))
    }

    private fun parseRangeValue(rangeValue: String?): Optional<BigDecimal> {
        return if (rangeValue == null || rangeValue == "null") Optional.empty() else Optional.of(BigDecimal(rangeValue))
    }

    private const val NULL_SOLR_STRING = "null"

    fun formatValue(value: Any?, escapeLevel: Int): String {
        if (value == null) {
            return NULL_SOLR_STRING
        }

        if (!ClassUtils.isPrimitiveOrWrapper(value.javaClass) &&
                value !is String &&
                value !is Enum<*>) {
            throw IllegalArgumentException("Unexpected type: " + value.javaClass + " for value: " + value)
        }

        val escapedString = escapeString(value.toString(), escapeLevel)
        return if (value is String) {
            "\"" + escapedString + "\""
        } else escapedString
    }

    fun escapeString(s: String, escapeLevel: Int): String {
        var s = s
        for (i in escapeLevel downTo 1) {
            s = ClientUtils.escapeQueryChars(s)
        }
        return s
    }

    @JvmStatic
    fun toString(solrQuery: SolrQuery): String = URLDecoder.decode(solrQuery.toString(), "UTF-8")
}