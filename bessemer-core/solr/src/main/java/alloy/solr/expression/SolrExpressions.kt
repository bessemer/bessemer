package alloy.solr.expression

import bessemer.cornerstone.expression.CustomExpression
import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression.ExpressionField
import bessemer.cornerstone.expression.ExpressionValue
import bessemer.cornerstone.expression._Expressions
import bessemer.cornerstone.expression.OperationExpression
import bessemer.cornerstone.expression.OperationType
import alloy.search.SearchField
import alloy.search.expression.SearchExpressionContext
import alloy.solr.SolrSearchUtil
import java.util.function.BiFunction
import java.util.stream.Collectors

object SolrExpressions {
    // JOHN probably need a better way to do this
    private val MATCH_ALL = query(_Expressions.field(SearchField("*")), "*")

    @JvmStatic
    fun matchAll(): Expression = MATCH_ALL

    @JvmStatic
    fun boost(expression: Expression, boost: Int): Expression {
        return BoostExpression(expression, boost)
    }

    @JvmStatic
    fun blockJoinParent(which: Expression, body: Expression): Expression {
        return blockJoinParent(which, null, body)
    }

    @JvmStatic
    fun blockJoinParent(which: Expression, score: String?, body: Expression): Expression {
        return BlockJoinParentExpression(body, which, score)
    }

    @JvmStatic
    fun blockJoinChild(of: Expression, body: Expression): Expression {
        return blockJoinChild(of, null, body)
    }

    @JvmStatic
    fun blockJoinChild(of: Expression, score: String?, body: Expression): Expression {
        return BlockJoinChildExpression(body, of, score)
    }

    @JvmStatic
    fun sortByParentField(matchAllParentsExpression: Expression, parentField: ExpressionField): Expression {
        return blockJoinChild(matchAllParentsExpression, _Expressions.and(matchAllParentsExpression, functionQuery(parentField)))
    }

    @JvmStatic
    fun to(field: Any, first: String?, second: String?): Expression {
        return to(_Expressions.field(field), first, second)
    }

    @JvmStatic
    fun to(field: ExpressionField, first: String?, second: String?): Expression {
        val firstValue = first ?: "*"
        val secondValue = second ?: "*"
        val toQuery = "[$firstValue TO $secondValue]"
        return query(field, toQuery)
    }

    @JvmStatic
    fun query(field: SearchField, rawQuery: String): Expression {
        return query(_Expressions.field(field), rawQuery)
    }

    @JvmStatic
    @JvmOverloads
    fun query(field: ExpressionField? = null, rawQuery: String): Expression {
        return QueryFieldExpression(field, rawQuery)
    }

    @JvmStatic
    fun tag(tagName: String, body: Expression): Expression {
        return TagExpression(body, tagName)
    }

    // JOHN
    fun functionQuery(field: SearchField): Expression {
        return TODO()
//        return functionQuery(field.asExpression())
    }

    @JvmStatic
    fun functionQuery(body: ExpressionField): Expression {
        return FunctionExpression(body)
    }

    @JvmStatic
    fun macroField(fieldName: String): Expression {
        return MacroFieldExpression(fieldName)
    }

    @JvmStatic
    fun buildQuery(expression: Expression, context: SolrExpressionContext): String {
        return when (expression) {
            is OperationExpression -> buildQuery(expression, context)
            is ExpressionField -> buildQuery(expression, context)
            is ExpressionValue -> buildQuery(expression, context)
            is CustomExpression<*> -> buildQuery(expression as CustomExpression<SolrExpressionContext>, context)
            _Expressions.NULL -> ""
            else -> throw IllegalArgumentException("Unexpected expression type: " + expression.javaClass)
        }
    }

    private fun buildQuery(expression: CustomExpression<SolrExpressionContext>, context: SolrExpressionContext): String {
        return expression.resolve(context)
    }

    private fun buildQuery(expression: OperationExpression, context: SolrExpressionContext): String {
        return when (expression.type) {
            OperationType.AND -> {
                "(" + expression.arguments.stream()
                        .map { exp -> buildQuery(exp, context) }
                        .collect(Collectors.joining(" AND ")) + ")"
            }
            OperationType.OR -> {
                "(" + expression.arguments.stream()
                        .map { exp -> buildQuery(exp, context) }
                        .collect(Collectors.joining(" OR ")) + ")"
            }
            OperationType.NOT -> {
                "-" + buildQuery(expression.arguments[0], context)
            }
            OperationType.EQUALS -> {
                "(" + buildQuery(expression.arguments[0], context) + ":" + buildQuery(expression.arguments[1], context) + ")"
            }
            else -> {
                throw IllegalArgumentException("Unexpected operation type: " + expression.type)
            }
        }
    }

    private fun buildQuery(expression: ExpressionValue, context: SolrExpressionContext): String {
        return SolrSearchUtil.formatValue(expression.value, context.escapeLevel)
    }

    private fun buildQuery(field: ExpressionField, context: SolrExpressionContext): String {
        return context.fieldResolver.apply(field, context.fieldResolutionType)
    }
}

enum class SolrFieldResolutionType {
    STANDARD, FUZZY, QUERY
}

data class SolrExpressionContext(
        val fieldResolver: BiFunction<ExpressionField, SolrFieldResolutionType, String>,
        val fieldResolutionType: SolrFieldResolutionType = SolrFieldResolutionType.STANDARD,
        val escapeLevel: Int = 1) : SearchExpressionContext {

    fun incrementEscapeLevel(): SolrExpressionContext {
        return this.copy(escapeLevel = this.escapeLevel + 1)
    }

    companion object {
        @JvmStatic
        fun builder() = SolrExpressionContext.Builder()
    }

    class Builder {
        lateinit var fieldResolver: BiFunction<ExpressionField, SolrFieldResolutionType, String>
        var fieldResolutionType: SolrFieldResolutionType = SolrFieldResolutionType.STANDARD

        fun fieldResolver(fieldResolver: BiFunction<ExpressionField, SolrFieldResolutionType, String>) = apply { this.fieldResolver = fieldResolver }
        fun fieldResolutionType(fieldResolutionType: SolrFieldResolutionType) = apply { this.fieldResolutionType = fieldResolutionType }

        fun build() = SolrExpressionContext(
                fieldResolver,
                fieldResolutionType
        )
    }
}