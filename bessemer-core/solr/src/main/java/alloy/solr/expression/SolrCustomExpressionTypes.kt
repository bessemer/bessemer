package alloy.solr.expression

import bessemer.cornerstone.expression.CustomExpression
import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression.ExpressionField

class QueryFieldExpression(
        private val field: ExpressionField?,
        private val query: String) : CustomExpression<SolrExpressionContext> {

    override fun resolve(context: SolrExpressionContext): String {
        return if(field == null) {
            "($query)"
        }
        else {
            SolrExpressions.buildQuery(field, context.copy(fieldResolutionType = SolrFieldResolutionType.QUERY)) + ":" + query
        }
    }
}

class BlockJoinParentExpression(
        private val body: Expression,
        private val which: Expression,
        private val score: String? = null) : CustomExpression<SolrExpressionContext> {

    override fun resolve(context: SolrExpressionContext): String {
        var scoreString = " "
        if (this.score != null) {
            scoreString = " score=" + this.score + " "
        }
        val internalQueryContext = context.incrementEscapeLevel()
        val whichQuery = SolrExpressions.buildQuery(this.which, internalQueryContext)
        val bodyQuery = SolrExpressions.buildQuery(this.body, internalQueryContext)

        return "{!parent which='" + whichQuery + "'" + scoreString + "v='" + bodyQuery + "'}"
    }
}

class BlockJoinChildExpression(
        private val body: Expression,
        private val of: Expression,
        private val score: String? = null) : CustomExpression<SolrExpressionContext> {

    override fun resolve(context: SolrExpressionContext): String {
        var scoreString = " "
        if (this.score != null) {
            scoreString = " score=" + this.score + " "
        }
        val internalQueryContext = context.incrementEscapeLevel()
        val ofQuery = SolrExpressions.buildQuery(this.of, internalQueryContext)
        val bodyQuery = SolrExpressions.buildQuery(this.body, internalQueryContext)

        return "{!child of='" + ofQuery + "'" + scoreString + "v='" + bodyQuery + "'}"
    }
}

class BoostExpression(
        private val body: Expression,
        private val boost: Int) : CustomExpression<SolrExpressionContext> {

    override fun resolve(context: SolrExpressionContext): String {
        return SolrExpressions.buildQuery(this.body, context) + "^" + this.boost
    }
}

class FunctionExpression(
        private val body: Expression) : CustomExpression<SolrExpressionContext> {

    override fun resolve(context: SolrExpressionContext): String {
        return "{!func}" + SolrExpressions.buildQuery(this.body, context)
    }
}

class MacroFieldExpression(
        private val fieldName: String) : CustomExpression<SolrExpressionContext> {

    override fun resolve(context: SolrExpressionContext): String {
        return "\${$fieldName}"
    }
}

class TagExpression(
        private val body: Expression,
        private val tagName: String) : CustomExpression<SolrExpressionContext> {

    override fun resolve(context: SolrExpressionContext): String {
        return "{!tag=" + tagName + "}" + SolrExpressions.buildQuery(this.body, context)
    }
}