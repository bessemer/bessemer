package alloy.solr.test;

import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Sets;
import bessemer.cornerstone.expression.Expression;
import bessemer.cornerstone.expression._Expressions;
import alloy.search.SearchCriteria;
import alloy.search.SearchFacet;
import alloy.search.SearchField;
import alloy.search.SearchIndexDetail;
import alloy.search.SearchIndexDetailImpl;
import alloy.solr.SolrSearchResolver;
import alloy.solr.SolrSearchUtil;
import alloy.solr.expression.SolrExpressionContext;
import alloy.solr.expression.SolrExpressions;

public class SolrSearchProviderTest {
	@Test
	public void testBuildSolrQuery() {
		SearchIndexDetail searchIndexDetail = new SearchIndexDetailImpl(new SearchField("id"), null);
		SearchField userNameField = new SearchField("user-name");

		SolrExpressionContext solrExpressionContext = SolrExpressionContext.builder()
				.fieldResolver((expressionField, type) -> ((SearchField) expressionField.getField()).getKey())
				.build();

		SolrSearchResolver searchResolver = new SolrSearchResolver() {
			@NotNull
			public Map<String, Object> resolveFacet(@NotNull SearchFacet facet) {
				return null;
			}

			@NotNull
			@Override
			public String resolveField(@NotNull SearchField field) {
				return field.getKey();
			}

			@NotNull
			@Override
			public String resolveExpression(@NotNull Expression query) {
				return SolrExpressions.buildQuery(query, solrExpressionContext);
			}
		};

		{
			SearchCriteria searchCriteria = SearchCriteria.builder()
					.queryExpression(SolrExpressions.matchAll())
					.page(0)
					.pageSize(10)
					.build();

			SolrQuery solrQuery = SolrSearchUtil.buildSolrQuery(searchCriteria, searchIndexDetail, searchResolver);
			Assertions.assertEquals("q=*:*&defType=edismax&fl=id,score&rows=10&start=0&sort=score desc", SolrSearchUtil.toString(solrQuery));
		}

		{
			SearchCriteria searchCriteria = SearchCriteria.builder()
					.queryExpression(SolrExpressions.query("Hello World!"))
					.page(1)
					.filterExpressions(_Sets.of(_Expressions.equals(userNameField, "Robert")))
					.build();

			SolrQuery solrQuery = SolrSearchUtil.buildSolrQuery(searchCriteria, searchIndexDetail, searchResolver);
			Assertions.assertEquals("q=(Hello World!)&defType=edismax&fl=id,score&sort=score desc&fq=(user-name:\"Robert\")", SolrSearchUtil.toString(solrQuery));
		}
	}
}