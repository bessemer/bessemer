package alloy.spring.web.cache

import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.data.MapSupplierMutableLexicon
import bessemer.cornerstone.data.MutableIndex
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.context.request.ServletWebRequest

object RequestCache {
    @JvmStatic
    @JvmOverloads
    fun <T: Any, N: Any> of(cacheName: String, requestSupplier: () -> ServletWebRequest, scope: Int = RequestAttributes.SCOPE_REQUEST): MutableIndex<T, N> {
        val mapSupplier = {
            // TODO should there be some kind of prefix here?
            val attributeName = cacheName

            val request = requestSupplier()
            var targetValue = request.getAttribute(attributeName, scope)
            if(targetValue == null) {
                targetValue = _Maps.emptyMutable<T, N>()
                request.setAttribute(attributeName, targetValue, scope)
            }

            targetValue as MutableMap<T, N>
        }

        return MapSupplierMutableLexicon(mapSupplier = mapSupplier)
    }
}