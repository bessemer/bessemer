package bessemer.spring.hibernate.session

import bessemer.cornerstone.exception.ExceptionalOperation
import bessemer.spring.jpa.DataSourceReference
import bessemer.spring.jpa.NamedDataSourceReference
import org.apache.commons.lang3.StringUtils
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.hibernate.CacheMode
import org.hibernate.FlushMode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.annotation.AnnotationTarget.PROPERTY_GETTER
import kotlin.annotation.AnnotationTarget.PROPERTY_SETTER

@Component
@Aspect
@Order(0)
class SessionAspect {
    @Autowired
    lateinit var sessionExecutor: SessionExecutor

    @Around(value = "@annotation( session )")
    fun openSession(pjp: ProceedingJoinPoint, session: Session): Any? {
        var dataSourceType: DataSourceReference = DataSourceReference.DEFAULT
        if (!StringUtils.isBlank(session.dataSourceName)) {
            dataSourceType = NamedDataSourceReference(session.dataSourceName)
        }

        val sessionExecution = SessionExecutionDetail(dataSourceType, session.flushMode, session.cacheMode)
        return sessionExecutor.execute(sessionExecution) { pjp.proceed() }
    }
}

@Retention
@Target(FUNCTION, PROPERTY_GETTER, PROPERTY_SETTER)
annotation class Session(
        val dataSourceName: String = "",
        val flushMode: FlushMode = FlushMode.AUTO,
        val cacheMode: CacheMode = CacheMode.NORMAL)