package bessemer.spring.hibernate.session

import bessemer.hibernate.util.HibernateUtils
import bessemer.cornerstone.function.FunctionalExecutor
import bessemer.cornerstone.function.FunctionalExecutorArg1
import bessemer.spring.hibernate.session.SessionExecutionDetail.Companion.of
import bessemer.spring.jpa.DataSourceReference
import bessemer.spring.jpa.PersistenceMetadata
import org.hibernate.CacheMode
import org.hibernate.FlushMode
import org.hibernate.Session
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.orm.jpa.EntityManagerFactoryUtils
import org.springframework.orm.jpa.EntityManagerHolder
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionSynchronizationManager
import java.util.Optional
import javax.persistence.EntityManager

data class SessionExecutionDetail(
        val dataSource: DataSourceReference = DataSourceReference.DEFAULT,
        val flushMode: FlushMode = FlushMode.AUTO,
        val cacheMode: CacheMode = CacheMode.NORMAL) {

    companion object {
        @JvmField
        val DEFAULT = SessionExecutionDetail()

        @JvmStatic
        fun of(dataSourceType: DataSourceReference, session: Session): SessionExecutionDetail {
            return SessionExecutionDetail(dataSourceType, session.hibernateFlushMode, session.cacheMode)
        }
    }
}

@Component
class SessionExecutor @Autowired constructor(
        private val persistenceMetadata: PersistenceMetadata) : FunctionalExecutor, FunctionalExecutorArg1<SessionExecutionDetail> {

    @JvmSynthetic
    override fun <T> execute(f: () -> T): T {
        return this.execute(SessionExecutionDetail.DEFAULT, f)
    }

    @JvmSynthetic
    override fun <T> execute(arg1: SessionExecutionDetail, f: () -> T): T {
        val emFactory = persistenceMetadata.getEntityManagerFactory(arg1.dataSource)
        val existingEntityManager = getEntityManager(arg1.dataSource)
        val existingSessionExecutionDto = existingEntityManager
                .map { entityManager: EntityManager -> of(arg1.dataSource, entityManager.unwrap(Session::class.java)) }
        val createEntityManager = !existingSessionExecutionDto.isPresent
        val substituteEntityManager = !createEntityManager && existingSessionExecutionDto.get() != arg1
        if (createEntityManager) {
            val em = emFactory.createEntityManager()
            HibernateUtils.setCacheMode(em, arg1.cacheMode)
            HibernateUtils.setFlushMode(em, arg1.flushMode)
            val emHolder = EntityManagerHolder(em)
            if (existingEntityManager.isPresent) {
                TransactionSynchronizationManager.unbindResource(emFactory)
            }
            TransactionSynchronizationManager.bindResource(emFactory, emHolder)
        } else if (substituteEntityManager && existingEntityManager.isPresent) {
            val em = existingEntityManager.get()
            HibernateUtils.setCacheMode(em, arg1.cacheMode)
            HibernateUtils.setFlushMode(em, arg1.flushMode)
        }
        return try {
            f.invoke()
        } finally {
            if (createEntityManager) {
                val emHolder = TransactionSynchronizationManager.unbindResource(emFactory) as EntityManagerHolder
                EntityManagerFactoryUtils.closeEntityManager(emHolder.entityManager)
            } else if (substituteEntityManager && existingEntityManager.isPresent) {
                val em = existingEntityManager.get()
                HibernateUtils.setCacheMode(em, existingSessionExecutionDto.get().cacheMode)
                HibernateUtils.setFlushMode(em, existingSessionExecutionDto.get().flushMode)
            }
        }
    }

    private fun getEntityManager(dataSource: DataSourceReference): Optional<EntityManager> {
        val emFactory = persistenceMetadata.getEntityManagerFactory(dataSource)
        var existingEntityManager: Optional<EntityManager> = Optional.empty()
        if (TransactionSynchronizationManager.hasResource(emFactory)) {
            val entityManagerHolder = TransactionSynchronizationManager.getResource(emFactory) as EntityManagerHolder
            existingEntityManager = Optional.of(entityManagerHolder.entityManager)
        }
        return existingEntityManager
    }
}