package bessemer.spring.hibernate

import bessemer.spring.jpa.BessemerSpringJpaConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@ComponentScan
@Import(value = [
    BessemerSpringJpaConfiguration::class
])
class BessemerSpringHibernateConfiguration {

}