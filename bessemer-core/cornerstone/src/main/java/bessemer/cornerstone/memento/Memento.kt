package bessemer.cornerstone.memento

import bessemer.cornerstone.collection._Collectors
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.collection._Sets
import bessemer.cornerstone.expression.ExpressionField
import bessemer.cornerstone.util._Objects
import com.google.common.collect.BiMap
import com.google.common.collect.ImmutableBiMap
import java.io.Serializable
import java.util.stream.Collector
import java.util.stream.Stream

interface Memento<T> : Serializable {
    val memento: T

    companion object {
        @JvmStatic
        fun <T: Any, R: Memento<T>> mementos(list: Collection<R>): List<T> {
            return list.stream().map{ it.memento }.collect(_Lists.collect())
        }

        @JvmStatic
        fun <T, R : Memento<T>> mementos(list: Set<R>): Set<T> {
            return list.stream().map{ it.memento }.collect(_Sets.collect())
        }

        @JvmStatic
        fun <T, R : Memento<T>> mementoSet(stream: Stream<R>): Set<T> {
            return stream.map{ it.memento }.collect(_Sets.collect())
        }

        @JvmStatic
        fun <T, R : Memento<T>> mapify(list: Collection<R>): Map<T, R> {
            return mapify(list.stream())
        }

        @JvmStatic
        fun <T, R : Memento<T>> mapify(stream: Stream<R>): Map<T, R> {
            return stream.map{ element -> element.memento to element }.collect(_Maps.collect())
        }

        @JvmStatic
        fun mementoEquals(target: Memento<*>, other: Any): Boolean {
            val result = _Objects.equalsPreamble(target, other)
            if (result != null) {
                return result
            }

            val memento = other as Memento<*>
            return target.memento == memento.memento
        }

        @JvmStatic
        fun mementoHashCode(target: Memento<out Any>): Int {
            return target.memento.hashCode()
        }

        @JvmStatic
        fun <T: Memento<N>, N> collectMap(): Collector<T, Any, BiMap<T, N>> = _Collectors.buildMapCollector(
                collectionTarget = ImmutableBiMap.of(),
                extractor = { entry, builder -> builder(entry, entry.memento) }
        )
    }
}

data class MementoMetadata<O: Any, T: Any>(
        val mementizer: (O) -> T,
        val elementType: Class<O>,
        val mementoType: Class<T>,
        val mementoField: ExpressionField) {

    companion object {
        @JvmStatic
        fun <N: Identifiable> of(clazz: Class<N>, mementoField: ExpressionField): MementoMetadata<N, Long> {
            return MementoMetadata({ it.memento }, clazz, Long::class.java, mementoField)
        }
    }
}