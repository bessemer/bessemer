package bessemer.cornerstone.memento;

public interface Identifiable extends Memento<Long> {
	Long getId();

	@Override
	default Long getMemento() {
		return getId();
	}
}