package bessemer.cornerstone.preform

import bessemer.cornerstone.memento.Memento
import bessemer.cornerstone.schema.Schema
import bessemer.cornerstone.schema.SchemaType

data class Uuid(val id: String = "")

interface UniversallyIdentifiable: Memento<Uuid> {
    val id: Uuid

    override val memento: Uuid
        get() = id
}



data class PreformArena(
        val id: Uuid = Uuid(),
        val name: String,
        val parent: PreformArena? = null)

data class PreformTemplate(
        val id: Uuid = Uuid(),
        val name: String,
        val arena: PreformArena,
        val schema: Schema)

data class PreformElement(
        val id: Uuid = Uuid(),
        val arena: PreformArena,
        val template: PreformTemplate,
        val data: Any)

data class PreformRelation(
        val id: Uuid = Uuid(),
        val arena: PreformArena,
        val first: PreformElement,
        val second: PreformElement)

data class ReifiedElement(
        val id: Uuid = Uuid(),
        val arena: PreformArena,
        val template: PreformTemplate,
        val data: Any)

data class ReifiedRelation(
        val id: Uuid = Uuid(),
        val arena: PreformArena,
        val first: PreformElement,
        val second: PreformElement)

fun test() {
    val global = PreformArena(name = "Global")
    val world = PreformArena(name = "Aurum", parent = global)
    val stormlord = PreformArena(name = "Stormlord", parent = world)
    val myCampaign = PreformArena(name = "My Campaign", parent = stormlord)

    val characterTemplate = PreformTemplate(name = "Character", arena = global, schema = Schema(type = SchemaType.of("Character")))

    val lesStroud = PreformElement(data = "Les Stroud", arena = stormlord, template = characterTemplate)
    val marik = PreformElement(data = "Marik", arena = stormlord, template = characterTemplate)
    val relation = PreformRelation(arena = stormlord, first = lesStroud, second = marik)

    val deadMarik = PreformElement(id = marik.id, data = "Marik is dead", arena = myCampaign, template = characterTemplate)

    // this is what things would look like after reification (magically)
    // val reifiedRelation = ReifiedRelation()
}