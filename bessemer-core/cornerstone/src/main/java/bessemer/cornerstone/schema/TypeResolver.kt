package bessemer.cornerstone.schema

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.reflection.TypeToken
import bessemer.cornerstone.util.map
import bessemer.cornerstone.util.toNullable
import java.math.BigDecimal
import java.math.BigInteger
import java.time.Instant
import java.util.*
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.jvm.jvmErasure

interface TypeResolver {
    fun resolve(type: TypeToken<*>): SchemaType?

    companion object {
        @JvmField
        val DEFAULT: TypeResolver = compose(TypeResolverComponent.BASE, TypeResolverComponent.OBJECT)

        @JvmStatic
        fun compose(vararg typeResolvers: TypeResolverComponent) = compose(_Lists.of(*typeResolvers))

        @JvmStatic
        fun compose(typeResolvers: List<TypeResolverComponent>) = object : TypeResolverComponent {
            override fun resolve(type: TypeToken<*>, typeResolver: TypeResolverComponent): SchemaType? {
                return typeResolvers.stream()
                        .map { it.resolve(type, this) }
                        .filter { it != null }
                        .map { it!! }
                        .findFirst()
                        .orElse(null)
            }
        }
    }
}

interface TypeResolverComponent: TypeResolver {
    override fun resolve(type: TypeToken<*>) = this.resolve(type, this)

    fun resolve(type: TypeToken<*>, typeResolver: TypeResolverComponent): SchemaType?

    companion object {
        @JvmField
        val BASE = object : TypeResolverComponent {
            override fun resolve(type: TypeToken<*>, typeResolver: TypeResolverComponent): SchemaType? {
                var result: SchemaType? = when(type.type.classifier) {
                    Any::class -> SchemaType.Any
                    Byte::class -> SchemaType.Byte
                    Boolean::class -> SchemaType.Boolean
                    Character::class -> SchemaType.Character
                    String::class -> SchemaType.String
                    Short::class -> SchemaType.Number
                    Integer::class -> SchemaType.Number
                    Long::class -> SchemaType.Number
                    Double::class -> SchemaType.Number
                    Float::class -> SchemaType.Number
                    BigInteger::class -> SchemaType.Number
                    BigDecimal::class -> SchemaType.Number
                    Date::class -> SchemaType.Instant
                    Instant::class -> SchemaType.Instant
                    else -> null
                }

                if(result != null) {
                    return result
                }

                if(type.type.jvmErasure.isSubclassOf(List::class)) {
                    result = typeResolver.resolve(TypeToken.of<Any>(type.type.arguments[0].type!!), typeResolver)
                            .map { SchemaType.List(it) }
                }

                if(type.type.jvmErasure.isSubclassOf(Set::class)) {
                    result = typeResolver.resolve(TypeToken.of<Any>(type.type.arguments[0].type!!), typeResolver)
                            .map { SchemaType.Set(it) }
                }

                if(type.type.jvmErasure.isSubclassOf(Map::class)) {
                    val keyType = typeResolver.resolve(TypeToken.of<Any>(type.type.arguments[0].type!!), typeResolver)
                    val valueType = typeResolver.resolve(TypeToken.of<Any>(type.type.arguments[1].type!!), typeResolver)

                    if(keyType != null && valueType != null) {
                        result = SchemaType.Map(keyType, valueType)
                    }
                }

                return result
            }
        }

        @JvmField
        val OBJECT = object : TypeResolverComponent {
            override fun resolve(type: TypeToken<*>, typeResolver: TypeResolverComponent): SchemaType? =
                    SchemaType.of(type.type.jvmErasure)
        }
    }
}
