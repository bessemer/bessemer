package bessemer.cornerstone.schema

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.util.toNullable
import java.util.*

interface TypeAliaser {
    fun alias(type: SchemaType): Optional<SchemaType>

    companion object {
        val NONE = object : TypeAliaser {
            override fun alias(type: SchemaType): Optional<SchemaType> = Optional.empty()
        }

        val SIMPLE = of(_Maps.from(
                SchemaType.Byte to SchemaType.String,
                SchemaType.Character to SchemaType.String,
                SchemaType.List to SchemaType.Collection,
                SchemaType.Set to SchemaType.Collection
        ))

        fun of(map: Map<SchemaType, SchemaType>): TypeAliaser {
            return object : TypeAliaser {
                override fun alias(type: SchemaType) = Optional.ofNullable(map[type])
            }
        }

        @JvmStatic
        fun compose(vararg typeAliasers: TypeAliaser) = compose(_Lists.of(*typeAliasers))

        @JvmStatic
        fun compose(typeAliasers: List<TypeAliaser>) = object : TypeAliaser {
            override fun alias(type: SchemaType): Optional<SchemaType> {
                return typeAliasers.stream()
                        .map { it.alias(type).toNullable() }
                        .filter { it != null }
                        .map { it!! }
                        .findFirst()
            }
        }
    }
}