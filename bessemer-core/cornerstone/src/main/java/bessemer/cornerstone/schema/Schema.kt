package bessemer.cornerstone.schema

import bessemer.cornerstone.collection._Sets
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

interface SchemaType {
    companion object {
        @JvmStatic
        fun of(type: Class<*>) = of(type.kotlin)

        fun of(type: KClass<*>) = of(type.qualifiedName!!)

        @JvmStatic
        fun of(name: String) = SimpleType(name)

        @JvmField
        val Any = SimpleType("Any")

        @JvmField
        val Byte = SimpleType("Byte")

        @JvmField
        val Boolean = SimpleType("Boolean")

        @JvmField
        val Character = SimpleType("Character")

        @JvmField
        val String = SimpleType("String")

        @JvmField
        val Number = SimpleType("Number")

        @JvmField
        val Instant = SimpleType("Instant")

        @JvmField
        val Collection = SimpleType("Collection")

        @JvmField
        val List = SimpleType("List")

        @JvmField
        val Set = SimpleType("Set")

        @JvmField
        val Map = SimpleType("Map")

        @JvmField
        val Reference = SimpleType("Reference")

        @JvmStatic
        fun Collection(type: SchemaType): SchemaType = ParameterizedType(Collection, listOf(type))

        @JvmStatic
        fun List(type: SchemaType): SchemaType = ParameterizedType(List, listOf(type))

        @JvmStatic
        fun Set(type: SchemaType): SchemaType = ParameterizedType(Set, listOf(type))

        @JvmStatic
        fun Map(keyType: SchemaType, valueType: SchemaType): SchemaType = ParameterizedType(Map, listOf(keyType, valueType))

        @JvmStatic
        fun Reference(type: SchemaType): SchemaType = ParameterizedType(Reference, listOf(type))
    }
}

data class SimpleType(val name: String): SchemaType

data class ParameterizedType(val root: SchemaType, val typeParameters: List<SchemaType>): SchemaType

data class Schema(
        val type: SchemaType,
        val fields : Set<SchemaField> = _Sets.of()): SchemaType {

    fun getField(name : String): Optional<SchemaField> {
        return fields.stream().filter { it.name == name }.findAny()
    }
}

data class SchemaField(val name: String, val type: SchemaType)

data class SchemaFieldReference(val type: SchemaType, val fieldName: String)

class SchemaDirectory {
    private val directory: MutableMap<SchemaType, Schema> = ConcurrentHashMap()

    fun register(schema: Schema) {
        directory[schema.type] = schema
    }

    fun read(type: SchemaType): Schema? = directory[type]

    companion object {
        @JvmField
        val GLOBAL = SchemaDirectory()

        @JvmStatic
        fun registerGlobal(schema: Schema) = GLOBAL.register(schema)

        @JvmStatic
        fun readGlobal(type: SchemaType) = GLOBAL.read(type)
    }
}