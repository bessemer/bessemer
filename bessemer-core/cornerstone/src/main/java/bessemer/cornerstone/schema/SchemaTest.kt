package bessemer.cornerstone.schema

import bessemer.cornerstone.collection._Sets

fun blah() {
    val characterSchema = Schema(
            type = SchemaType.of("simulacrum.Character"),
            fields = _Sets.of(
                    SchemaField("id", SchemaType.Number),
                    SchemaField("name", SchemaType.String),
                    SchemaField("age", SchemaType.Number),
                    SchemaField("weapon", SchemaType.of("simulacrum.Weapon")),
                    SchemaField("father", SchemaType.Reference(SchemaType.of("simulacrum.Character"))),
                    SchemaField("siblings", SchemaType.Reference(SchemaType.List(SchemaType.of("simulacrum.Character"))))
            ))
}