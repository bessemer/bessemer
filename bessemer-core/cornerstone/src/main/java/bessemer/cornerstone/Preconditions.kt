package bessemer.cornerstone

import bessemer.cornerstone.string._Strings
import java.lang.IllegalArgumentException

object _Preconditions {
    @JvmStatic
    fun notEmpty(string: String, message: String) {
        if(_Strings.isEmpty(string)) {
            throw IllegalArgumentException(message)
        }
    }

    @JvmStatic
    fun notNull(item: Any?, message: String) {
        if(item == null) {
            throw IllegalArgumentException(message)
        }
    }

    @JvmStatic
    fun checkArgument(result: Boolean, message: String) {
        if(!result) {
            throw IllegalArgumentException(message)
        }
    }
}