package bessemer.cornerstone.repository

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.data.AlwaysEmptyMutableLexicon
import bessemer.cornerstone.data.MutableIndex
import bessemer.cornerstone.data.PagingLexicon
import bessemer.cornerstone.data._Indexes
import java.util.stream.Stream

object _Repositories {
    @JvmStatic
    fun <KEY: Any, T: Any> empty(mementizer: (T) -> KEY): Repository<T, KEY> = MapRepository(mementizer)

    @JvmStatic
    fun <KEY: Any, T: Any> from(mementizer: (T) -> KEY, map: Map<KEY, T>): Repository<T, KEY> = MapRepository(mementizer, _Maps.copyMutable(map))
}

class AlwaysEmptyRepository private constructor(
        private val index: MutableIndex<Any, Any> = AlwaysEmptyMutableLexicon.get()): Repository<Any, Any>, MutableIndex<Any, Any> by index {

    override fun <S : Any> saveStream(entities: Stream<S>): Stream<S> = entities

    override fun delete(entities: Iterable<Any>) {
        // Do nothing
    }

    override fun put(elements: Stream<Pair<Any, Any>>) {
        // Do nothing
    }

    companion object {
        private val INSTANCE = AlwaysEmptyRepository()

        @Suppress("UNCHECKED_CAST")
        @JvmStatic
        fun <T: Any, KEY: Any> get(): Repository<T, KEY> {
            return INSTANCE as Repository<T, KEY>
        }
    }

    override fun findAll(): PagingLexicon<Any, Any> = _Indexes.empty()
}

class MapRepository<T: Any, KEY: Any>(
        private val mementizer: (T) -> KEY,
        private val map: MutableMap<KEY, T> = _Maps.emptyMutable()): MementoRepository<T, KEY> {

    override fun mementize(entity: T): KEY = mementizer(entity)

    override fun findAll(): PagingLexicon<KEY, T> = _Indexes.from(map)

    override fun findEach(keys: Stream<KEY>): Stream<Pair<KEY, T>> {
        return keys.filter { map.containsKey(it) }
                .map { it!! to map[it]!! }
    }

    override fun <S : T> saveStream(entities: Stream<S>): Stream<S> {
        val resolvedEntities = entities.collect(_Lists.collect())
        resolvedEntities.forEach {
            map[mementizer(it)] = it
        }

        return resolvedEntities.stream()
    }

    override fun remove(keys: Stream<KEY>) = keys.forEach { map.remove(it) }

    override fun removeAll() = map.clear()
}

//interface RepositoryIndexAdapter<N: Any, T: Any>: RepositoryIndex<N, T>, IndexDelegator<N, T> {
//    val entityManager: EntityManager
//    val context: QueryIndexContext<N, T>
//
//    @JvmDefault
//    override val indexDelegate: Index<N, T>
//        get() = MementizedQueryIndex(context)
//
//    @JvmDefault
//    override fun putAll(elements: Stream<Pair<N, T>>) {
//        elements.map { it.second }.forEach { entityManager.merge(it) }
//    }
//
//    @JvmDefault
//    override fun removeAll(keys: Stream<N>) {
//        // TODO this could be more efficient by not loading the entity before removing it
//        this.findEach(keys).forEach { entityManager.remove(it) }
//    }
//
//    @JvmDefault
//    override fun query(query: StructuredQuery<T>): Index<N, T> = MementizedQueryIndex(context.withQuery(query))
//}

//data class ConcreteRepositoryIndex<N: Any, T: Any>(
//        override val context: QueryIndexContext<N, T>) : RepositoryIndex<N, T>, RepositoryIndexAdapter<N, T>

//interface RepositoryAdapter<N: Any, T: Any>: Repository<N, T>, RepositoryIndexAdapter<N, T>, PagingLexiconDelegator<N, T>  {
//    @JvmDefault
//    val paginationAttributes: PaginationAttributes
//        get() = PaginationAttributes()
//
//    @JvmDefault
//    override val indexDelegate: Index<N, T>
//        get() = MementizedQueryIndex(context, paginationAttributes.pageSize)
//
//    @JvmDefault
//    override val lexiconDelegate: PagingLexicon<N, T>
//        get() = _Queries.buildLexicon(context, paginationAttributes)
//
//    @JvmDefault
//    override fun query(query: HqlQuery<T>): PagingLexicon<N, T> = _Queries.buildLexicon(context.withQuery(query), paginationAttributes)
//
//    @JvmDefault
//    override fun findEach(mementos: Stream<N>) = lexiconDelegate.findEach(mementos)
//
//    @JvmDefault
//    override fun stream() = lexiconDelegate.stream()
//}
//
//class ConcreteRepository<N: Any, T: Any>(
//        override val context: QueryIndexContext<N, T>): Repository<N, T>, RepositoryAdapter<N, T>

//interface MementoRepository<N: Any, T: Memento<N>>: RepositoryOperations<N, T> {
//
//}
//
//interface IdentifiableRepository<T: bessemer.cornerstone.memento.Identifiable>: MementoRepository<Long, T> {
//
//}

//interface Repository<T, N>: PagingLexicon<N, T> {
//    fun query(query: HqlQuery<T>): PagingLexicon<N, T>
//}
//
//interface DelegatingDao<T, N>: Repository<T, N> {
//    val delegate: Repository<T, N>
//
//    override fun findEach(mementos: Stream<N>) = delegate.findEach(mementos)
//
//    override fun keys() = delegate.keys()
//
//    override fun entries() = delegate.entries()
//
//    override fun query(query: HqlQuery<T>) = delegate.query(query)
//}
//
//class ConcreteRepository<T: Any, N: Any>(private val dictionary: QueryLexicon<N, T>): Repository<T, N>, PagingLexicon<N, T> by dictionary {
//    override fun query(query: HqlQuery<T>) = dictionary.withQuery(query)
//}