package bessemer.cornerstone.repository

import bessemer.cornerstone.collection._Iterables
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Streams
import bessemer.cornerstone.data.MutableIndex
import bessemer.cornerstone.data.PagingLexicon
import java.util.stream.Stream

interface RepositoryOperations<T: Any, KEY: Any> : MutableIndex<KEY, T> {
    fun findAll(): PagingLexicon<KEY, T>

    @JvmDefault
    fun <S : T> save(entity: S): S = _Iterables.only(this.save(_Lists.of(entity)))!!

    @JvmDefault
    fun <S : T> save(vararg entities: S): List<S> = this.save(_Lists.of(*entities))

    @JvmDefault
    fun <S : T> save(entities: Iterable<S>): List<S> = this.saveStream(_Streams.from(entities)).collect(_Lists.collect())

    fun <S : T> saveStream(entities: Stream<S>): Stream<S>

    @JvmDefault
    fun delete(entity: T) = this.delete(_Lists.of(entity))

    fun delete(entities: Iterable<T>)

    @JvmDefault
    override fun put(elements: Stream<Pair<KEY, T>>) {
        this.saveStream(elements.map { it.second })
    }
}

interface Repository<T: Any, KEY: Any>: RepositoryOperations<T, KEY> {

}

interface MementoRepository<T: Any, KEY: Any>: Repository<T, KEY> {
    fun mementize(entity: T): KEY

    override fun delete(entities: Iterable<T>) = this.remove(entities.map(this::mementize))
}