package bessemer.cornerstone.retry

import bessemer.cornerstone.domain.Outcome
import bessemer.cornerstone.exception._Exceptions
import bessemer.cornerstone.random._Random
import com.google.common.base.Predicates
import mu.KLogging
import java.time.Duration
import java.util.*
import java.util.function.Function
import java.util.function.Predicate
import java.util.function.Supplier

object _Retry: KLogging() {
    @JvmStatic
    @JvmOverloads
    fun <T> retry(operation: Supplier<T>, retryRequest: RetryRequest = RetryRequest.DEFAULT): T =
            retry(operation, Predicates.alwaysTrue(), retryRequest)

    @JvmStatic
    @JvmOverloads
    fun <T> retry(operation: Supplier<T>, exceptionType: Class<Exception>, retryRequest: RetryRequest = RetryRequest.DEFAULT): T =
            retry(operation, Predicate { _Exceptions.getCause(it, exceptionType) != null }, retryRequest)

    @JvmStatic
    @JvmOverloads
    fun <T> retry(operation: Supplier<T>, exceptionFilter: Predicate<Exception>, retryRequest: RetryRequest = RetryRequest.DEFAULT): T {
        val result = retry(
                Supplier<Outcome<T, Exception>> {
                    try {
                        return@Supplier Outcome.success(operation.get())
                    } catch (e: Exception) {
                        if(exceptionFilter.test(e)) {
                            logger.warn(e) { "Encountered exception while in retry block" }
                            return@Supplier Outcome.error(e)
                        }
                        else {
                            throw e
                        }
                    }
                },
                retryRequest)

        if (result.isError()) {
            throw result.error
        }

        return result.result
    }

    @JvmStatic
    @JvmOverloads
    fun <T> retryValue(operation: Supplier<Optional<T>>, request: RetryRequest = RetryRequest.DEFAULT): Optional<T> {
        val supplier = Supplier { operation.get().map { Outcome.success<T, Any>(it) }.orElse(Outcome.error(Optional.empty<Any>())) }

        val result = retry(supplier, request)
        return if (!result.isError()) {
            Optional.of(result.result)
        } else {
            Optional.empty()
        }
    }

    @JvmStatic
    @JvmOverloads
    fun <T, N> retry(operation: Supplier<Outcome<T, N>>, request: RetryRequest = RetryRequest.DEFAULT): Outcome<T, N> {
        return request.retryJoinStrategy.handle(request, Function { context -> retry(operation, context) })
    }

    private fun <T, N> retry(operation: Supplier<Outcome<T, N>>, context: RetryContext): Outcome<T, N> {
        var outcome: Outcome<T, N>? = null

        logger.trace { "Beginning retry with context $context" }
        while (context.totalAttempts <= context.request.attempts) {
            outcome = operation.get()

            if (!outcome.isError()) {
                return outcome
            }

            val backoff = context.request.backoffStrategy.getBackoff(context)

            logger.trace { "Retry attempt failed, backing off with duration " + backoff.toMillis() + " ms" }
            Thread.sleep(backoff.toMillis())

            context.totalAttempts = context.totalAttempts + 1
        }

        if (outcome!!.isError()) {
            logger.trace { "Retry limit exceeded, returning error result" }
        }

        return outcome
    }
}

data class RetryRequest(
        val attempts: Int = 5,
        val backoffStrategy: BackoffStrategy = BackoffStrategy.random(),
        val retryJoinStrategy: RetryJoinStrategy = RetryJoinStrategy.NONE) {

    companion object {
        val DEFAULT = RetryRequest()
    }
}

class RetryContext : Cloneable {
    var totalAttempts: Int = 0
    var request: RetryRequest

    constructor(request: RetryRequest) {
        this.totalAttempts = 0
        this.request = request
    }

    constructor(context: RetryContext) {
        this.totalAttempts = context.totalAttempts
        this.request = context.request
    }
}

@FunctionalInterface
interface BackoffStrategy {
    fun getBackoff(context: RetryContext): Duration

    companion object {
        fun immediate(): BackoffStrategy {
            return constant(Duration.ZERO)
        }

        fun constant(duration: Duration): BackoffStrategy {
            return of { _ -> duration }
        }

        @JvmOverloads
        fun random(minDuration: Duration = Duration.ofMillis(50), maxDuration: Duration = Duration.ofMillis(150)): BackoffStrategy {
            return of { _ -> _Random.randomDuration(minDuration, maxDuration) }
        }

        fun of(func: (RetryContext) -> Duration): BackoffStrategy {
            return object : BackoffStrategy {
                override fun getBackoff(context: RetryContext) = func(context)
            }
        }
    }
}

interface RetryJoinStrategy {
    fun <T, N> handle(request: RetryRequest, handler: Function<RetryContext, Outcome<T, N>>): Outcome<T, N>

    companion object {
        val NONE: RetryJoinStrategy = object : RetryJoinStrategy {
            override fun <T, N> handle(request: RetryRequest, handler: Function<RetryContext, Outcome<T, N>>): Outcome<T, N> {
                return handler.apply(RetryContext(request))
            }
        }
    }
}