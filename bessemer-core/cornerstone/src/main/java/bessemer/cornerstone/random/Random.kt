package bessemer.cornerstone.random

import bessemer.cornerstone.time._Durations
import java.time.Duration
import java.util.concurrent.ThreadLocalRandom

object _Random {
    @JvmStatic
    @JvmOverloads
    fun randomLong(min: Long = Long.MIN_VALUE, max: Long = Long.MAX_VALUE): Long {
        var boundedMax = max;
        if(max != Long.MAX_VALUE) {
            boundedMax = max + 1
        }

        return ThreadLocalRandom.current().nextLong(min, boundedMax)
    }

    @JvmStatic
    @JvmOverloads
    fun randomDuration(min: Duration = _Durations.ZERO, max: Duration = _Durations.MAX_VALUE): Duration {
        return Duration.ofMillis(randomLong(min.toMillis(), max.toMillis()))
    }
}