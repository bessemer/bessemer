package bessemer.cornerstone.uri

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.domain.Outcome
import bessemer.cornerstone.string._Strings
import bessemer.cornerstone.uri.impl.UriComponentsBuilder
import com.google.common.base.Charsets
import org.apache.commons.lang3.StringUtils
import java.net.URLDecoder
import java.net.URLEncoder
import java.util.Optional

// TODO there are "optional" fields on these DTOs... using ? makes it a pain to call from java and using Optional makes it a pain in Kotlin... we need to find an answer here
data class UriScheme(val value: String) {
    override fun toString(): String {
        return _Uris.encode(value)
    }

    companion object {
        val HTTP = UriScheme("http")
        val HTTPS = UriScheme("https")

        fun parse(scheme: String?): Optional<UriScheme> {
            if(_Strings.isBlank(scheme)) {
               return Optional.empty()
            }

            return Optional.of(UriScheme(scheme!!))
        }
    }
}

data class UriAuthentication(
        val principal: String,
        val password: String) {

    override fun toString(): String {
        return _Uris.encode(this.principal) + ":" + _Uris.encode(this.password)
    }
}

data class UriAuthority
    @JvmOverloads
    constructor(
            val hostname: String,
            val port: Int? = null,
            val authentication: UriAuthentication? = null) {

    override fun toString(): String {
        var url = ""

        val authentication = this.authentication
        if (authentication != null) {
            url = "$url$authentication@"
        }

        url += _Uris.encode(hostname)

        val port = this.port
        if (port != null) {
            url = "$url:$port"
        }

        return url
    }
}

data class UriLocation
    @JvmOverloads
    constructor(
            val path: List<String> = _Lists.of(),
            val parameters: Map<String, List<String>> = _Maps.of(),
            val fragment: String? = null) {

    companion object {
        @JvmField
        val EMPTY: UriLocation = UriLocation()
    }

    override fun toString(): String {
        var url = ""

        if (path.isEmpty()) {
            url = "/"
        } else {
            for (pathString in path) {
                url = url + '/' + _Uris.encode(pathString)
            }
        }

        if (!parameters.isEmpty()) {
            url = "$url?"
            url += buildQueryParameters(parameters)
        }

        val fragment = this.fragment
        if (fragment != null) {
            url = url + '#'.toString() + fragment
        }

        return url
    }

    private fun buildQueryParameters(query: Map<String, List<String>>): String? {
        var parameters = ""
        for ((key, values) in query.entries) {
            if(values.isEmpty()) {
                parameters += _Uris.encode(key)
                parameters += "&"
            }
            else {
                values.forEach {
                    parameters += _Uris.encode(key)
                    parameters = parameters + "=" + _Uris.encode(it)
                    parameters += "&"
                }
            }
        }
        parameters = StringUtils.removeEnd(parameters, "&")
        return parameters
    }
}

data class UriContainer(
        val scheme: UriScheme? = null,
        val authority: UriAuthority? = null,
        val location: UriLocation = UriLocation.EMPTY) {

    companion object {
        @JvmField
        val EMPTY: UriContainer = UriContainer()
    }

    fun scheme(scheme: String) = copy(scheme = UriScheme.parse(scheme).orElse(null))

    fun scheme(scheme: UriScheme?) = copy(scheme = scheme)

    fun authority(authority: UriAuthority) = copy(authority = authority)

    fun location(location: UriLocation) = copy(location = location)

    override fun toString(): String {
        var url = ""
        val scheme = this.scheme
        if (scheme != null) {
            url = "$url$scheme:"
        }

        val authority = this.authority
        if (authority != null) {
            if (scheme != null) {
                url = "$url//"
            }

            url += authority.toString()
        }

        url += this.location.toString()
        return url
    }
}

object _Uris {
    @JvmStatic
    fun encode(string: String) = URLEncoder.encode(string, Charsets.UTF_8.name())

    @JvmStatic
    fun decode(string: String) = URLDecoder.decode(string, Charsets.UTF_8.name())

    @JvmStatic
    fun parse(uriString: String?): UriContainer {
        val result = parseResult(uriString)
        if(result.isError()) {
            throw result.error
        }

        return result.result
    }

    @JvmStatic
    fun parseResult(uriString: String?): Outcome<UriContainer, Exception> {
        var uriString = uriString
        try {
            if (StringUtils.isBlank(uriString)) {
                uriString = "/"
            }

            val components = UriComponentsBuilder.fromUriString(uriString).build()

            var authentication: UriAuthentication? = null
            val rawAuthentication = _Strings.splitPair(components.userInfo, ":")
            if (rawAuthentication.isPresent) {
                authentication = UriAuthentication(rawAuthentication.get().first, rawAuthentication.get().second)
            }

            var authority: UriAuthority? = null
            if (!_Strings.isEmpty(components.host)) {
                var port: Int? = null
                if(components.port != -1) {
                    port = components.port
                }
                authority = UriAuthority(components.host, port, authentication)
            }

            var scheme: UriScheme? = null
            if (components.scheme != null) {
                scheme = UriScheme(components.scheme)
            }

            val location = UriLocation(components.pathSegments, components.queryParams, components.fragment)
            return Outcome.success(UriContainer(scheme, authority, location))
        } catch (e: Exception) {
            return Outcome.error(e)
        }
    }

    @JvmStatic
    fun build() = UriContainer.EMPTY
}