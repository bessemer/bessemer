package bessemer.cornerstone.data

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.collection._Sets
import bessemer.cornerstone.collection._Streams
import bessemer.cornerstone.domain.Serializer
import com.google.common.collect.HashBiMap
import java.util.stream.Stream

class AlwaysEmptyMutableLexicon private constructor(): MutablePagingLexicon<Any, Any> {
    override fun findEach(keys: Stream<Any>): Stream<Pair<Any, Any>> {
        return _Streams.empty()
    }

    override fun put(elements: Stream<Pair<Any, Any>>) {
        // Do nothing
    }

    override fun remove(keys: Stream<Any>) {
        // Do nothing
    }

    override fun removeAll() {
        // Do nothing
    }

    override fun entries(): PagingDataSet<Pair<Any, Any>> = _DataSets.empty()

    companion object {
        private val INSTANCE = AlwaysEmptyMutableLexicon()

        @Suppress("UNCHECKED_CAST")
        @JvmStatic
        fun <KEY: Any, T: Any> get(): MutableIndex<KEY, T> {
            return INSTANCE as MutableIndex<KEY, T>
        }
    }
}

open class MapLexicon<T: Any, N: Any>(private val map: Map<T, N>): Lexicon<T, N> {
    override fun findEach(keys: Stream<T>): Stream<Pair<T, N>> {
        return keys.filter { map.containsKey(it) } .map { it to map.getValue(it) }
    }

    override fun entries(): DataSet<Pair<T, N>> = _DataSets.from { _Maps.stream(map) }
}

open class PagingMapLexicon<T: Any, N: Any>(map: Map<T, N>): MapLexicon<T, N>(map), PagingLexicon<T, N> {
    private val dataSet: PagingDataSet<Pair<T, N>> = _DataSets.from(map.keys.map { it to map[it]!! })

    override fun entries(): PagingDataSet<Pair<T, N>> = dataSet
}

class MapMutableLexicon<T: Any, N: Any>(
        private val map: MutableMap<T, N> = _Maps.emptyMutable()): MapLexicon<T, N>(map), MutableLexicon<T, N> {

    override fun put(elements: Stream<Pair<T, N>>) {
        elements.forEach { map[it.first] = it.second }
    }

    override fun remove(keys: Stream<T>) {
        keys.forEach { map.remove(it) }
    }

    override fun removeAll() = map.clear()
}

class MapSupplierMutableLexicon<T: Any, N: Any>(
        private val mapSupplier: () -> MutableMap<T, N>): MutableLexicon<T, N> {
    override fun findEach(keys: Stream<T>): Stream<Pair<T, N>> {
        val map = mapSupplier()
        return keys.filter { map.containsKey(it) } .map { it to map.getValue(it) }
    }

    override fun put(elements: Stream<Pair<T, N>>) {
        val map = mapSupplier()
        elements.forEach { map[it.first] = it.second }
    }

    override fun remove(keys: Stream<T>) {
        val map = mapSupplier()
        keys.forEach { map.remove(it) }
    }

    override fun removeAll() = mapSupplier().clear()

    override fun entries(): DataSet<Pair<T, N>> {
        val map = mapSupplier()
        return _DataSets.from { _Maps.stream(map) }
    }
}

class ChainingMutableIndex<T: Any, N: Any>(
        private val targetIndex: Index<T, N>,
        private val delegateIndex: MutableIndex<T, N>,
        private val propagate: Boolean = false): MutableIndex<T, N> {
    override fun findEach(keys: Stream<T>): Stream<Pair<T, N>> {
        // FUTURE any way to stream this?
        var mementoSet = _Sets.collect(keys)
        val targetItems = targetIndex.findEach(mementoSet)
        mementoSet = _Sets.remove(mementoSet, targetItems.keys)

        val delegateItems = delegateIndex.findEach(mementoSet)

        if (targetIndex is MutableIndex<*, *>) {
            (targetIndex as MutableIndex<T, N>).put(delegateItems)
        }

        val resultMap = _Maps.merge(targetItems, delegateItems)
        return _Maps.stream(resultMap)
    }

    override fun put(elements: Stream<Pair<T, N>>) {
        if (targetIndex is MutableIndex<*, *>) {
            (targetIndex as MutableIndex<T, N>).put(elements)
        }
        else if(!propagate) {
            throw IllegalStateException("Cannot chain put call to immutable index where propagate = false... nowhere to put the value!")
        }

        if(propagate) {
            delegateIndex.put(elements)
        }
    }

    override fun remove(keys: Stream<T>) {
        if (targetIndex is MutableIndex<*, *>) {
            (targetIndex as MutableIndex<T, N>).remove(keys)
        }

        delegateIndex.remove(keys)
    }

    override fun removeAll() {
        if (targetIndex is MutableIndex<*, *>) {
            (targetIndex as MutableIndex<T, N>).removeAll()
        }

        delegateIndex.removeAll()
    }
}

class SerializingMutableIndex<N: Any, T: Any, K: Any, V: Any>(
        private val delegate: MutableIndex<K, V>,
        private val keySerializer: (N) -> K,
        private val valueSerializer: Serializer<T, V>) : MutableIndex<N, T> {
    override fun findEach(keys: Stream<N>): Stream<Pair<N, T>> {
        // TODO this needs to be upgraded to invoke the serializer in batch operation rather than one at a time
        // TODO we should consider where BiMaps go in the utilities packages... if they don't go in _Maps multimaps should also probably be factored out
        val keyMap = HashBiMap.create(_Maps.mapValues(keys, { keySerializer(it) }))
        val results = this.delegate.findEach(keys.map { keyMap[it]!! })

        return results.map { result ->
            val key = keyMap.inverse()[result.first]!!
            val value = valueSerializer.deserialize(result.second)

            return@map key to value
        }
    }

    override fun put(elements: Stream<Pair<N, T>>) {
        val serializedElements = elements.map { result ->
            keySerializer(result.first) to valueSerializer.serialize(result.second)
        }

        this.delegate.put(serializedElements)
    }

    override fun remove(keys: Stream<N>) {
        return delegate.remove(keys.map { keySerializer(it) })
    }

    override fun removeAll() = delegate.removeAll()
}

class LoadingMutableIndex<N: Any, T: Any>(
        private val delegate: MutableIndex<N, T>,
        private val loader: (Collection<N>) -> Map<N, T>) : MutableIndex<N, T> by delegate {

    override fun findEach(keys: Stream<N>): Stream<Pair<N, T>> {
        val results = _Maps.ofMutable<N, T>()
        val uncachedItems = _Lists.ofMutable<N>()

        val cachedResults = delegate.findEach(keys.collect(_Lists.collect()))

        for (item in keys) {
            if (!cachedResults.containsKey(item)) {
                uncachedItems.add(item)
            } else {
                results[item] = cachedResults[item]!!
            }
        }

        if (!uncachedItems.isEmpty()) {
            val loadedResults = loader(uncachedItems)
            delegate.put(loadedResults)
            results.putAll(loadedResults)
        }

        return _Maps.stream(results)
    }
}