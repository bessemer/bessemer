package bessemer.cornerstone.data

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.collection._Streams
import bessemer.cornerstone.data.pagination.PageData
import bessemer.cornerstone.data.pagination.PageRequest
import bessemer.cornerstone.data.pagination.SimplePageRequest
import java.util.stream.Stream

object _DataSets {
    private val EMPTY_DATA_SET: DataSet<Any> = EmptyDataSet()

    @Suppress("UNCHECKED_CAST")
    @JvmStatic
    fun <T: Any> empty(): PagingDataSet<T> = EMPTY_DATA_SET as PagingDataSet<T>

    @JvmStatic
    fun <T: Any> from(iterable: Iterable<T>): DataSet<T> = from { _Streams.from(iterable) }

    @JvmStatic
    fun <T: Any> from(data: List<T>): PagingDataSet<T> = SimplePagingDataSet(data)

    @JvmStatic
    fun <T: Any> from(supplier: () -> Stream<T>): DataSet<T> = SimpleDataSet(supplier)
}

private class EmptyDataSet: PagingDataSet<Any> {
    private val lexicon: Lexicon<PageRequest, PageData<Any>> = _Indexes.from(_Maps.empty())

    override fun pageLexicon(): Lexicon<PageRequest, PageData<Any>> = lexicon
}

class SimpleDataSet<T: Any>(private val supplier: () -> Stream<T>): DataSet<T> {
    override fun stream() = supplier()
}

class SimplePagingDataSet<T: Any>(
        data: List<T>,
        pageSize: Int? = null): PagingDataSet<T> {

    private val pageIndex: Lexicon<PageRequest, PageData<T>>

    init {
        if(data.isEmpty()) {
            pageIndex = MapLexicon(_Maps.empty())
        }
        else {
            val partitions = _Streams.partition(_Streams.from(data), pageSize ?: data.size)

            val pageMap = _Streams.mapIndexed(partitions) { it, index ->
                val partition = it.collect(_Lists.collect())
                val pageRequest: PageRequest = SimplePageRequest(index, partition.size)
                pageRequest to PageData(pageRequest, partition)
            }.collect(_Maps.collect())

            pageIndex = MapLexicon(pageMap)
        }
    }

    override fun pageLexicon(): Lexicon<PageRequest, PageData<T>> = pageIndex
}