package bessemer.cornerstone.data

import bessemer.cornerstone.domain.Referent
import bessemer.cornerstone.reflection.TypeToken
import java.util.*

interface Bag<T: Any>: Index<T, Any> {
    fun <N> findReferent(key: T, type: TypeToken<N>): Referent<N>

    fun <N> find(key: T, type: TypeToken<N>): Optional<N>
}

interface MutableBag<T: Any>: Bag<T>, MutableIndex<T, Any> {

}