package bessemer.cornerstone.data

import bessemer.cornerstone.collection._Streams
import java.util.stream.Stream

object _Indexes {
    private val EMPTY_LEXICON: PagingLexicon<Any, Any> = EmptyLexicon()

    @Suppress("UNCHECKED_CAST")
    @JvmStatic
    fun <KEY: Any, T: Any> empty(): PagingLexicon<KEY, T> = EMPTY_LEXICON as PagingLexicon<KEY, T>

    @JvmStatic
    fun <KEY: Any, T: Any> from(map: Map<KEY, T>): PagingLexicon<KEY, T> = PagingMapLexicon(map)
}

private class EmptyLexicon: PagingLexicon<Any, Any> {
    override fun findEach(keys: Stream<Any>): Stream<Pair<Any, Any>> = _Streams.empty()

    override fun entries(): PagingDataSet<Pair<Any, Any>> = _DataSets.empty()
}