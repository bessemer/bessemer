package bessemer.cornerstone.data.pagination

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.expression.ExpressionField
import bessemer.cornerstone.expression.ExpressionValue
import bessemer.cornerstone.memento.MementoMetadata

object Pagination {
    const val DEFAULT_PAGE_SIZE: Int = 50

    @JvmStatic
    fun <T, N> mapData(pageData: PageData<T>, mapper: (T) -> N): PageData<N> {
        return PageData(pageData.pageRequest, pageData.data.stream().map(mapper).collect(_Lists.collect()))
    }
}

data class PageData<T>(val pageRequest: PageRequest, val data: List<T>)

interface PageRequest

data class SimplePageRequest(
        val page: Int,
        val size: Int) : PageRequest

data class KeySetPageRequest(
        val key: ExpressionValue? = null,
        val keyField: ExpressionField,
        val size: Int) : PageRequest

interface PaginationStrategy<T: Any> {
    val pageSize: Int
}

data class SimplePaginationStrategy<T: Any>(override val pageSize: Int): PaginationStrategy<T>

// TODO keyset pagination needs to support custom sorts somehow... going to need to think about that one
// TODO keyset pagination needs to support composite keys
data class KeySetPaginationStrategy<T: Any>(
        override val pageSize: Int,
        val keyMetadata: MementoMetadata<T, out Any>): PaginationStrategy<T> {

    val keyField = keyMetadata.mementoField
    val keyType = keyMetadata.mementoType

    fun getKey(element: T): Any {
        return keyMetadata.mementizer(element)
    }
}