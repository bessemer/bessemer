package bessemer.cornerstone.data

import bessemer.cornerstone._Equality
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.domain.RelativePosition
import java.util.Comparator
import java.util.function.Function

data class RelativeOrder(val target: Any, val position: RelativePosition) {
    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(target: Any, position: RelativePosition = RelativePosition.AFTER) = RelativeOrder(target, position)
    }
}

object RelativeOrdering {
    fun <T> sort(elements: List<T>, ordering: Function<T, Set<RelativeOrder>>, targetExtractor: Function<T, Any>): List<T> {
        val comparator = bessemer.cornerstone._Equality.biDirectionalComparator<T>(Comparator { base, target ->
            val baseOrderings = ordering.apply(base)

            for (potentialOrdering in baseOrderings) {
                if (potentialOrdering.target == targetExtractor.apply(target)) {
                    if (potentialOrdering.position == RelativePosition.BEFORE) {
                        return@Comparator - 1
                    }
                    if (potentialOrdering.position == RelativePosition.AFTER) {
                        return@Comparator 1
                    }
                }
            }

            return@Comparator 0
        })

        return relativeSort(elements, comparator)
    }

    fun <T> relativeSort(elements: List<T>, comparator: Comparator<T>): List<T> {
        val inputsToProcess = _Lists.copyMutable(elements)
        val result = _Lists.ofMutable<T>()

        do {
            val iterator = inputsToProcess.iterator()
            while (iterator.hasNext()) {
                val input = iterator.next()

                if (inputsToProcess.stream().allMatch { inputToProcess -> comparator.compare(input, inputToProcess) <= 0 }) {
                    result.add(input)
                    iterator.remove()
                }
            }
        } while (!inputsToProcess.isEmpty())

        return result
    }
}