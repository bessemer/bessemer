package bessemer.cornerstone.data

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Sets
import bessemer.cornerstone.data.pagination.PageData
import bessemer.cornerstone.data.pagination.PageRequest
import java.util.stream.Stream

@FunctionalInterface
interface DataSet<T: Any> {
    fun stream(): Stream<T>

    @JvmDefault
    fun toList(): List<T> = this.stream().collect(_Lists.collect())

    @JvmDefault
    fun toSet(): Set<T> = this.stream().collect(_Sets.collect())

    @JvmDefault
    fun size(): Int = toList().size
}

interface DataSetDelegator<T: Any>: DataSet<T> {
    val dataSetDelegate: DataSet<T>

    @JvmDefault
    override fun stream() = dataSetDelegate.stream()

    @JvmDefault
    override fun toList() = dataSetDelegate.toList()

    @JvmDefault
    override fun toSet() = dataSetDelegate.toSet()

    @JvmDefault
    override fun size() = dataSetDelegate.size()
}

interface PagingDataSet<T: Any> : DataSet<T> {
    @JvmDefault
    override fun stream(): Stream<T> {
        return this.pageLexicon().stream().flatMap{ page -> page.data.stream() }
    }

    fun pageLexicon(): Lexicon<PageRequest, PageData<T>>

    companion object {
        fun <T: Any> of(func: () -> Lexicon<PageRequest, PageData<T>>): PagingDataSet<T> {
            return object : PagingDataSet<T> {
                override fun pageLexicon() = func()
            }
        }
    }
}

interface PagingDataSetAdapter<T: Any> : PagingDataSet<T> {
    fun findPages(keys: Stream<PageRequest>): Stream<Pair<PageRequest, PageData<T>>>

    fun streamPageData(): Stream<Pair<PageRequest, PageData<T>>>

    @JvmDefault
    fun streamPages(): Stream<PageRequest> = streamPageData().map{ data -> data.first }

    @JvmDefault
    fun pageCount() = pageLexicon().toList().size

    @JvmDefault
    override fun pageLexicon(): Lexicon<PageRequest, PageData<T>> {
        return object : Lexicon<PageRequest, PageData<T>> {
            override fun findEach(keys: Stream<PageRequest>) = findPages(keys)

            override fun entries(): DataSet<Pair<PageRequest, PageData<T>>> {
                return object : DataSet<Pair<PageRequest, PageData<T>>> {
                    override fun stream() = streamPageData()

                    override fun size() = this@PagingDataSetAdapter.pageCount()
                }
            }

            override fun keys(): DataSet<PageRequest> {
                return object : DataSet<PageRequest> {
                    override fun stream() = streamPages()

                    override fun size() = this@PagingDataSetAdapter.pageCount()
                }
            }

            override fun size() = this@PagingDataSetAdapter.pageCount()
        }
    }
}