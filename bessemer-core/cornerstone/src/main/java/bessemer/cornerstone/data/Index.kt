package bessemer.cornerstone.data

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.collection._Streams
import bessemer.cornerstone.data.pagination.PageData
import bessemer.cornerstone.data.pagination.PageRequest
import bessemer.cornerstone.data.pagination.Pagination
import java.util.Optional
import java.util.stream.Stream

interface Index<KEY: Any, T: Any> {
    fun findEach(keys: Stream<KEY>): Stream<Pair<KEY, T>>

    @JvmDefault
    fun findEach(keys: Collection<KEY>): Map<KEY, T> = this.findEach(keys.stream()).collect(_Maps.collect())

    @JvmDefault
    fun find(key: KEY): T? = this.findEach(_Lists.of(key))[key]

    @JvmDefault
    fun findOptional(key: KEY) = Optional.ofNullable(this.find(key))

    @JvmDefault
    fun contains(key: KEY) = this.find(key) != null
}

interface IndexDelegator<KEY: Any, T: Any>: Index<KEY, T> {
    val indexDelegate: Index<KEY, T>

    @JvmDefault
    override fun findEach(keys: Stream<KEY>) = indexDelegate.findEach(keys)

    @JvmDefault
    override fun findEach(keys: Collection<KEY>): Map<KEY, T> = indexDelegate.findEach(keys)

    @JvmDefault
    override fun find(key: KEY) = indexDelegate.find(key)
}

interface MutableIndex<KEY: Any, T: Any>: Index<KEY, T> {
    fun put(elements: Stream<Pair<KEY, T>>)

    fun remove(keys: Stream<KEY>)

    fun removeAll()

    @JvmDefault
    fun remove(key: KEY) = remove(_Streams.of(key))

    @JvmDefault
    fun remove(keys: Iterable<KEY>) = remove(_Streams.from(keys))

    @JvmDefault
    fun put(key: KEY, value: T) = put(_Lists.of(key to value))

    @JvmDefault
    fun put(elements: Iterable<Pair<KEY, T>>) = put(_Streams.from(elements))

    @JvmDefault
    fun put(vararg elements: Pair<KEY, T>) = put(_Lists.of(*elements))

    @JvmDefault
    fun put(elements: Map<KEY, T>) = put(_Maps.stream(elements))
}

interface Lexicon<KEY: Any, T: Any>: Index<KEY, T>, DataSet<T> {
    fun entries(): DataSet<Pair<KEY, T>>

    @JvmDefault
    fun keys(): DataSet<KEY> = _DataSets.from { this.entries().stream().map{it.first} }

    @JvmDefault
    override fun stream(): Stream<T> = this.entries().stream().map{it.second}
}

interface LexiconDelegator<KEY: Any, T: Any>: Lexicon<KEY, T> {
    val lexiconDelegate: Lexicon<KEY, T>

    @JvmDefault
    override fun findEach(keys: Stream<KEY>) = lexiconDelegate.findEach(keys)

    @JvmDefault
    override fun entries() = lexiconDelegate.entries()

    @JvmDefault
    override fun keys() = lexiconDelegate.keys()

    @JvmDefault
    override fun stream() = lexiconDelegate.stream()
}

interface MutableLexicon<KEY: Any, T: Any>: Lexicon<KEY, T>, MutableIndex<KEY, T>

interface PagingLexicon<KEY: Any, T: Any> : Lexicon<KEY, T>, PagingDataSet<T> {
    override fun entries(): PagingDataSet<Pair<KEY, T>>

    @JvmDefault
    override fun stream(): Stream<T> {
        return this.pageLexicon().stream().flatMap { page -> page.data.stream() }
    }

    @JvmDefault
    override fun keys(): PagingDataSet<KEY> {
        return PagingDataSet.of {
            object : Lexicon<PageRequest, PageData<KEY>> {
                override fun entries(): DataSet<Pair<PageRequest, PageData<KEY>>> = _DataSets.from {
                    this@PagingLexicon.entries().pageLexicon().entries().stream()
                            .map { entry ->
                                entry.first to Pagination.mapData(entry.second, { it.first })
                            }
                }

                override fun findEach(keys: Stream<PageRequest>): Stream<Pair<PageRequest, PageData<KEY>>> {
                    return this@PagingLexicon.entries().pageLexicon().findEach(keys)
                            .map { entry ->
                                entry.first to Pagination.mapData(entry.second, { it.first })
                            }
                }
            }
        }
    }

    @JvmDefault
    override fun pageLexicon(): Lexicon<PageRequest, PageData<T>> {
        return object : Lexicon<PageRequest, PageData<T>> {
            override fun keys(): DataSet<PageRequest> {
                return this@PagingLexicon.entries().pageLexicon().keys()
            }

            override fun entries(): DataSet<Pair<PageRequest, PageData<T>>> = _DataSets.from {
                this@PagingLexicon.entries().pageLexicon().entries().stream()
                        .map { entry ->
                            entry.first to Pagination.mapData(entry.second, { it.second })
                        }
            }

            override fun findEach(keys: Stream<PageRequest>): Stream<Pair<PageRequest, PageData<T>>> {
                return this@PagingLexicon.entries().pageLexicon().findEach(keys)
                        .map { entry ->
                            entry.first to Pagination.mapData(entry.second, { it.second })
                        }
            }
        }
    }
}

interface PagingLexiconDelegator<KEY: Any, T: Any>: PagingLexicon<KEY, T> {
    val lexiconDelegate: PagingLexicon<KEY, T>

    @JvmDefault
    override fun findEach(keys: Stream<KEY>) = lexiconDelegate.findEach(keys)

    @JvmDefault
    override fun entries() = lexiconDelegate.entries()

    @JvmDefault
    override fun keys() = lexiconDelegate.keys()

    @JvmDefault
    override fun pageLexicon() = lexiconDelegate.pageLexicon()

    @JvmDefault
    override fun stream() = lexiconDelegate.stream()
}

interface MutablePagingLexicon<KEY: Any, T: Any> : MutableLexicon<KEY, T>, PagingLexicon<KEY, T> {

}