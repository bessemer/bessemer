package bessemer.cornerstone.collection

import bessemer.cornerstone.function._Predicates
import bessemer.cornerstone.util._Objects
import bessemer.cornerstone.util.toOptional
import com.google.common.collect.ImmutableList
import com.google.common.collect.Lists
import java.util.*
import java.util.stream.Collector
import java.util.stream.Stream

object _Lists {
    /**
    ======================================================
    ======================================================
    LIST CONSTRUCTORS
    ======================================================
    ======================================================
     */
    @JvmStatic
    @SafeVarargs
    fun <T> of(vararg targets: T) = ImmutableList.copyOf(targets)

    @JvmStatic
    fun <T: Any> empty() = of<T>()

    @JvmStatic
    fun <T> ofMutable(vararg targets: T): MutableList<T> {
        return Lists.newArrayList(*targets)
    }

    @JvmStatic
    fun <T> emptyMutable() = ofMutable<T>()

    @JvmStatic
    fun <T: Any> copy(elements: Iterable<T>?): List<T> {
        if(elements == null) {
            return empty()
        }

        return ImmutableList.copyOf(elements)
    }

    @JvmStatic
    fun <T: Any> copy(elements: Stream<T>?): List<T> {
        if(elements == null) {
            return empty()
        }

        return elements.collect(collect())
    }

    @JvmStatic
    fun <T: Any> coerce(iterable: Iterable<T>): List<T> {
        if (iterable is List<T> && _Iterables.isMutable(iterable)) {
            return iterable
        }
        else {
            return copy(iterable)
        }
    }

    @JvmStatic
    fun <T: Any> defaulted(list: List<T>?) = _Objects.defaulted(list, of())

    @JvmSynthetic
    fun <T> first(iterable: Iterable<T>?) = _Iterables.first(iterable)

    @JvmStatic
    @JvmName("first")
    fun <T: Any> optionalFirst(iterable: Iterable<T>?) = _Iterables.optionalFirst(iterable)

    @JvmSynthetic
    fun <T> last(list: List<T>?): T? {
        if (list == null || list.isEmpty()) {
            return null
        }

        return list[list.size - 1]
    }

    @JvmStatic
    @JvmName("last")
    fun <T: Any> optionalLast(list: List<T>?) = last(list).toOptional()

    @JvmStatic
    fun <T: Any> rest(iterable: Iterable<T>) = copy(_Iterables.rest(iterable))

    @JvmSynthetic
    fun <T> only(iterable: Iterable<T>?) = _Iterables.only(iterable)

    @JvmStatic
    @JvmName("only")
    fun <T: Any> optionalOnly(iterable: Iterable<T>?) = _Iterables.optionalOnly(iterable)

    @JvmStatic
    fun <T: Any> take(iterable: Iterable<T>, size: Long) = _Streams.take(iterable, size).collect(collect())

    @JvmStatic
    fun <T: Any> take(stream: Stream<T>, size: Long) = _Streams.take(stream, size).collect(collect())

    @JvmStatic
    fun <T> reverse(list: List<T>): List<T> = Lists.reverse(list)

    @JvmStatic
    fun <T: Any> append(collection: Collection<T>, vararg others: T) = concat(collection, of(*others))

    @JvmStatic
    @SafeVarargs
    fun <T: Any> concat(vararg lists: Iterable<T>) = concat(of(*lists))

    @JvmStatic
    fun <T> concat(lists: Iterable<Iterable<T>>): List<T> {
        val builder = ImmutableList.builder<T>()
        for (list in lists) {
            builder.addAll(list)
        }

        return builder.build()
    }

    @JvmStatic
    @SafeVarargs
    fun <T: Any> removeAll(elements: Iterable<T>, vararg remove: T): List<T> =
            removeAll(elements, of(*remove))

    @JvmStatic
    fun <T: Any> removeAll(elements: Iterable<T>, remove: Collection<T>): List<T> =
            filter(elements, _Predicates.invert(_Predicates.matchElements(remove)))

    @JvmStatic
    fun <T: Any> filter(elements: Iterable<T>, filter: (T) -> Boolean) =
            _Streams.from(elements).filter(filter).collect(collect())

    @JvmStatic
    fun <T> partition(elements: Iterable<T>, size: Int): List<List<T>> =
            _Streams.partition(_Streams.from(elements), size).map { it.collect(collect()) }.collect(collect())

    @JvmStatic
    fun <T> partition(elements: Iterable<T>, predicate: (T) -> Boolean): Pair<List<T>, List<T>> {
        val partition = _Streams.partition(_Streams.from(elements), predicate)
        return partition.first.collect(collect()) to partition.second.collect(collect())
    }

    @JvmStatic
    fun <T: Comparable<T>> sort(iterable: Iterable<T>) = sort(iterable, Comparator.naturalOrder())

    @JvmStatic
    fun <T> sort(iterable: Iterable<T>, comparator: Comparator<T>): List<T> {
        val list = copyMutable(iterable)
        Collections.sort(list, comparator)
        return list
    }

    @JvmStatic
    fun <T> copyMutable(elements: Iterable<T>?): MutableList<T> {
        if(elements == null) {
            return Lists.newArrayList()
        }

        return Lists.newArrayList(elements)
    }

    @JvmStatic
    fun <T> defaultMutable(list: MutableList<T>) = _Objects.defaulted(list, ofMutable())

    @JvmStatic
    fun <T> collect(stream: Stream<T>) = stream.collect(collect())

    @JvmStatic
    fun <T> collect(): Collector<@JvmWildcard T, *, List<T>> = collect(of())

    @JvmStatic
    fun <T> collectMutable(): Collector<@JvmWildcard T, *, List<T>> = collect(ofMutable())

    @JvmStatic
    fun <T, TARGET_TYPE : List<T>> collect(collectionTarget: TARGET_TYPE): Collector<@JvmWildcard T, Any, TARGET_TYPE> = _Collectors.buildIterableCollector(
            collectionTarget = collectionTarget,
            extractor = { element, builder -> builder(element) }
    )

    @JvmStatic
    fun <T: Any, N> collect(extractor: (N, (T) -> Unit) -> Unit): Collector<@JvmWildcard N, Any, List<T>> = _Collectors.buildIterableCollector(
            collectionTarget = of(),
            extractor = extractor
    )
}

fun <T: Any> Stream<T>.toList(): List<T> = this.collect(_Lists.collect())