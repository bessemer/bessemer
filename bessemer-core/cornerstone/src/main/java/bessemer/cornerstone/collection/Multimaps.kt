package bessemer.cornerstone.collection

import bessemer.cornerstone.domain.Referent
import com.google.common.collect.ImmutableListMultimap
import com.google.common.collect.ImmutableMultimap
import com.google.common.collect.ImmutableSetMultimap
import com.google.common.collect.Multimap
import com.google.common.collect.Multimaps
import java.util.stream.Collector
import java.util.stream.Stream
import kotlin.collections.Map.Entry

// TODO add append as a utility
// TODO add transform utility
object _Multimaps {
    /**
    ======================================================
    ======================================================
    MULTI-MAP ACCESSORS
    ======================================================
    ======================================================
     */
    fun <T, N> stream(map: Multimap<T, N>): Stream<Pair<T, N>> {
        return map.entries().stream().map { entry -> entry.key to entry.value }
    }

    @JvmStatic
    fun <T, N> getReferent(map: Multimap<T, N>, key: T): Referent<Collection<N>> {
        if (!map.containsKey(key)) {
            return Referent.absent()
        }

        return Referent.present(map.get(key))
    }

    /**
    ======================================================
    ======================================================
    COLLECTION MULTI-MAP CONSTRUCTORS
    ======================================================
    ======================================================
     */
    @JvmStatic
    fun <T, N> of(): Multimap<T, N> = ImmutableMultimap.of()

    @JvmStatic
    fun <T, N> empty(): Multimap<T, N> = of()

    @JvmStatic
    fun <T, N> of(k1: T, v1: N) = from(k1 to v1)

    @JvmStatic
    fun <T, N> of(k1: T, v1: N, k2: T, v2: N) = from(k1 to v1, k2 to v2)

    @JvmStatic
    fun <T, N> of(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N) = from(k1 to v1, k2 to v2, k3 to v3)

    @JvmStatic
    fun <T, N> of(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N, k4: T, v4: N) = from(k1 to v1, k2 to v2, k3 to v3, k4 to v4)

    @JvmStatic
    fun <T, N> ofMutable(): Multimap<T, N> {
        return Multimaps.newMultimap(_Maps.ofMutable<T, Collection<N>>(), { _Lists.ofMutable() })
    }

    @JvmStatic
    fun <T, N> emptyMutable(): Multimap<T, N> = ofMutable()

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N) = fromMutable(k1 to v1)

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N, k2: T, v2: N) = fromMutable(k1 to v1, k2 to v2)

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N) = fromMutable(k1 to v1, k2 to v2, k3 to v3)

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N, k4: T, v4: N) = fromMutable(k1 to v1, k2 to v2, k3 to v3, k4 to v4)

    @JvmStatic
    fun <T, N> from(vararg elements: Pair<T, N>) = from(_Lists.of(*elements))

    @JvmStatic
    fun <T, N> from(elements: Iterable<Pair<T, N>>): Multimap<T, N> {
        val builder = ImmutableMultimap.builder<T, N>()
        for (element in elements) {
            builder.put(element.first, element.second)
        }
        return builder.build()
    }

    @JvmStatic
    fun <T, N> fromMutable(vararg elements: Pair<T, N>) = _Maps.from(_Lists.of(*elements))

    @JvmStatic
    fun <T, N> fromMutable(elements: Iterable<Pair<T, N>>): Multimap<T, N> {
        val multimap = ofMutable<T, N>()
        for (element in elements) {
            multimap.put(element.first, element.second)
        }
        return multimap
    }

    @JvmStatic
    fun <T, N> append(map: Multimap<T, N>, k1: T, v1: N) = append(map, k1 to v1)

    @JvmStatic
    fun <T, N> append(map: Multimap<T, N>, k1: T, v1: N, k2: T, v2: N) = append(map, k1 to v1, k2 to v2)

    @JvmStatic
    fun <T, N> append(map: Multimap<T, N>, k1: T, v1: N, k2: T, v2: N, k3: T, v3: N) = append(map, k1 to v1, k2 to v2, k3 to v3)

    @JvmStatic
    fun <T, N> append(map: Multimap<T, N>, k1: T, v1: N, k2: T, v2: N, k3: T, v3: N, k4: T, v4: N) = append(map, k1 to v1, k2 to v2, k3 to v3, k4 to v4)

    @JvmStatic
    fun <T, N> append(map: Multimap<T, N>, vararg elements: Pair<T, N>) = append(map, _Lists.of(*elements))

    @JvmStatic
    fun <T, N> append(map: Multimap<T, N>, elements: Iterable<Pair<T, N>>) = append(map, _Streams.from(elements))

    @SafeVarargs
    fun <T, N> append(map: Multimap<T, N>, elements: Stream<Pair<T, N>>) = _Streams.concat(stream(map), elements).collect(collect())

    @JvmStatic
    fun <T, N> remove(map: Multimap<T, N>, vararg elements: T) = remove(map, _Sets.of(*elements))

    @SafeVarargs
    fun <T, N> remove(map: Multimap<T, N>, elements: Stream<T>) = remove(map, elements.collect(_Sets.collect()))

    @JvmStatic
    fun <T, N> remove(map: Multimap<T, N>, elements: Iterable<T>): Multimap<T, N> {
        val set = _Sets.coerce(elements)
        return _Streams.from(map).filter { !set.contains(it.first) }.collect(collect())
    }

    @JvmStatic
    fun <T, N> copy(map: Map<T, N>): Multimap<T, N> = _Maps.stream(map).collect(collect())

    @JvmStatic
    fun <T, N> copy(map: Multimap<T, N>): Multimap<T, N> {
        if (map is ImmutableMultimap<*, *>) {
            return map
        }

        return ImmutableMultimap.copyOf(map)
    }

    @JvmStatic
    fun <T, N> mapKeys(list: Collection<T>, keyMapper: (T) -> N) = mapKeys(list.stream(), keyMapper)

    @JvmStatic
    fun <T, N> mapKeys(list: Stream<T>, keyMapper: (T) -> N) = map(list, keyMapper, { it })

    @JvmStatic
    fun <L, T, N> map(
            list: Collection<L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N) = map(list.stream(), keyExtractor, valueExtractor)

    @JvmStatic
    fun <L, T, N> map(
            list: Stream<out L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N): Multimap<T, N> {

        val map = ofMutable<T, N>()

        list.forEach { item ->
            val key = keyExtractor(item)
            val value = valueExtractor(item)

            map.put(key, value)
        }

        return map
    }

    /**
    ======================================================
    ======================================================
    TRANSFORM
    ======================================================
    ======================================================
     */
    @JvmStatic
    fun <T, N, S, R, M : Multimap<S, R>> transformEntriesBatch(target: Stream<Entry<T, N>>, transformer: (Stream<Entry<T, N>>) -> Stream<Pair<S, R>>, collector: Collector<Pair<S, R>, *, M>): M =
            transformer(target).collect(collector)

    @JvmStatic
    fun <T, N, S, R> transformEntriesBatch(target: Stream<Entry<T, N>>, transformer: (Stream<Entry<T, N>>) -> Stream<Pair<S, R>>): Multimap<S, R> =
            transformer(target).collect(collect())

    @JvmStatic
    fun <T, N, S, R, M : Multimap<S, R>> transformBatch(target: Stream<Pair<T, N>>, transformer: (Stream<Pair<T, N>>) -> Stream<Pair<S, R>>, collector: Collector<Pair<S, R>, *, M>): M =
            transformer(target).collect(collector)

    @JvmStatic
    fun <T, N, S, R, M : Multimap<S, R>> transformBatch(target: Multimap<T, N>, transformer: (Stream<Pair<T, N>>) -> Stream<Pair<S, R>>, collector: Collector<Pair<S, R>, *, M>): M =
            transformBatch(stream(target), transformer, collector)

    @JvmStatic
    fun <T, N, S, R, M : Multimap<S, R>> transform(target: Multimap<T, N>, transformer: (Pair<T, N>) -> Pair<S, R>, collector: Collector<Pair<S, R>, *, M>): M =
            transformBatch(target, { it.map(transformer) }, collector)

    @JvmStatic
    fun <T, N, S, R> transform(target: Multimap<T, N>, transformer: (Pair<T, N>) -> Pair<S, R>): Multimap<S, R> =
            transform(target, transformer, collect())

    @JvmStatic
    fun <T, N, S, R> transform(target: Map<T, N>, transformer: (Pair<T, N>) -> Pair<S, R>): Multimap<S, R> =
            transform(copy(target), transformer)

    @JvmStatic
    fun <T, N, S> transformValues(target: Multimap<T, N>, transformer: (T, N) -> S): Multimap<T, S> =
            transform(target, { it.first to transformer(it.first, it.second) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Map<T, N>, transformer: (T, N) -> S): Multimap<T, S> =
            transform(target, { it.first to transformer(it.first, it.second) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Multimap<T, N>, transformer: (N) -> S): Multimap<T, S> =
            transformValues(target, { _, value -> transformer(value) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Map<T, N>, transformer: (N) -> S): Multimap<T, S> =
            transformValues(target, { _, value -> transformer(value) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Multimap<T, N>, valuesMap: Map<N, S>): Multimap<T, S> =
            transformValues(target, { it -> valuesMap.getValue(it) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Map<T, N>, valuesMap: Map<N, S>): Multimap<T, S> =
            transformValues(target, { it -> valuesMap.getValue(it) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Map<T, N>, valuesMap: Multimap<N, S>): Multimap<T, S> =
            transformEntriesBatch(
                    target = target.entries.stream(),
                    transformer = { batch -> batch.flatMap { element -> valuesMap.get(element.value).stream().map { element.key to it } } })

    @JvmStatic
    fun <T, N, S> transformValues(target: Multimap<T, N>, valuesMap: Multimap<N, S>): Multimap<T, S> =
            transformEntriesBatch(
                    target = target.entries().stream(),
                    transformer = { batch -> batch.flatMap { element -> valuesMap.get(element.value).stream().map { element.key to it } } })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Multimap<T, N>, transformer: (T, N) -> S): Multimap<S, N> =
            transform(target, { transformer(it.first, it.second) to it.second })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Map<T, N>, transformer: (T, N) -> S): Multimap<S, N> =
            transform(target, { transformer(it.first, it.second) to it.second })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Multimap<T, N>, transformer: (T) -> S): Multimap<S, N> =
            transformKeys(target, { key, _ -> transformer(key) })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Map<T, N>, transformer: (T) -> S): Multimap<S, N> =
            transformKeys(target, { key, _ -> transformer(key) })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Multimap<T, N>, keysMap: Map<T, S>): Multimap<S, N> =
            transformKeys(target, { it -> keysMap.getValue(it) })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Map<T, N>, keysMap: Map<T, S>): Multimap<S, N> =
            transformKeys(target, { it -> keysMap.getValue(it) })

    @JvmStatic
    fun <T, N, S> fuse(target: Map<T, N>, valuesMap: Map<N, S>) = transformValues(target, valuesMap)

    @JvmStatic
    fun <T, N, S> fuse(target: Map<T, N>, valuesMap: Multimap<N, S>) = transformValues(target, valuesMap)

    @JvmStatic
    fun <T, N, S> fuse(target: Multimap<T, N>, valuesMap: Map<N, S>) = transformValues(target, valuesMap)

    @JvmStatic
    fun <T, N, S> fuse(target: Multimap<T, N>, valuesMap: Multimap<N, S>) = transformValues(target, valuesMap)

    /**
    ======================================================
    ======================================================
    MERGE
    ======================================================
    ======================================================
     */
    @JvmStatic
    @SafeVarargs
    fun <T, N> merge(vararg maps: Multimap<T, N>) = merge(_Lists.of(*maps))

    @JvmStatic
    fun <T, N> merge(maps: Collection<Multimap<T, N>>) = merge(maps.stream())

    // TODO this isn't actually immutable
    @JvmStatic
    fun <T, N> merge(maps: Stream<Multimap<T, N>>): Multimap<T, N> {
        val result = _Multimaps.ofMutable<T, N>()

        maps.forEach {
            it.entries().forEach { entry ->
                result.put(entry.key, entry.value)
            }
        }

        return result
    }

    @JvmStatic
    @SafeVarargs
    fun <T, N> mergeMutable(vararg maps: Multimap<T, N>) = mergeMutable(_Lists.of(*maps))

    @JvmStatic
    fun <T, N> mergeMutable(maps: Collection<Multimap<T, N>>) = mergeMutable(maps.stream())

    @JvmStatic
    fun <T, N> mergeMutable(maps: Stream<Multimap<T, N>>): Multimap<T, N> {
        val result = _Multimaps.ofMutable<T, N>()

        maps.forEach {
            it.entries().forEach { entry ->
                result.put(entry.key, entry.value)
            }
        }

        return result
    }

    /**
    ======================================================
    ======================================================
    COLLECTION MUTABLE MULTI-MAP CONSTRUCTORS
    ======================================================
    ======================================================
     */

    @JvmStatic
    fun <T, N> copyMutable(map: Multimap<T, N>): Multimap<T, N> {
        val multimap = ofMutable<T, N>()
        multimap.putAll(map)
        return map
    }

    @JvmStatic
    fun <L, T, N> multimapMutable(list: Collection<L>, keyExtractor: java.util.function.Function<L, T>, valueExtractor: java.util.function.Function<L, N>): Multimap<T, N> = multimapMutable(list.stream(), keyExtractor, valueExtractor)

    @JvmStatic
    fun <L, T, N> multimapMutable(stream: Stream<L>, keyExtractor: java.util.function.Function<L, T>, valueExtractor: java.util.function.Function<L, N>): Multimap<T, N> {
        return stream.map { element -> (keyExtractor.apply(element) to valueExtractor.apply(element)) }
                .collect(collectMutable())
    }

    @JvmStatic
    fun <T, N> multimapValuesMutable(list: Collection<T>, valueExtractor: java.util.function.Function<T, N>): Multimap<T, N> = multimapValuesMutable(list.stream(), valueExtractor)

    @JvmStatic
    fun <T, N> multimapValuesMutable(stream: Stream<T>, valueExtractor: java.util.function.Function<T, N>): Multimap<T, N> {
        return stream.map { element -> (element to valueExtractor.apply(element)) }
                .collect(collectMutable())
    }

    @JvmStatic
    fun <T, N> multimapKeysMutable(list: Collection<T>, keyExtractor: java.util.function.Function<T, N>): Multimap<N, T> = multimapKeysMutable(list.stream(), keyExtractor)

    @JvmStatic
    fun <T, N> multimapKeysMutable(stream: Stream<T>, keyExtractor: java.util.function.Function<T, N>): Multimap<N, T> {
        return stream.map { element -> (keyExtractor.apply(element) to element) }
                .collect(collectMutable())
    }

    @JvmStatic
    fun <T, N> collect(): Collector<Pair<@JvmWildcard T, @JvmWildcard N>, *, Multimap<T, N>> = collect(of())

    @JvmStatic
    fun <T, N> collectList(): Collector<Pair<@JvmWildcard T, @JvmWildcard N>, *, ImmutableListMultimap<T, N>> = collect(ImmutableListMultimap.of())

    @JvmStatic
    fun <T, N> collectSet(): Collector<Pair<@JvmWildcard T, @JvmWildcard N>, *, ImmutableSetMultimap<T, N>> = collect(ImmutableSetMultimap.of())

    @JvmStatic
    fun <T, N> collectEntries(): Collector<Entry<@JvmWildcard T, @JvmWildcard N>, *, Multimap<T, N>> = collectEntries(of())

    @JvmStatic
    fun <T, N> collectBulk(): Collector<Pair<@JvmWildcard T, @JvmWildcard Iterable<N>>, *, Multimap<T, N>> = collectBulk(of())

    @JvmStatic
    fun <T, N> collectStreams(): Collector<Pair<@JvmWildcard T, @JvmWildcard Stream<N>>, *, Multimap<T, N>> = collectStreams(of())

    @JvmStatic
    fun <T, N> collectMutable(): Collector<Pair<@JvmWildcard T, @JvmWildcard N>, *, Multimap<T, N>> = collect(ofMutable())

    @JvmStatic
    fun <T, N> collectEntriesMutable(): Collector<Entry<@JvmWildcard T, @JvmWildcard N>, *, Multimap<T, N>> = collectEntries(ofMutable())

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE : Multimap<KEY_TYPE, VALUE_TYPE>>
            collect(collectionTarget: TARGET_TYPE): Collector<Pair<@JvmWildcard KEY_TYPE, @JvmWildcard VALUE_TYPE>, Any, TARGET_TYPE> = _Collectors.buildMultimapCollector(
            collectionTarget = collectionTarget,
            extractor = { entry, builder -> builder(entry.first, entry.second) }
    )

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE>
            collect(extractor: (Pair<@JvmWildcard KEY_TYPE, @JvmWildcard VALUE_TYPE>, (KEY_TYPE, VALUE_TYPE) -> Unit) -> Unit): Collector<Pair<KEY_TYPE, VALUE_TYPE>, Any, Multimap<KEY_TYPE, VALUE_TYPE>> = _Collectors.buildMultimapCollector(
            collectionTarget = of(),
            extractor = extractor
    )

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE : Multimap<KEY_TYPE, VALUE_TYPE>>
            collectEntries(collectionTarget: TARGET_TYPE): Collector<Entry<@JvmWildcard KEY_TYPE, @JvmWildcard VALUE_TYPE>, Any, TARGET_TYPE> = _Collectors.buildMultimapCollector(
            collectionTarget = collectionTarget,
            extractor = { entry, builder -> builder(entry.key, entry.value) }
    )

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE : Multimap<KEY_TYPE, VALUE_TYPE>>
            collectBulk(collectionTarget: TARGET_TYPE): Collector<Pair<@JvmWildcard KEY_TYPE, @JvmWildcard Iterable<VALUE_TYPE>>, Any, TARGET_TYPE> = _Collectors.buildMultimapCollector(
            collectionTarget = collectionTarget,
            extractor = { entry, builder -> entry.second.forEach { builder(entry.first, it) } }
    )

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE : Multimap<KEY_TYPE, VALUE_TYPE>>
            collectStreams(collectionTarget: TARGET_TYPE): Collector<Pair<@JvmWildcard KEY_TYPE, @JvmWildcard Stream<VALUE_TYPE>>, Any, TARGET_TYPE> = _Collectors.buildMultimapCollector(
            collectionTarget = collectionTarget,
            extractor = { entry, builder -> entry.second.forEach { builder(entry.first, it) } }
    )
}