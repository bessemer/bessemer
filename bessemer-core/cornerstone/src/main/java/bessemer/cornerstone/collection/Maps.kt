package bessemer.cornerstone.collection

import bessemer.cornerstone.domain.Referent
import com.google.common.collect.BiMap
import com.google.common.collect.ImmutableBiMap
import com.google.common.collect.ImmutableMap
import com.google.common.collect.Maps
import java.util.Collections
import java.util.HashMap
import java.util.stream.Collector
import java.util.stream.Stream
import kotlin.collections.Map.Entry

object _Maps {
    /**
    ======================================================
    ======================================================
        MAP ACCESSORS
    ======================================================
    ======================================================
     */
    @JvmStatic
    fun <T, N> stream(map: Map<T, N>): Stream<Pair<T, N>> = _Streams.from(map)

    @JvmStatic
    fun <T, N> getReferent(map: Map<T, N?>, key: T): Referent<N> {
        if (!map.containsKey(key)) {
            return Referent.absent()
        }

        return Referent.present(map[key])
    }

    /**
    ======================================================
    ======================================================
        MAP CONSTRUCTORS
    ======================================================
    ======================================================
     */
    @JvmStatic
    fun <T, N> of(): Map<T, N> = ImmutableMap.of<T, N>()

    @JvmStatic
    fun <T, N> empty() = of<T, N>()

    @JvmStatic
    fun <T, N> of(k1: T, v1: N) = from(k1 to v1)

    @JvmStatic
    fun <T, N> of(k1: T, v1: N, k2: T, v2: N) = from(k1 to v1, k2 to v2)

    @JvmStatic
    fun <T, N> of(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N) = from(k1 to v1, k2 to v2, k3 to v3)

    @JvmStatic
    fun <T, N> of(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N, k4: T, v4: N) = from(k1 to v1, k2 to v2, k3 to v3, k4 to v4)

    @JvmStatic
    fun <T, N> ofMutable(): MutableMap<T, N> = Maps.newHashMap()

    @JvmStatic
    fun <T, N> emptyMutable(): MutableMap<T, N> = ofMutable()

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N) = fromMutable(k1 to v1)

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N, k2: T, v2: N) = fromMutable(k1 to v1, k2 to v2)

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N) = fromMutable(k1 to v1, k2 to v2, k3 to v3)

    @JvmStatic
    fun <T, N> ofMutable(k1: T, v1: N, k2: T, v2: N, k3: T, v3: N, k4: T, v4: N) = fromMutable(k1 to v1,  k2 to v2, k3 to v3, k4 to v4)

    @JvmStatic
    @SafeVarargs
    fun <T, N> from(vararg elements: Pair<T, N>) = from(_Lists.of(*elements))

    @JvmStatic
    fun <T, N> from(elements: Iterable<Pair<T, N>>) = from(_Streams.from(elements))

    @JvmStatic
    fun <T, N> from(elements: Stream<Pair<T, N>>) = elements.collect(collect())

    @JvmStatic
    @SafeVarargs
    fun <T, N> fromMutable(vararg elements: Pair<T, N>) = fromMutable(_Lists.of(*elements))

    @JvmStatic
    fun <T, N> fromMutable(elements: Iterable<Pair<T, N>>) = fromMutable(_Streams.from(elements))

    @JvmStatic
    fun <T, N> fromMutable(elements: Stream<Pair<T, N>>) = elements.collect(collectMutable())

    @JvmStatic
    fun <T, N> append(map: Map<T, N>, k1: T, v1: N) = append(map, k1 to v1)

    @JvmStatic
    fun <T, N> append(map: Map<T, N>, k1: T, v1: N, k2: T, v2: N) = append(map, k1 to v1, k2 to v2)

    @JvmStatic
    fun <T, N> append(map: Map<T, N>, k1: T, v1: N, k2: T, v2: N, k3: T, v3: N) = append(map, k1 to v1, k2 to v2, k3 to v3)

    @JvmStatic
    fun <T, N> append(map: Map<T, N>, k1: T, v1: N, k2: T, v2: N, k3: T, v3: N, k4: T, v4: N) = append(map, k1 to v1, k2 to v2, k3 to v3, k4 to v4)

    @JvmStatic
    @SafeVarargs
    fun <T, N> append(map: Map<T, N>, vararg elements: Pair<T, N>) = append(map, _Lists.of(*elements))

    @JvmStatic
    fun <T, N> append(map: Map<T, N>, elements: Iterable<Pair<T, N>>) = append(map, _Streams.from(elements))

    @SafeVarargs
    fun <T, N> append(map: Map<T, N>, elements: Stream<Pair<T, N>>) = _Streams.concat(_Maps.stream(map), elements).collect(_Maps.collect())

    @JvmStatic
    @SafeVarargs
    fun <T, N> remove(map: Map<T, N>, vararg elements: T) = remove(map, _Sets.of(*elements))

    @SafeVarargs
    fun <T, N> remove(map: Map<T, N>, elements: Stream<T>) = remove(map, elements.collect(_Sets.collect()))

    @JvmStatic
    fun <T, N> remove(map: Map<T, N>, elements: Iterable<T>): Map<T, N> {
        val set = _Sets.coerce(elements)
        return _Streams.from(map).filter { !set.contains(it.first) }.collect(collect())
    }

    @JvmStatic
    fun <T, N> copy(map: Map<T, N>): Map<T, N> {
        if (map is ImmutableMap<*, *>) {
            return map
        }

        return ImmutableMap.copyOf(map)
    }

    @JvmStatic
    fun <T, N> mapKeys(list: Collection<T>, keyMapper: (T) -> N) = mapKeys(list.stream(), keyMapper)

    @JvmStatic
    fun <T, N> mapKeys(list: Stream<T>, keyMapper: (T) -> N) = mapKeys(list, keyMapper, { _, second -> second })

    @JvmStatic
    fun <T, N> mapKeys(list: Collection<T>, keyMapper: (T) -> N, mergeFunction: (T, T) -> T) = mapKeys(list.stream(), keyMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapKeys(list: Stream<T>, keyMapper: (T) -> N, mergeFunction: (T, T) -> T) = mapKeys(list, keyMapper, { _, first, second -> mergeFunction(first, second) })

    @JvmStatic
    fun <T, N> mapKeys(list: Collection<T>, keyMapper: (T) -> N, mergeFunction: (N, T, T) -> T) = mapKeys(list.stream(), keyMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapKeys(list: Stream<T>, keyMapper: (T) -> N, mergeFunction: (N, T, T) -> T) =  map(list, keyMapper, { it }, mergeFunction)

    @JvmStatic
    fun <T, N> mapValues(list: Collection<T>, valueMapper: (T) -> N) = mapValues(list.stream(), valueMapper)

    @JvmStatic
    fun <T, N> mapValues(list: Stream<T>, valueMapper: (T) -> N) = mapValues(list, valueMapper, { _, second -> second })

    @JvmStatic
    fun <T, N> mapValues(list: Collection<T>, valueMapper: (T) -> N, mergeFunction: (N, N) -> N) = mapValues(list.stream(), valueMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapValues(list: Stream<T>, valueMapper: (T) -> N, mergeFunction: (N, N) -> N) = mapValues(list, valueMapper, { _, first, second -> mergeFunction(first, second) })

    @JvmStatic
    fun <T, N> mapValues(list: Collection<T>, valueMapper: (T) -> N, mergeFunction: (T, N, N) -> N) = mapValues(list.stream(), valueMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapValues(list: Stream<T>, valueMapper: (T) -> N, mergeFunction: (T, N, N) -> N) =  map(list, { it }, valueMapper, mergeFunction)

    @JvmStatic
    fun <L, T, N> map(
            list: Collection<L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N) = map(list.stream(), keyExtractor, valueExtractor)

    @JvmStatic
    fun <L, T, N> map(
            list: Stream<out L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N) = map(list, keyExtractor, valueExtractor, { _: N, secondValue: N -> secondValue })

    @JvmStatic
    fun <L, T, N> map(
            list: Collection<L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (N, N) -> N) = map(list.stream(), keyExtractor, valueExtractor, mergeFunction)

    @JvmStatic
    fun <L, T, N> map(
            list: Stream<out L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (N, N) -> N) = map(list, keyExtractor, valueExtractor, { _: T, firstValue: N, secondValue: N -> mergeFunction(firstValue, secondValue) })

    @JvmStatic
    fun <L, T, N> map(
            list: Collection<L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (T, N, N) -> N) = map(list.stream(), keyExtractor, valueExtractor, mergeFunction)

    @JvmStatic
    fun <L, T, N> map(
            list: Stream<out L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (T, N, N) -> N): Map<T, N> {

        val map = ofMutable<T, N>()

        list.forEach { item ->
            val key = keyExtractor(item)
            val value = valueExtractor(item)

            if (map.containsKey(key)) {
                map[key] = mergeFunction(key, map[key]!!, value)
            } else {
                map[key] = value
            }
        }

        return map
    }

    // TODO should we add some kind of transformOptional? it was pretty useful on other projects
    @JvmStatic
    fun <T, N, S, R, M: Map<S, R>> transformBatch(target: Map<T, N>, transformer: (Stream<Pair<T, N>>) -> Stream<Pair<S, R>>, collector: Collector<Pair<S, R>, *, M>): M =
            transformer(_Maps.stream(target)).collect(collector)

    @JvmStatic
    fun <T, N, S, R, M: Map<S, R>> transform(target: Map<T, N>, transformer: (Pair<T, N>) -> Pair<S, R>, collector: Collector<Pair<S, R>, *, M>): M =
            _Maps.transformBatch(target, { it.map(transformer) }, collector)

    @JvmStatic
    fun <T, N, S, R> transform(target: Map<T, N>, transformer: (Pair<T, N>) -> Pair<S, R>): Map<S, R> {
        return target.entries.stream()
                .map { entry -> entry.key to entry.value }
                .map(transformer)
                .collect(collect())
    }

    @JvmStatic
    fun <T, N, S> transformValues(target: Map<T, N>, transformer: (T, N) -> S): Map<T, S> =
            transform(target, { it.first to transformer(it.first, it.second) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Map<T, N>, transformer: (N) -> S): Map<T, S> =
            transformValues(target, { _, value -> transformer(value) })

    @JvmStatic
    fun <T, N, S> transformValues(target: Map<T, N>, valuesMap: Map<N, S>): Map<T, S> =
            transformValues(target, { x -> valuesMap.getValue(x) })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Map<T, N>, transformer: (T, N) -> S): Map<S, N> =
            transform(target, { transformer(it.first, it.second) to it.second })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Map<T, N>, transformer: (T) -> S): Map<S, N> =
            transformKeys(target, { key, _ -> transformer(key) })

    @JvmStatic
    fun <T, N, S> transformKeys(target: Map<T, N>, keysMap: Map<T, S>): Map<S, N> =
            transformKeys(target, { x -> keysMap.getValue(x) })

    @JvmStatic
    fun <T, N, S> fuse(target: Map<T, N>, valuesMap: Map<N, S>) = transformValues(target, valuesMap)

    @JvmStatic
    @SafeVarargs
    fun <T, N> merge(vararg maps: Map<T, N>) = merge(_Lists.of(*maps))

    @JvmStatic
    @SafeVarargs
    fun <T, N> merge(mergeFunction: (N, N) -> N = { first, _ -> first }, vararg maps: Map<T, N>) = merge(_Lists.of(*maps), mergeFunction)

    @JvmStatic
    @SafeVarargs
    fun <T, N> merge(mergeFunction: (T, N, N) -> N, vararg maps: Map<T, N>) = merge(_Lists.of(*maps), mergeFunction)

    @JvmStatic
    @JvmOverloads
    fun <T, N> merge(maps: Collection<Map<T, N>>, mergeFunction: (N, N) -> N = { first, _ -> first }) = merge(maps.stream(), mergeFunction)

    @JvmStatic
    fun <T, N> merge(maps: Collection<Map<T, N>>, mergeFunction: (T, N, N) -> N) = merge(maps.stream(), mergeFunction)

    @JvmStatic
    @JvmOverloads
    fun <T, N> merge(maps: Stream<Map<T, N>>, mergeFunction: (N, N) -> N = { first, _ -> first }) = merge(maps, { _, firstValue, secondValue -> mergeFunction(firstValue, secondValue) })

    @JvmStatic
    fun <T, N> merge(maps: Stream<Map<T, N>>, mergeFunction: (T, N, N) -> N): Map<T, N> {
        val result = _Maps.ofMutable<T, N>()

        maps.forEach {
            it.entries.forEach { entry ->
                if(result.containsKey(entry.key)) {
                    result[entry.key] = mergeFunction(entry.key, result[entry.key]!!, entry.value)
                }
                else {
                    result[entry.key] = entry.value
                }
            }
        }

        return result
    }

    @JvmStatic
    fun <T, N> copyMutable(map: Map<T, N>): MutableMap<T, N> = HashMap(map)

    @JvmStatic
    fun <T, N> mapKeysMutable(list: Collection<T>, keyMapper: (T) -> N) = mapKeysMutable(list.stream(), keyMapper)

    @JvmStatic
    fun <T, N> mapKeysMutable(list: Stream<T>, keyMapper: (T) -> N) = mapKeysMutable(list, keyMapper, { _, second -> second })

    @JvmStatic
    fun <T, N> mapKeysMutable(list: Collection<T>, keyMapper: (T) -> N, mergeFunction: (T, T) -> T) = mapKeysMutable(list.stream(), keyMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapKeysMutable(list: Stream<T>, keyMapper: (T) -> N, mergeFunction: (T, T) -> T) = mapKeysMutable(list, keyMapper, { _, first, second -> mergeFunction(first, second) })

    @JvmStatic
    fun <T, N> mapKeysMutable(list: Collection<T>, keyMapper: (T) -> N, mergeFunction: (N, T, T) -> T) = mapKeysMutable(list.stream(), keyMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapKeysMutable(list: Stream<T>, keyMapper: (T) -> N, mergeFunction: (N, T, T) -> T) =  mapMutable(list, keyMapper, { it }, mergeFunction)

    @JvmStatic
    fun <T, N> mapValuesMutable(list: Collection<T>, valueMapper: (T) -> N) = mapValues(list.stream(), valueMapper)

    @JvmStatic
    fun <T, N> mapValuesMutable(list: Stream<T>, valueMapper: (T) -> N) = mapValuesMutable(list, valueMapper, { _, second -> second })

    @JvmStatic
    fun <T, N> mapValuesMutable(list: Collection<T>, valueMapper: (T) -> N, mergeFunction: (N, N) -> N) = mapValuesMutable(list.stream(), valueMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapValuesMutable(list: Stream<T>, valueMapper: (T) -> N, mergeFunction: (N, N) -> N) = mapValuesMutable(list, valueMapper, { _, first, second -> mergeFunction(first, second) })

    @JvmStatic
    fun <T, N> mapValuesMutable(list: Collection<T>, valueMapper: (T) -> N, mergeFunction: (T, N, N) -> N) = mapValuesMutable(list.stream(), valueMapper, mergeFunction)

    @JvmStatic
    fun <T, N> mapValuesMutable(list: Stream<T>, valueMapper: (T) -> N, mergeFunction: (T, N, N) -> N) =  mapMutable(list, { it }, valueMapper, mergeFunction)

    @JvmStatic
    fun <L, T, N> mapMutable(
            list: Collection<L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N) = mapMutable(list.stream(), keyExtractor, valueExtractor)

    @JvmStatic
    fun <L, T, N> mapMutable(
            list: Stream<out L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N) = mapMutable(list, keyExtractor, valueExtractor, { _: N, secondValue: N -> secondValue })

    @JvmStatic
    fun <L, T, N> mapMutable(
            list: Collection<L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (N, N) -> N) = mapMutable(list.stream(), keyExtractor, valueExtractor, mergeFunction)

    @JvmStatic
    fun <L, T, N> mapMutable(
            list: Stream<out L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (N, N) -> N) = mapMutable(list, keyExtractor, valueExtractor, { _: T, firstValue: N, secondValue: N -> mergeFunction(firstValue, secondValue) })

    @JvmStatic
    fun <L, T, N> mapMutable(
            list: Collection<L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (T, N, N) -> N) = mapMutable(list.stream(), keyExtractor, valueExtractor, mergeFunction)

    @JvmStatic
    fun <L, T, N> mapMutable(
            list: Stream<out L>,
            keyExtractor: (L) -> T,
            valueExtractor: (L) -> N,
            mergeFunction: (T, N, N) -> N): MutableMap<T, N> {

        val map = ofMutable<T, N>()

        list.forEach { item ->
            val key = keyExtractor(item)
            val value = valueExtractor(item)

            if (map.containsKey(key)) {
                map[key] = mergeFunction(key, map[key]!!, value)
            } else {
                map[key] = value
            }
        }

        return Collections.unmodifiableMap(map)
    }

    @JvmStatic
    @JvmOverloads
    @SafeVarargs
    fun <T, N> mergeMutable(vararg maps: Map<T, N>, mergeFunction: (N, N) -> N = { first, _ -> first }) = merge(_Lists.of(*maps), mergeFunction)

    @JvmStatic
    @SafeVarargs
    fun <T, N> mergeMutable(vararg maps: Map<T, N>, mergeFunction: (T, N, N) -> N) = merge(_Lists.of(*maps), mergeFunction)

    @JvmStatic
    fun <T, N> mergeMutable(maps: Collection<Map<T, N>>, mergeFunction: (N, N) -> N) = merge(maps.stream(), mergeFunction)

    @JvmStatic
    fun <T, N> mergeMutable(maps: Collection<Map<T, N>>, mergeFunction: (T, N, N) -> N) = merge(maps.stream(), mergeFunction)

    @JvmStatic
    fun <T, N> mergeMutable(maps: Stream<Map<T, N>>, mergeFunction: (N, N) -> N) = merge(maps, { _, firstValue, secondValue -> mergeFunction(firstValue, secondValue) })

    @JvmStatic
    fun <T, N> mergeMutable(maps: Stream<Map<T, N>>, mergeFunction: (T, N, N) -> N): MutableMap<T, N> {
        val result = _Maps.ofMutable<T, N>()

        maps.forEach {
            it.entries.forEach { entry ->
                if(result.containsKey(entry.key)) {
                    result[entry.key] = mergeFunction(entry.key, result[entry.key]!!, entry.value)
                }
                else {
                    result[entry.key] = entry.value
                }
            }
        }

        return result
    }

    @JvmStatic
    fun <T, N> collect(): Collector<Pair<@JvmWildcard T, @JvmWildcard N>, *, Map<T, N>> = collect(of())

    @JvmStatic
    fun <T, N> collectEntries(): Collector<Entry<@JvmWildcard T, @JvmWildcard N>, *, Map<T, N>> = collectEntries(of())

    @JvmStatic
    fun <T, N> collectMutable(): Collector<Pair<@JvmWildcard T, @JvmWildcard N>, *, Map<T, N>> = collect(ofMutable())

    @JvmStatic
    fun <T, N> collectEntriesMutable(): Collector<Entry<@JvmWildcard T, @JvmWildcard N>, *, Map<T, N>> = collectEntries(ofMutable())

    @JvmStatic
    fun <T, N> collectBiMap(): Collector<Pair<@JvmWildcard T, @JvmWildcard N>, *, BiMap<T, N>> = collect(ImmutableBiMap.of())

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE : Map<KEY_TYPE, VALUE_TYPE>>
            collect(collectionTarget: TARGET_TYPE): Collector<Pair<@JvmWildcard KEY_TYPE, @JvmWildcard VALUE_TYPE>, Any, TARGET_TYPE> = _Collectors.buildMapCollector(
            collectionTarget = collectionTarget,
            extractor = { entry, builder -> builder(entry.first, entry.second) }
    )

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE>
            collect(extractor: (Pair<@JvmWildcard KEY_TYPE, @JvmWildcard VALUE_TYPE>, (KEY_TYPE, VALUE_TYPE) -> Unit) -> Unit): Collector<Pair<KEY_TYPE, VALUE_TYPE>, Any, Map<KEY_TYPE, VALUE_TYPE>> = _Collectors.buildMapCollector(
            collectionTarget = of(),
            extractor = extractor
    )

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE : Map<KEY_TYPE, VALUE_TYPE>>
            collectEntries(collectionTarget: TARGET_TYPE): Collector<Entry<@JvmWildcard KEY_TYPE, @JvmWildcard VALUE_TYPE>, Any, TARGET_TYPE> = _Collectors.buildMapCollector(
            collectionTarget = collectionTarget,
            extractor = { entry, builder -> builder(entry.key, entry.value) }
    )
}