package bessemer.cornerstone.collection

import bessemer.cornerstone.util.toOptional
import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableSet
import com.google.common.collect.Iterables
import com.google.common.collect.Lists
import java.util.*

object _Iterables {
    @JvmSynthetic
    fun <T> first(iterable: Iterable<T>?): T? {
        if (iterable == null) {
            return null
        }

        return Iterables.getFirst(iterable, null)
    }

    @JvmStatic
    @JvmName("first")
    fun <T: Any> optionalFirst(iterable: Iterable<T>?) = first(iterable).toOptional()

    @JvmSynthetic
    fun <T> only(iterable: Iterable<T>?): T? {
        if (iterable == null) {
            return null
        }

        val i = iterable.iterator()
        if (!i.hasNext()) {
            return null
        }

        val result = i.next()

        if (i.hasNext()) {
            throw IllegalArgumentException("Expected iterable to contain one value but has many.")
        }

        return result
    }

    @JvmStatic
    @JvmName("only")
    fun <T: Any> optionalOnly(iterable: Iterable<T>?) = only(iterable).toOptional()

    @JvmStatic
    fun <T> take(iterable: Iterable<T>, size: Long) = _Streams.take(iterable, size).collect(_Lists.collect())

    @JvmStatic
    fun <T> rest(iterable: Iterable<T>?): Iterable<T> {
        if(iterable == null) {
            return _Lists.of()
        }

        return Iterable {
            val delegate = iterable.iterator()
            if (delegate.hasNext()) {
                delegate.next()
            }

            object : Iterator<T> {
                override fun hasNext(): Boolean {
                    return delegate.hasNext()
                }

                override fun next(): T {
                    return delegate.next()
                }
            }
        }
    }

    @JvmStatic
    fun <T> reverse(list: List<T>): Iterable<T> = Lists.reverse(list)

    // TODO the immutability check needs to be made better here, probably
    @JvmStatic
    fun isMutable(collection: Iterable<Any?>): Boolean {
        val isImmutable =
                collection is ImmutableList ||
                        collection is ImmutableSet

        return !isImmutable
    }

//    @JvmStatic
//    @SafeVarargs
//    fun <T> concat(vararg lists: Iterable<T>) = concat(of(*lists))
//
//    @JvmStatic
//    fun <T> concat(lists: Iterable<Iterable<T>>): List<T> {
//        val builder = ImmutableList.builder<T>()
//        for (list in lists) {
//            builder.addAll(list)
//        }
//
//        return builder.build()
//    }
}