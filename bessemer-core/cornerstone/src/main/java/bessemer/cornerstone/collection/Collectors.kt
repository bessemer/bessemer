package bessemer.cornerstone.collection

import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import com.google.common.collect.ImmutableBiMap
import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableListMultimap
import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableMultimap
import com.google.common.collect.ImmutableSet
import com.google.common.collect.ImmutableSetMultimap
import com.google.common.collect.ImmutableSetMultimap.Builder
import com.google.common.collect.ImmutableSortedSet
import com.google.common.collect.Multimap
import java.util.function.BiConsumer
import java.util.function.BinaryOperator
import java.util.function.Function
import java.util.function.Supplier
import java.util.stream.Collector

object _Collectors {
    @JvmStatic
    fun <ELEMENT_TYPE, TARGET_TYPE: Iterable<ELEMENT_TYPE>, COLLECTEE_TYPE> buildIterableCollector(
            collectionTarget: TARGET_TYPE,
            extractor: (COLLECTEE_TYPE, (ELEMENT_TYPE) -> Unit) -> Unit
    ): Collector<COLLECTEE_TYPE, Any, TARGET_TYPE> {
        return when (collectionTarget) {
            is ImmutableSortedSet<*> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableSortedSet<Comparable<ELEMENT_TYPE>>> = Collector.of(
                        Supplier<ImmutableSortedSet.Builder<Comparable<ELEMENT_TYPE>>> { ImmutableSortedSet.Builder({ first, second -> first.compareTo(second as ELEMENT_TYPE) }) },
                        BiConsumer<ImmutableSortedSet.Builder<Comparable<ELEMENT_TYPE>>, COLLECTEE_TYPE> { builder, element -> extractor(element, { builder.add(it as Comparable<ELEMENT_TYPE>) }) },
                        BinaryOperator<ImmutableSortedSet.Builder<Comparable<ELEMENT_TYPE>>> { first, second ->
                            first.addAll(second.build())
                            return@BinaryOperator first
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is ImmutableList<*> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableList<ELEMENT_TYPE>> = Collector.of(
                        Supplier<ImmutableList.Builder<ELEMENT_TYPE>> { ImmutableList.Builder() },
                        BiConsumer<ImmutableList.Builder<ELEMENT_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, { builder.add(it) }) },
                        BinaryOperator<ImmutableList.Builder<ELEMENT_TYPE>> { first, second ->
                            first.addAll(second.build())
                            return@BinaryOperator first
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is ImmutableSet<*> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableSet<ELEMENT_TYPE>> = Collector.of(
                        Supplier<ImmutableSet.Builder<ELEMENT_TYPE>> { ImmutableSet.Builder() },
                        BiConsumer<ImmutableSet.Builder<ELEMENT_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, { builder.add(it) }) },
                        BinaryOperator<ImmutableSet.Builder<ELEMENT_TYPE>> { first, second ->
                            first.addAll(second.build())
                            return@BinaryOperator first
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is MutableSet<*> -> {
                val collector: Collector<COLLECTEE_TYPE, *, MutableSet<ELEMENT_TYPE>> = Collector.of(
                        Supplier<MutableSet<ELEMENT_TYPE>> { _Sets.emptyMutable() },
                        BiConsumer<MutableSet<ELEMENT_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, { builder.add(it) }) },
                        BinaryOperator<MutableSet<ELEMENT_TYPE>> { first, second ->
                            first.addAll(second)
                            return@BinaryOperator first
                        }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is MutableList<*> -> {
                val collector: Collector<COLLECTEE_TYPE, *, MutableList<ELEMENT_TYPE>> = Collector.of(
                        Supplier<MutableList<ELEMENT_TYPE>> { _Lists.emptyMutable() },
                        BiConsumer<MutableList<ELEMENT_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, { builder.add(it) }) },
                        BinaryOperator<MutableList<ELEMENT_TYPE>> { first, second ->
                            first.addAll(second)
                            return@BinaryOperator first
                        }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is Set<*> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableSet<ELEMENT_TYPE>> = Collector.of(
                        Supplier<ImmutableSet.Builder<ELEMENT_TYPE>> { ImmutableSet.Builder() },
                        BiConsumer<ImmutableSet.Builder<ELEMENT_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, { builder.add(it) }) },
                        BinaryOperator<ImmutableSet.Builder<ELEMENT_TYPE>> { first, second ->
                            first.addAll(second.build())
                            return@BinaryOperator first
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            else -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableList<ELEMENT_TYPE>> = Collector.of(
                        Supplier<ImmutableList.Builder<ELEMENT_TYPE>> { ImmutableList.Builder() },
                        BiConsumer<ImmutableList.Builder<ELEMENT_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, { builder.add(it) }) },
                        BinaryOperator<ImmutableList.Builder<ELEMENT_TYPE>> { first, second ->
                            first.addAll(second.build())
                            return@BinaryOperator first
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
        }
    }

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE: Map<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> buildMapCollector(
            collectionTarget: TARGET_TYPE,
            extractor: (COLLECTEE_TYPE, (KEY_TYPE, VALUE_TYPE) -> Unit) -> Unit
    ): Collector<COLLECTEE_TYPE, Any, TARGET_TYPE> {
        return when (collectionTarget) {
            is ImmutableBiMap<*, *> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableBiMap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<ImmutableBiMap.Builder<KEY_TYPE, VALUE_TYPE>> { ImmutableBiMap.builder() },
                        BiConsumer<ImmutableBiMap.Builder<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<ImmutableBiMap.Builder<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2.build())
                            builder
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is BiMap<*, *> -> {
                val collector: Collector<COLLECTEE_TYPE, *, BiMap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<BiMap<KEY_TYPE, VALUE_TYPE>> { HashBiMap.create() },
                        BiConsumer<BiMap<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<BiMap<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2)
                            builder
                        }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is ImmutableMap<*, *> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableMap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<ImmutableMap.Builder<KEY_TYPE, VALUE_TYPE>> { ImmutableMap.builder() },
                        BiConsumer<ImmutableMap.Builder<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<ImmutableMap.Builder<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2.build())
                            builder
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is MutableMap<*, *> -> {
                val collector: Collector<COLLECTEE_TYPE, *, MutableMap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<MutableMap<KEY_TYPE, VALUE_TYPE>> { _Maps.emptyMutable() },
                        BiConsumer<MutableMap<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<MutableMap<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2)
                            builder
                        }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            else -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableMap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<ImmutableMap.Builder<KEY_TYPE, VALUE_TYPE>> { ImmutableMap.builder() },
                        BiConsumer<ImmutableMap.Builder<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<ImmutableMap.Builder<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2.build())
                            builder
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
        }
    }

    @JvmStatic
    fun <KEY_TYPE, VALUE_TYPE, TARGET_TYPE: Multimap<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> buildMultimapCollector(
            collectionTarget: TARGET_TYPE,
            extractor: (COLLECTEE_TYPE, (KEY_TYPE, VALUE_TYPE) -> Unit) -> Unit
    ): Collector<COLLECTEE_TYPE, Any, TARGET_TYPE> {
        return when (collectionTarget) {
            is ImmutableListMultimap<*, *> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableListMultimap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<ImmutableListMultimap.Builder<KEY_TYPE, VALUE_TYPE>> { ImmutableListMultimap.builder() },
                        BiConsumer<ImmutableListMultimap.Builder<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<ImmutableListMultimap.Builder<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2.build())
                            builder
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is ImmutableSetMultimap<*, *> -> {
                val collector: Collector<COLLECTEE_TYPE, *, ImmutableSetMultimap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<Builder<KEY_TYPE, VALUE_TYPE>> { ImmutableSetMultimap.builder() },
                        BiConsumer<Builder<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<Builder<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2.build())
                            builder
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            is ImmutableMultimap<*, *> -> {
                val collector: Collector<COLLECTEE_TYPE, *, Multimap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<ImmutableMultimap.Builder<KEY_TYPE, VALUE_TYPE>> { ImmutableMultimap.builder() },
                        BiConsumer<ImmutableMultimap.Builder<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, {key, value -> builder.put(key, value)}) },
                        BinaryOperator<ImmutableMultimap.Builder<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2.build())
                            builder
                        },
                        Function { it.build() }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
            else -> {
                val collector: Collector<COLLECTEE_TYPE, *, Multimap<KEY_TYPE, VALUE_TYPE>> = Collector.of(
                        Supplier<Multimap<KEY_TYPE, VALUE_TYPE>> { _Multimaps.emptyMutable() },
                        BiConsumer<Multimap<KEY_TYPE, VALUE_TYPE>, COLLECTEE_TYPE> { builder, element -> extractor(element, { key, value -> builder.put(key, value) }) },
                        BinaryOperator<Multimap<KEY_TYPE, VALUE_TYPE>> { builder, builder2 ->
                            builder.putAll(builder2)
                            builder
                        }
                )

                return collector as Collector<COLLECTEE_TYPE, Any, TARGET_TYPE>
            }
        }
    }
}