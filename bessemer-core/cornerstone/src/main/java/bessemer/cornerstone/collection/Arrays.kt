package bessemer.cornerstone.collection

object _Arrays {
    @JvmStatic
    fun isEmpty(collection: Array<Any?>?): Boolean {
        if(collection == null) {
            return true;
        }

        return collection.isEmpty()
    }
}