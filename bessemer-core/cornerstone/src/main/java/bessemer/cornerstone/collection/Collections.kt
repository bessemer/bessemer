package bessemer.cornerstone.collection

import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableSet
import java.util.*

// TODO add support for guava Range(...) as well as custom DiscreteDomains
object _Collections {
    @JvmStatic
    fun isEmpty(collection: Collection<Any?>?): Boolean {
        if(collection == null) {
            return true;
        }

        return collection.isEmpty()
    }

    @JvmStatic
    fun <T> removeAll(target: Collection<T>, toRemoveGeneric: Collection<T>): List<T> {
        val toRemove = _Sets.coerce(toRemoveGeneric)

        val builder = ImmutableList.builder<T>()
        for (element in target) {
            if (!toRemove.contains(element)) {
                builder.add(element)
            }
        }

        return builder.build()
    }
}