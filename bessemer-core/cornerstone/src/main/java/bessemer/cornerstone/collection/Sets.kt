package bessemer.cornerstone.collection

import bessemer.cornerstone.util._Objects
import com.google.common.collect.ImmutableSet
import com.google.common.collect.ImmutableSortedSet
import com.google.common.collect.Sets
import java.util.*
import java.util.function.BiConsumer
import java.util.function.BinaryOperator
import java.util.function.Function
import java.util.function.Predicate
import java.util.function.Supplier
import java.util.stream.Collector
import java.util.stream.Stream

object _Sets {
    @JvmStatic
    @SafeVarargs
    fun <T> of(vararg targets: T) = ImmutableSet.copyOf(targets)

    @JvmStatic
    fun <T> empty() = of<T>()

    @JvmStatic
    fun <T> ofMutable(vararg targets: T): MutableSet<T> {
        return Sets.newHashSet(*targets)
    }

    @JvmStatic
    fun <T> emptyMutable() = ofMutable<T>()

    @JvmStatic
    fun <T> copy(elements: Iterable<T>?): Set<T> {
        if(elements == null) {
            return of()
        }

        return ImmutableSet.copyOf(elements)
    }

    @JvmStatic
    fun <T> coerce(iterable: Iterable<@JvmWildcard T>): Set<T> {
        return if (iterable is Set<*>) {
            iterable as Set<T>
        } else copy(iterable)

    }

    @JvmStatic
    fun <T> defaulted(set: Set<T>) = _Objects.defaulted(set, of())

    @JvmStatic
    fun <T> add(collection: Collection<T>, vararg others: T) = union(collection, of(*others))

    @JvmStatic
    @SafeVarargs
    fun <T> concat(vararg lists: Iterable<T>) = union(*lists)

    @JvmStatic
    fun <T> concat(lists: Iterable<Iterable<T>>) = union(lists)

    @JvmStatic
    @SafeVarargs
    fun <T> union(vararg lists: Iterable<T>) = union(of(*lists))

    @JvmStatic
    fun <T> union(lists: Iterable<Iterable<T>>): Set<T> {
        val builder = ImmutableSet.builder<T>()
        for (list in lists) {
            builder.addAll(list)
        }

        return builder.build()
    }

    @JvmStatic
    fun <T> remove(elements: Collection<T>, remove: T) = remove(elements, of(remove))

    @JvmStatic
    fun <T> remove(elements: Collection<T>, remove: Collection<T>) =
            elements.stream().filter { element -> !remove.contains(element) }.collect(collect())

    @JvmStatic
    fun <T> filter(elements: Collection<T>, filter: Predicate<T>) =
            elements.stream().filter(filter).collect(collect())

    @JvmStatic
    fun <T> partition(elements: Iterable<T>, size: Int) =
            _Streams.partition(_Streams.from(elements), size).collect(collect())

    @JvmStatic
    fun <T> partition(elements: Iterable<T>, predicate: (T) -> Boolean): Pair<Set<T>, Set<T>> {
        val result = _Streams.partition(_Streams.from(elements), predicate)
        return result.first.collect(collect()) to result.second.collect(collect())
    }

    @JvmStatic
    fun <T, N> crossProduct(c1: T, c2: Collection<N>) = crossProduct(of(c1), c2)

    @JvmStatic
    fun <T, N> crossProduct(c1: Collection<T>, c2: N) = crossProduct(c1, of(c2))

    @JvmStatic
    fun <T, N> crossProduct(c1: Collection<T>, c2: Collection<N>) =
            c1.stream().flatMap { e1 -> c2.stream().map { e2 -> e1 to e2 } }.collect(collect())

    @JvmStatic
    fun <T> powerSet(originalSet: Collection<T>): Set<Set<T>> {
        val sets = HashSet<Set<T>>()
        if (originalSet.isEmpty()) {
            sets.add(HashSet())
            return sets
        }

        val list = ArrayList(originalSet)
        val head = list[0]
        val rest = HashSet(list.subList(1, list.size))
        for (set in powerSet(rest)) {
            val newSet = HashSet<T>()
            newSet.add(head)
            newSet.addAll(set)
            sets.add(newSet)
            sets.add(set)
        }

        return sets
    }

    @JvmStatic
    fun <T> copyMutable(elements: Iterable<T>?): MutableSet<T> {
        if(elements == null) {
            return Sets.newHashSet()
        }

        return Sets.newHashSet(elements)
    }

    @JvmStatic
    fun <T> defaultMutable(set: MutableSet<T>) = _Objects.defaulted(set, ofMutable())

    @JvmStatic
    fun <T> collect(stream: Stream<T>) = stream.collect(collect())

    @JvmStatic
    fun <T> collect(): Collector<@JvmWildcard T, *, Set<T>> = collect(of())

    @JvmStatic
    fun <T> collectMutable(): Collector<@JvmWildcard T, *, Set<T>> = collect(ofMutable())

    @JvmStatic
    fun <T: Comparable<@JvmWildcard T>> collectSorted(): Collector<@JvmWildcard T, *, SortedSet<T>> = collect(ImmutableSortedSet.of())

    // TODO migrate this over to the new collector system if possible
    @JvmStatic
    fun <T> collectSorted(comparator: Comparator<T>): Collector<T, ImmutableSortedSet.Builder<T>, SortedSet<T>> {
        return Collector.of<T, ImmutableSortedSet.Builder<T>, SortedSet<T>>(
                Supplier<ImmutableSortedSet.Builder<T>> { ImmutableSortedSet.Builder(comparator) },
                BiConsumer<ImmutableSortedSet.Builder<T>, T> { obj, element -> obj.add(element) },
                BinaryOperator<ImmutableSortedSet.Builder<T>> { first, second ->
                    first.addAll(second.build())
                    return@BinaryOperator first
                },
                Function { it.build() }
        )
    }

    @JvmStatic
    fun <T, TARGET_TYPE : Set<T>> collect(collectionTarget: TARGET_TYPE): Collector<@JvmWildcard T, Any, TARGET_TYPE> = _Collectors.buildIterableCollector(
            collectionTarget = collectionTarget,
            extractor = { element, builder -> builder(element) }
    )

    @JvmStatic
    fun <T, N> collect(extractor: (N, (T) -> Unit) -> Unit): Collector<@JvmWildcard N, Any, Set<T>> = _Collectors.buildIterableCollector(
            collectionTarget = of(),
            extractor = extractor
    )
}