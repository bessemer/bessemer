package bessemer.cornerstone.collection

import java.util.NoSuchElementException

abstract class SingleEntryIterator<T> : Iterator<T> {
    private var nextGenerated = false
    private var next: T? = null

    override fun hasNext(): Boolean {
        if (!nextGenerated) {
            try {
                next = generateNext()
                nextGenerated = true
            } catch (e: NoSuchElementException) {
                nextGenerated = false
            }

        }
        return nextGenerated
    }

    override fun next(): T {
        if (nextGenerated) {
            nextGenerated = false
            return next!!
        } else {
            return generateNext()
        }
    }

    @Throws(NoSuchElementException::class)
    protected abstract fun generateNext(): T
}