package bessemer.cornerstone.collection


import bessemer.cornerstone._Preconditions
import bessemer.cornerstone.function._Predicates
import bessemer.cornerstone.util.toOptional
import com.google.common.collect.Multimap
import java.util.LinkedList
import java.util.Queue
import java.util.Spliterator
import java.util.Spliterators.AbstractSpliterator
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Consumer
import java.util.function.Function
import java.util.stream.Stream
import java.util.stream.StreamSupport

object _Streams {
    @JvmStatic
    @SafeVarargs
    fun <T> of(vararg element: T): Stream<T> {
        return _Lists.of(*element).stream()
    }

    @JvmStatic
    fun <T> empty() = of<T>()

    @JvmStatic
    fun <T> from(elements: Iterable<T>): Stream<T> {
        if(elements is Collection) {
            return elements.stream()
        }
        else {
            return StreamSupport.stream(elements.spliterator(), false)
        }
    }

    @JvmStatic
    fun <T, N> from(map: Map<T, N>): Stream<Pair<T, N>> = map.entries.stream().map { entry -> entry.key to entry.value }

    @JvmStatic
    fun <T, N> from(map: Multimap<T, N>): Stream<Pair<T, N>> = map.entries().stream().map { entry -> entry.key to entry.value }

    @JvmStatic
    @JvmOverloads
    fun <T> from(
            advancer: Function<Consumer<in T>, Boolean>,
            parallel: Boolean = false,
            size: Long = Long.MAX_VALUE,
            characteristics: Int = Spliterator.NONNULL): Stream<T> {

        val spliterator = object : AbstractSpliterator<T>(size, characteristics) {
            override fun tryAdvance(action: Consumer<in T>): Boolean {
                return advancer.apply(action)
            }
        }

        return from(spliterator, parallel)
    }

    @JvmStatic
    @JvmOverloads
    fun <T> from(spliterator: Spliterator<T>, parallel: Boolean = false): Stream<T> = StreamSupport.stream(spliterator, parallel)

    @JvmSynthetic
    fun <T: Any> findOnly(stream: Stream<T>): T? {
        val firstTwo = _Lists.take(stream, 2)
        _Preconditions.checkArgument(firstTwo.size <= 1, "Find only returned at least two matching items")
        return _Iterables.first(firstTwo)
    }

    @JvmStatic
    @JvmName("findOnly")
    fun <T: Any> optionalFindOnly(stream: Stream<T>) = findOnly(stream).toOptional()

    @JvmStatic
    fun <T> take(iterable: Iterable<T>, size: Long) = take(from(iterable), size)

    @JvmStatic
    fun <T> take(stream: Stream<T>, size: Long) = stream.limit(size)

    @JvmStatic
    fun <T> append(collection: Stream<T>, vararg others: T) = concat(collection, of(*others))

    @JvmStatic
    @SafeVarargs
    fun <T> concat(vararg lists: Iterable<T>): Stream<T> =
            concatCollections(_Lists.of(*lists))

    @JvmStatic
    @SafeVarargs
    fun <T> concat(vararg lists: Stream<out T>): Stream<T> =
            concat(_Lists.of(*lists))

    @JvmStatic
    fun <T> concatCollections(streams: Iterable<Iterable<T>>): Stream<T> =
            concat(from(streams).map(::from))

    @JvmStatic
    fun <T> concat(streams: Iterable<Stream<out T>>): Stream<T> =
            concat(from(streams))

    @JvmStatic
    fun <T> concat(streams: Stream<out Stream<out T>>): Stream<T> {
        var result = of<T>()

        for (stream in streams) {
            result = Stream.concat<T>(result, stream)
        }

        return result
    }

    @JvmStatic
    fun <T, N> mapIndexed(iterable: Stream<T>, mapper: (T, Int) -> N): Stream<N> {
        if (iterable.isParallel) {
            throw IllegalArgumentException("Unable to call map indexed on a parallel stream")
        }

        val i = AtomicInteger(0)
        return iterable.map { v -> mapper(v, i.getAndIncrement()) }
    }

    @JvmStatic
    fun <T> removeAll(stream: Stream<T>, vararg remove: T) =
            removeAll(stream, _Lists.of(*remove))

    @JvmStatic
    fun <T> removeAll(stream: Stream<T>, remove: Iterable<T>): Stream<T> {
        return stream.filter(_Predicates.matchElements(remove))
    }

    @JvmStatic
    fun <T> partition(stream: Stream<T>, size: Int): Stream<Stream<T>> {
        _Preconditions.checkArgument(size > 0, "Partition size: $size must be > 0")

        val originalSpliterator = stream.spliterator()

        val spliterator = object : AbstractSpliterator<Stream<T>>(java.lang.Long.MAX_VALUE, Spliterator.NONNULL) {
            override fun tryAdvance(action: Consumer<in Stream<T>>): Boolean {
                val chunk = ArrayList<T>(size)
                var i = 0
                while (i < size && originalSpliterator.tryAdvance { chunk.add(it) }) {
                    i++
                    // Empty block
                }

                if (chunk.isEmpty()) {
                    return false
                }

                action.accept(chunk.stream())
                return true
            }
        }

        return StreamSupport.stream(spliterator, stream.isParallel)
    }

    @JvmStatic
    fun <T> partition(stream: Stream<T>, predicate: (T) -> Boolean): Pair<Stream<T>, Stream<T>> {
        val originalSpliterator = stream.spliterator()
        val truePipeline: BufferedSpliterator<T>
        var falsePipeline: BufferedSpliterator<T>? = null

        truePipeline = BufferedSpliterator(advanceFunction = { consumer ->
            var canAdvance = true
            var consumedElement = false

            while (!consumedElement && canAdvance) {
                canAdvance = originalSpliterator.tryAdvance {
                    if (predicate(it)) {
                        consumer(it)
                        consumedElement = true
                    } else {
                        falsePipeline!!.buffer.offer(it)
                    }
                }
            }

            return@BufferedSpliterator consumedElement
        })

        falsePipeline = BufferedSpliterator(advanceFunction = { consumer ->
            var canAdvance = true
            var consumedElement = false

            while (!consumedElement && canAdvance) {
                canAdvance = originalSpliterator.tryAdvance {
                    if (!predicate(it)) {
                        consumer(it)
                        consumedElement = true
                    } else {
                        truePipeline.buffer.offer(it)
                    }
                }
            }

            return@BufferedSpliterator consumedElement
        })

        return truePipeline.stream() to falsePipeline.stream()
    }

    class BufferedSpliterator<T>(
            val buffer: Queue<T> = LinkedList(),
            val advanceFunction: ((T) -> Unit) -> Boolean
    ): AbstractSpliterator<T>(java.lang.Long.MAX_VALUE, Spliterator.NONNULL) {
        override fun tryAdvance(action: Consumer<in T>): Boolean {
            if(!buffer.isEmpty()) {
                action.accept(buffer.poll())
                return true
            }

            return advanceFunction { action.accept(it) }
        }

        fun stream() = StreamSupport.stream(this, false)
    }
}