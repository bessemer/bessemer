package bessemer.cornerstone.file

import bessemer.cornerstone.collection.toList
import bessemer.cornerstone.string._Strings
import java.util.*
import java.util.stream.Collectors

object _Files {
    private const val FOLDER_SEPARATOR = "/"
    private const val WINDOWS_FOLDER_SEPARATOR = "\\"
    private const val TOP_PATH = ".."
    private const val CURRENT_PATH = "."

    /**
     * Normalize the path by suppressing sequences like "path/.." and
     * inner simple dots.
     *
     * The result is convenient for path comparison. For other uses,
     * notice that Windows separators ("\") are replaced by simple slashes.
     * @param path the original path
     * @return the normalized path
     */
    @JvmStatic
    fun cleanPath(path: String?): String? {
        if (path == null) {
            return null
        }

        var pathToUse = _Strings.replace(path, WINDOWS_FOLDER_SEPARATOR, FOLDER_SEPARATOR)

        // Strip prefix from path to analyze, to not treat it as part of the
        // first path element. This is necessary to correctly parse paths like
        // "file:core/../core/io/Resource.class", where the ".." should just
        // strip the first "core" directory while keeping the "file:" prefix.
        val prefixIndex = pathToUse.indexOf(":")
        var prefix = ""
        if (prefixIndex != -1) {
            prefix = pathToUse.substring(0, prefixIndex + 1)
            if (prefix.contains("/")) {
                prefix = ""
            } else {
                pathToUse = pathToUse.substring(prefixIndex + 1)
            }
        }
        if (pathToUse.startsWith(FOLDER_SEPARATOR)) {
            prefix += FOLDER_SEPARATOR
            pathToUse = pathToUse.substring(1)
        }

        val pathArray = _Strings.split(pathToUse, FOLDER_SEPARATOR).toList()
        val pathElements = LinkedList<String>()
        var tops = 0

        for (i in pathArray.indices.reversed()) {
            val element = pathArray[i]
            if (CURRENT_PATH == element) {
                // Points to current directory - drop it.
            } else if (TOP_PATH == element) {
                // Registering top path found.
                tops++
            } else {
                if (tops > 0) {
                    // Merging path element with element corresponding to top path.
                    tops--
                } else {
                    // Normal path element found.
                    pathElements.add(0, element)
                }
            }
        }

        // Remaining top paths need to be retained.
        for (i in 0 until tops) {
            pathElements.add(0, TOP_PATH)
        }

        return prefix + pathElements.stream().collect(Collectors.joining(FOLDER_SEPARATOR))
    }
}