package bessemer.cornerstone.exception

import bessemer.cornerstone.collection._Iterables
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.domain.Outcome
import bessemer.cornerstone.domain.propagate
import bessemer.cornerstone.util._Objects
import com.google.common.base.Throwables

object _Exceptions {
    @JvmStatic
    fun <T> tryOutcome(operation: ExceptionalSupplier<T, Exception>) = tryOutcome { operation.invoke() }

    @JvmStatic
    fun tryOutcome(operation: ExceptionalOperation<*>) = tryOutcome { operation.invoke() }

    @JvmSynthetic
    fun <T> tryOutcome(supplier: () -> T): Outcome<T, Exception> {
        return try {
            Outcome.success(supplier())
        } catch (e: Exception) {
            Outcome.error(e)
        }
    }

    @JvmStatic
    fun <T> tryPropagate(operation: ExceptionalSupplier<T, Exception>) = tryPropagate { operation.invoke() }

    @JvmStatic
    fun tryPropagate(operation: ExceptionalOperation<*>) = tryPropagate { operation.invoke() }

    @JvmSynthetic
    fun <T> tryPropagate(supplier: () -> T) = tryOutcome(supplier).propagate()

    @JvmStatic
    fun toUnchecked(throwable: Throwable): RuntimeException {
        if (throwable is RuntimeException) {
            throw throwable
        }
        if (throwable is Error) {
            throw throwable
        }

        throw RuntimeException(throwable)
    }

    @JvmStatic
    fun <T> getCause(exception: Throwable, exceptionType: Class<T>): T? {
        val throwables = _Lists.append(Throwables.getCausalChain(exception), exception)
        return _Iterables.first(_Objects.filterByType(throwables, exceptionType))
    }

    @JvmStatic
    fun getCause(exception: Throwable, predicate: (Throwable) -> Boolean): Throwable? {
        val throwables = _Lists.append(Throwables.getCausalChain(exception), exception)
        return throwables.stream().filter(predicate).findFirst().orElse(null)
    }
}