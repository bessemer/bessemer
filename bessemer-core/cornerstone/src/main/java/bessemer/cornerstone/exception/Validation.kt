package bessemer.cornerstone.exception

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Maps

data class ValidationEvent(
        val key: String,
        val message: KeyedMessage,
        val parameters: Map<String, Any> = _Maps.of(),
        val sourceEvents: List<ValidationEvent> = _Lists.of()) {

    companion object {
        @JvmStatic
        @JvmOverloads
        fun of(key: String, parameters: Map<String, Any> = _Maps.of(), sourceEvents: List<ValidationEvent> = _Lists.of()) = of(key, KeyedMessage.of(key, parameters), parameters, sourceEvents)

        @JvmStatic
        fun of(message: KeyedMessage) = of(message.key, message, message.parameters)

        @JvmStatic
        @JvmOverloads
        fun of(key: String, messageKey: String, parameters: Map<String, Any> = _Maps.of(), sourceEvents: List<ValidationEvent> = _Lists.of()) = of(key, KeyedMessage.of(messageKey, parameters), parameters, sourceEvents)

        @JvmStatic
        @JvmOverloads
        fun of(key: String, message: KeyedMessage, parameters: Map<String, Any> = _Maps.of(), sourceEvents: List<ValidationEvent> = _Lists.of()): ValidationEvent {
            return ValidationEvent(key, message, parameters, sourceEvents)
        }
    }
}