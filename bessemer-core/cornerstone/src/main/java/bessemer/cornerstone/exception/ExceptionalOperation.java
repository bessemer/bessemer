package bessemer.cornerstone.exception;

@FunctionalInterface
public interface ExceptionalOperation<T extends Throwable> {
	void invoke() throws T;
}