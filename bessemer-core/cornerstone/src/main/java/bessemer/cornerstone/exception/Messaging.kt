package bessemer.cornerstone.exception

import bessemer.cornerstone.collection._Maps

data class KeyedMessage(
        val key: String,
        val parameters: Map<String, Any>) {

    companion object {
        fun of(key: String, parameters: Map<String, Any> = _Maps.of()): KeyedMessage {
            return KeyedMessage(key, parameters)
        }
    }
}