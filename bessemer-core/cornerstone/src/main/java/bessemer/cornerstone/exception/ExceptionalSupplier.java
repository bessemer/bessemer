package bessemer.cornerstone.exception;

import java.util.function.Supplier;

@FunctionalInterface
public interface ExceptionalSupplier<T, N extends Throwable> {
	T invoke() throws N;

	static <N> ExceptionalSupplier<N, Exception> wrap(Supplier<N> f) {
		return f::get;
	}
}