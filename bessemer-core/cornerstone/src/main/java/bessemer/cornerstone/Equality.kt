package bessemer.cornerstone

import java.util.*
import java.util.function.BiFunction

interface Equalitor<T, N> : BiFunction<T, N, Boolean> {
    fun reverse(): bessemer.cornerstone.Equalitor<N, T> {
        return object: bessemer.cornerstone.Equalitor<N, T> {
            override fun apply(t: N, u: T) = this@Equalitor.apply(u, t)
        }
    }
}

interface SymmetricEqualitor<T> : bessemer.cornerstone.Equalitor<T, T>

object _Equality {
    fun <T, N> equals(o1: T, o2: N, equalitor: bessemer.cornerstone.Equalitor<T, N>): Boolean {
        return equalitor.apply(o1, o2)
    }

    fun <T, N> natural(): bessemer.cornerstone.Equalitor<T, N> {
        return object: bessemer.cornerstone.Equalitor<T, N> {
            override fun apply(t: T, u: N) = Objects.equals(t, u)
        }
    }

    fun <T> biDirectionalCompare(comparator: Comparator<T>, first: T, second: T): Int {
        val firstResult = comparator.compare(first, second)
        val secondResult = comparator.compare(second, first)

        if (firstResult > 0 && secondResult > 0 || firstResult < 0 && secondResult < 0) {
            throw RuntimeException()
        }

        if (firstResult > 0) {
            return 1
        }

        if (firstResult < 0) {
            return -1
        }

        if (secondResult > 0) {
            return -1
        }

        return if (secondResult < 0) {
            1
        } else 0

    }

    fun <T> biDirectionalComparator(comparator: Comparator<T>): Comparator<T> {
        return Comparator { first, second -> bessemer.cornerstone._Equality.biDirectionalCompare(comparator, first, second) }
    }
}