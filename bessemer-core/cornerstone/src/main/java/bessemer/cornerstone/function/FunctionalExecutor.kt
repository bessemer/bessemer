package bessemer.cornerstone.function

import bessemer.cornerstone.exception.ExceptionalOperation
import bessemer.cornerstone.exception.ExceptionalSupplier

interface FunctionalExecutor {
    @JvmDefault
    fun <T : Throwable> tryExecute(f: ExceptionalOperation<T>) = execute { f.invoke() }

    @JvmDefault
    fun <T, N : Throwable> tryExecute(f: ExceptionalSupplier<T, N>): T = execute { f.invoke() }

    @JvmDefault
    fun <T> execute(f: Runnable) = execute { f.run() }

    fun <T> execute(f: () -> T): T
}

interface FunctionalExecutorArg1<A1> {
    @JvmDefault
    fun <T : Throwable> tryExecute(arg1: A1, f: ExceptionalOperation<T>) = execute(arg1) { f.invoke() }

    @JvmDefault
    fun <T, N : Throwable> tryExecute(arg1: A1, f: ExceptionalSupplier<T, N>): T = execute(arg1) { f.invoke() }

    @JvmDefault
    fun <T> execute(arg1: A1, f: Runnable) = execute(arg1) { f.run() }

    fun <T> execute(arg1: A1, f: () -> T): T
}