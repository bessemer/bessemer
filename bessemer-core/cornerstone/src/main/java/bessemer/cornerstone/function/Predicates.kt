package bessemer.cornerstone.function

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Sets
import java.util.stream.Stream

object _Predicates {
    @JvmStatic
    fun <T> invert(predicate: (T) -> Boolean): (T) -> Boolean {
        return { !predicate(it) }
    }

    @JvmStatic
    fun <T> alwaysTrue(): (T) -> Boolean {
        return { true }
    }

    @JvmStatic
    fun <T> alwaysFalse(): (T) -> Boolean {
        return { false }
    }

    @JvmStatic
    fun <T> matchElements(elements: Iterable<T>): (T) -> Boolean {
        val removeSet = _Sets.coerce(elements)
        return { removeSet.contains(it) }
    }

    @JvmStatic
    fun <T> matchDuplicates(): (T) -> Boolean {
        val seen = _Sets.ofMutable<T>();
        return predicate@ {
            val hasSeen = seen.contains(it)
            if(hasSeen) {
                return@predicate false
            }
            else {
                seen.add(it)
                return@predicate true
            }
        }
    }
}