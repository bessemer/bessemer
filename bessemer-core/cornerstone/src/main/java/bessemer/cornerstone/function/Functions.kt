package bessemer.cornerstone.function
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.exception.ExceptionalOperation
import bessemer.cornerstone.exception.ExceptionalSupplier
import java.util.function.Supplier

object _Functions {
    @JvmStatic
    fun <T> identity(element: T) = element

    @JvmStatic
    fun <T: Any, N> multiplex(function: (T) -> N): (List<T>) -> List<N> = { it.stream().map(function).collect(_Lists.collect()) }

    @JvmStatic
    fun voidSupplier(f: Runnable): Supplier<Unit> {
        return Supplier {
            f.run()
        }
    }

    @JvmStatic
    fun <T : Throwable> voidExceptionSupplier(f: ExceptionalOperation<T>): ExceptionalSupplier<Unit, T> {
        return ExceptionalSupplier {
            f.invoke()
        }
    }
}