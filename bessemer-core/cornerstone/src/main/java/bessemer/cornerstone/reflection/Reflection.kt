package bessemer.cornerstone.reflection

import com.google.common.base.Preconditions
import java.lang.reflect.Field
import java.lang.reflect.GenericArrayType
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable
import java.lang.reflect.WildcardType
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.KTypeProjection
import kotlin.reflect.KVariance
import kotlin.reflect.full.createType
import kotlin.reflect.full.starProjectedType

open class TypeToken<T> {
    /**
     * Gets the referenced type.
     */
    val type: KType
    val javaType: Type

    private constructor(type: KType, javaType: Type) {
        this.type = type
        this.javaType = javaType
    }

    protected constructor() {
        val superclass = javaClass.genericSuperclass
        if (superclass is Class<*>) {
            throw RuntimeException("Missing type parameter.")
        }

        this.javaType = (superclass as ParameterizedType).actualTypeArguments[0]
        this.type = this.javaType.kotlin()
    }

    companion object {
        inline fun <reified T: Any> create(): TypeToken<T> = object : TypeToken<T>(){}

        fun <T: Any> of(type: KType): TypeToken<T> = TypeToken(type, type.javaClass)

        fun <T: Any> of(clazz: KClass<T>): TypeToken<T> = TypeToken(clazz.starProjectedType, clazz.java)

        @JvmStatic
        fun <T: Any> of(type: Type): TypeToken<T> = TypeToken(type.kotlin(), type)

        @JvmStatic
        fun <T: Any> of(clazz: Class<T>): TypeToken<T> = TypeToken(clazz.kotlin(), clazz)
    }
}

object _Reflection {
    private fun buildInvariantFlexibleProjection(clazz: KClass<*>, arguments: List<KTypeProjection> = emptyList()): KTypeProjection {
        // TODO: there should be an API in kotlin-reflect which creates KType instances corresponding to flexible types
        // Currently we always produce a non-null type, which is obviously wrong
        val args = if (clazz.java.isArray) listOf(buildInvariantFlexibleProjection(clazz.java.componentType.kotlin)) else arguments
        return KTypeProjection.invariant(clazz.createType(args, nullable = false))
    }

    private fun buildKTypeProjection(type: Type): KTypeProjection = when (type) {
        is Class<*> -> buildInvariantFlexibleProjection(type.kotlin, if(type.isArray) listOf(_Reflection.buildKTypeProjection(type.componentType)) else emptyList())
        is ParameterizedType -> {
            val erasure = (type.rawType as Class<*>).kotlin

            buildInvariantFlexibleProjection(
                    erasure,
                    (erasure.typeParameters.zip(type.actualTypeArguments).map { (parameter, argument) ->
                        val projection = _Reflection.buildKTypeProjection(argument)

                        projection.takeIf {
                            // Get rid of use-site projections on arguments, where the corresponding parameters already have a declaration-site projection
                            parameter.variance == KVariance.INVARIANT || parameter.variance != projection.variance
                        } ?: KTypeProjection.invariant(projection.type!!)
                    }))
        }
        is WildcardType -> when {
            type.lowerBounds.isNotEmpty() -> KTypeProjection.contravariant(_Reflection.toKType(type.lowerBounds.single()))
            type.upperBounds.isNotEmpty() -> KTypeProjection.covariant(_Reflection.toKType(type.upperBounds.single()))
            // This looks impossible to obtain through Java reflection API, but someone may construct and pass such an instance here anyway
            else -> KTypeProjection.STAR
        }
        is GenericArrayType -> buildInvariantFlexibleProjection(Array<Any>::class, listOf(_Reflection.buildKTypeProjection(type.genericComponentType)))
        // TODO this is probably wrong...
        is TypeVariable<*> -> KTypeProjection.STAR
        else -> throw IllegalArgumentException("Unsupported type: $this")
    }

    fun toKType(type: Type): KType = _Reflection.buildKTypeProjection(type).type!!

    fun findField(clazz: Class<*>, name: String, type: Class<*>? = null): Field? {
        Preconditions.checkNotNull(clazz, "Class must not be null")

        var searchType: Class<*>? = clazz
        while (Any::class.java != searchType && searchType != null) {
            val fields = getDeclaredFields(searchType)
            for (field in fields) {
                if ((name == field.name) && (type == null || type == field.type)) {
                    return field
                }
            }
            searchType = searchType.superclass
        }
        return null
    }

    private fun getDeclaredFields(clazz: Class<*>): Array<Field> {
        Preconditions.checkNotNull(clazz, "Class must not be null")
        return clazz.declaredFields

//        var result: Array<Field>? = declaredFieldsCache.get(clazz)
//        if (result == null) {
//            result = clazz.declaredFields
//            declaredFieldsCache.put(clazz, if (result!!.size == 0) NO_FIELDS else result)
//        }
//        return result
    }
}

fun Type.kotlin() = _Reflection.toKType(this)