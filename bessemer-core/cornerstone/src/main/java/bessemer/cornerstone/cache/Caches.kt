package bessemer.cornerstone.cache

import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.data.AlwaysEmptyMutableLexicon
import bessemer.cornerstone.data.ChainingMutableIndex
import bessemer.cornerstone.data.Index
import bessemer.cornerstone.data.LoadingMutableIndex
import bessemer.cornerstone.data.MutableIndex
import bessemer.cornerstone.data.SerializingMutableIndex
import bessemer.cornerstone.domain.Serializer

object _Caches {
    @JvmStatic
    fun <T: Any, N: Any> uncached(): MutableIndex<T, N> = AlwaysEmptyMutableLexicon.get()

    @JvmStatic
    fun <T: Any, N: Any> builder(cache: MutableIndex<T, N>): CacheWrapperBuilder<T, N> {
        return CacheWrapperBuilder(cache)
    }
}

class CacheWrapperBuilder<T: Any, N: Any>(private var currentCache: MutableIndex<T, N>) {
    fun chain(cache: Index<T, N>): CacheWrapperBuilder<T, N> {
        currentCache = ChainingMutableIndex(cache, currentCache)
        return this
    }

    fun loader(loader: (Collection<T> ) -> Map<T, N>): CacheWrapperBuilder<T, N> {
        currentCache = LoadingMutableIndex(currentCache, loader)
        return this
    }

    fun multiplexingLoader(singleElementLoader: (T) -> N?): CacheWrapperBuilder<T, N> {
        val loader: (Collection<T>) -> Map<T, N> = { collection ->
            val results = _Maps.ofMutable<T, N>()

            for (element in collection) {
                val result = singleElementLoader(element)
                if (result != null) {
                    results[element] = result
                }
            }

            results
        }

        return loader(loader)
    }

    fun <K: Any> serializeKeys(keySerializer: (K) -> T): CacheWrapperBuilder<K, N> {
        return CacheWrapperBuilder<K, N>(SerializingMutableIndex(currentCache, keySerializer, Serializer.passthrough()))
    }

    fun <V: Any> serializeValues(valueSerializer: Serializer<V, N>): CacheWrapperBuilder<T, V> {
        return CacheWrapperBuilder(SerializingMutableIndex(currentCache, { it }, valueSerializer))
    }

    fun <K: Any, V: Any> serialize(keySerializer: (K) -> T, valueSerializer: Serializer<V, N>): CacheWrapperBuilder<K, V> {
        return CacheWrapperBuilder(SerializingMutableIndex(currentCache, keySerializer, valueSerializer))
    }

    fun build(): MutableIndex<T, N> {
        return currentCache
    }
}