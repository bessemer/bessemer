package bessemer.cornerstone.util

import bessemer.cornerstone.reflection.TypeToken
import com.fasterxml.jackson.databind.ObjectMapper

object _Json {
    @JvmStatic
    @JvmOverloads
    fun marshall(o: Any, typeConverter: TypeConverter = _TypeConverters.globalTypeConverter): String {
        return typeConverter.convert(o, String::class.java)
    }

    @JvmStatic
    @JvmOverloads
    fun <T: Any> unMarshall(json: String, clazz: Class<T>, typeConverter: TypeConverter = _TypeConverters.globalTypeConverter): T {
        return typeConverter.convert(json, clazz)
    }

    @JvmStatic
    @JvmOverloads
    fun <T: Any> unMarshall(json: String, typeReference: TypeToken<T>, typeConverter: TypeConverter = _TypeConverters.globalTypeConverter): T {
        return typeConverter.convert(json, typeReference)
    }
}