package bessemer.cornerstone.util

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Streams
import java.util.Objects
import java.util.Optional
import java.util.function.Supplier
import java.util.stream.Stream

object _Optionals {
    @SafeVarargs
    fun <T> firstSome(vararg optionals: Optional<T>) = firstSome(_Lists.of(*optionals))

    fun <T> firstSome(optionals: Collection<Optional<T>>) = firstSome(optionals.stream())

    fun <T> firstSome(optionals: Stream<Optional<T>>): Optional<T> {
        return optionals.filter { it.isPresent }.map<T> { it.get() }.findFirst()
    }

    fun <T> lift(operation: Supplier<T>): Supplier<Optional<T>> {
        return Supplier { Optional.ofNullable(operation.get()) }
    }

    fun <T> flatMap(stream: Stream<Optional<T>>): Stream<T> {
        return stream.map { element -> element.orElse(null) }.filter { Objects.nonNull(it) }
    }

    fun <T> stream(opt: Optional<T>): Stream<T> = if (opt.isPresent) {
        _Streams.of(opt.get())
    } else {
        _Streams.of()
    }
}

/** @return [Optional.Some] with the receiver if its not `null`, [Optional.None] otherwise. */
fun <T : Any> T?.toOptional(): Optional<T> {
    return this?.let { Optional.of(it) } ?: Optional.empty()
}

fun <T : Any, N> T?.map(function: (T) -> N): N? {
    return if(this != null) {
        function(this)
    } else {
        null;
    }
}

fun <T : Any> T?.ifPresent(function: (T) -> Unit) {
    if(this != null) {
        function(this)
    }
}

fun <T : Any> Optional<T>.toNullable(): T? = this.orElse(null)