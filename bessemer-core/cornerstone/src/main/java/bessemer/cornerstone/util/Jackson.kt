package bessemer.cornerstone.util

import bessemer.cornerstone.reflection.TypeToken
import bessemer.cornerstone.reflection._Reflection
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import java.lang.reflect.Type

object Jackson {
    @JvmStatic
    fun defaultObjectMapper(): ObjectMapper {
        val mapper = ObjectMapper()
        applyDefaultConfiguration(mapper)
        return mapper;
    }

    @JvmStatic
    fun applyDefaultConfiguration(mapper: ObjectMapper) {
        mapper.registerModule(JavaTimeModule())
        mapper.registerModule(Jdk8Module())

        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        mapper.disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS)
        mapper.disable(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS)

        mapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    }

    @JvmStatic
    fun <T> toTypeReference(type: TypeToken<T>): TypeReference<T> {
        return SettableTypeReference(type.javaType)
    }
}

class SettableTypeReference<T>(type: Type) : TypeReference<T>() {
    init {
        val typeField = _Reflection.findField(SettableTypeReference::class.java, "_type")
        typeField!!.isAccessible = true
        typeField.set(this, type)
    }
}