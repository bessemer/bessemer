package bessemer.cornerstone.util

import com.google.common.base.Stopwatch
import java.time.Duration
import java.util.concurrent.TimeUnit
import java.util.function.Supplier

object Timer {
    fun time(runnable: Runnable): Duration {
        val stopwatch = Stopwatch.createStarted()
        runnable.run()
        stopwatch.stop()

        return Duration.ofMillis(stopwatch.elapsed(TimeUnit.MILLISECONDS))
    }

    fun <T> time(supplier: () -> T) = time(Supplier { supplier() })

    fun <T> time(supplier: Supplier<T>): Pair<T, Duration> {
        val stopwatch = Stopwatch.createStarted()
        val result = supplier.get()
        stopwatch.stop()

        return result to Duration.ofMillis(stopwatch.elapsed(TimeUnit.MILLISECONDS))
    }
}