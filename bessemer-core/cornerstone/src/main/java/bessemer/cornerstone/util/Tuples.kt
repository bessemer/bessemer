package bessemer.cornerstone.util

object _Tuples {
    @JvmStatic
    fun <T, N> pair(first: T, second: N) = first to second

    @JvmStatic
    fun <T, N, S> triple(first: T, second: N, third: S) = Triple(first, second, third)
}