package bessemer.cornerstone.util

import bessemer.cornerstone.collection._Lists
import java.util.*
import java.util.stream.Stream
import kotlin.reflect.KClass

object _Objects {
    @JvmStatic
    fun <T> defaulted(obj: T?, default: T): T {
        return firstNonNull(obj, default)!!
    }

    @JvmStatic
    fun <T> firstNonNull(vararg elements: T?): T? {
        for (element in elements) {
            if (element != null) {
                return element
            }
        }

        return null
    }

    @JvmStatic
    fun equalsPreamble(target: Any, other: Any?): Boolean? {
        if (other === target) {
            return true
        }
        if (other == null) {
            return false
        }

        if (!target.javaClass.isAssignableFrom(other.javaClass)) {
            return false
        }

        return null
    }

    fun <T: Any> filterByType(collection: Collection<*>, type: KClass<T>) = filterByType(collection, type.java)

    @JvmStatic
    fun <T> filterByType(collection: Collection<*>, type: Class<T>): List<T> {
        return filterByType(collection.stream(), type)
                .collect(_Lists.collect())
    }

    fun <T: Any> filterByType(collection: Stream<*>, type: KClass<T>) = filterByType(collection, type.java)

    @JvmStatic
    fun <T> filterByType(stream: Stream<*>, type: Class<T>): Stream<T> {
        return stream
                .filter { i -> type.isAssignableFrom(i.javaClass) }
                .map { throwable -> throwable as T }
    }

    @JvmStatic
    fun equals(target1: Any?, target2: Any?) = Objects.equals(target1, target2)

    @JvmStatic
    fun hash(vararg targets: Any?) = Objects.hash(targets)

    @JvmStatic
    fun isArray(target: Any?) = target is Array<*>
}