package bessemer.cornerstone.util

import com.google.common.collect.BoundType
import com.google.common.collect.Range

object _Ranges {
    @JvmField
    val EMPTY_INTEGERS: Range<Int> = Range.closedOpen(0, 0)

    @JvmField
    val POSITIVE_INTEGERS: Range<Int> = Range.atLeast(0)

    @JvmField
    val POSITIVE_LONGS: Range<Long> = Range.atLeast(0)

    @JvmStatic
    fun size(range: Range<Int>): Int? {
        if(!range.hasLowerBound() || !range.hasUpperBound()) {
            return null
        }

        var adjustment = 1
        if(range.lowerBoundType() == BoundType.OPEN) {
            adjustment--
        }
        if(range.upperBoundType() == BoundType.OPEN) {
            adjustment--
        }

        return range.upperEndpoint() - range.lowerEndpoint() + adjustment
    }

    @JvmStatic
    fun limit(range: Range<Int>, value: Int?): Range<Int> {
        if(!range.hasLowerBound()) {
            throw IllegalArgumentException("Limit not supported on ranges unbounded on the left.")
        }

        if(value == null) {
            return Range.downTo(range.lowerEndpoint(), range.lowerBoundType())
        }

        return Range.range(range.lowerEndpoint(), range.lowerBoundType(), range.lowerEndpoint() + value, opposite(range.lowerBoundType()))
    }

    @JvmStatic
    fun offset(range: Range<Int>, value: Int?): Range<Int> {
        if(!range.hasLowerBound()) {
            throw IllegalArgumentException("Offset not supported on ranges unbounded on the left.")
        }

        if(range.hasUpperBound()) {
            var adjustment = 0
            if(range.lowerBoundType() == BoundType.OPEN) {
                adjustment--
            }
            if(range.upperBoundType() == BoundType.OPEN) {
                adjustment--
            }

            return Range.closed(
                    value ?: 0,
                    range.upperEndpoint() - range.lowerEndpoint() + (value ?: 0) + adjustment)
        }
        else {
            return Range.atLeast(value ?: 0)
        }
    }

    fun opposite(boundType: BoundType): BoundType {
        return if(boundType == BoundType.OPEN) {
            BoundType.CLOSED
        }
        else {
            BoundType.OPEN
        }
    }
}