package bessemer.cornerstone.util

import bessemer.cornerstone.domain.Outcome
import bessemer.cornerstone.domain.propagate
import bessemer.cornerstone.exception._Exceptions
import bessemer.cornerstone.reflection.TypeToken
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.lang3.ClassUtils
import java.io.IOException
import kotlin.reflect.jvm.jvmErasure

interface TypeConverter {
    fun <T: Any> tryConvert(element: Any, clazz: TypeToken<T>): Outcome<T, Exception>

    fun <T: Any> tryConvert(element: Any, clazz: Class<T>): Outcome<T, Exception> {
        return this.tryConvert(element, TypeToken.of(clazz))
    }

    fun <T: Any> convert(element: Any, clazz: TypeToken<T>) = this.tryConvert(element, clazz).propagate()

    fun <T: Any> convert(element: Any, clazz: Class<T>) = this.convert(element, TypeToken.of(clazz))

    fun mapify(element: Any): Map<String, Any> {
        return this.convert(element, TypeToken.create())
    }

    companion object {
        @JvmStatic
        fun from(objectMapper: ObjectMapper): TypeConverter = ConcreteTypeConverter(objectMapper)
    }
}

class ConcreteTypeConverter(private val mapper: ObjectMapper): TypeConverter {
    override fun <T: Any> tryConvert(element: Any, clazz: TypeToken<T>): Outcome<T, Exception> {
        return _Exceptions.tryOutcome {
            if (element::class == clazz.type) {
                return@tryOutcome element as T
            }
            else if (clazz.type.jvmErasure == String::class && ClassUtils.isPrimitiveWrapper(element.javaClass)) {
                return@tryOutcome element.toString () as T
            }
            else if (clazz.type.jvmErasure == String::class) {
                return@tryOutcome mapper.writer().writeValueAsString(element) as T
            }
            else if (element is String) {
                return@tryOutcome mapper.readValue(element, Jackson.toTypeReference(clazz))
            }
            else {
                return@tryOutcome mapper.convertValue(element, Jackson.toTypeReference(clazz))
            }
        }
    }
}

object _TypeConverters {
    @Volatile
    private var GLOBAL_TYPE_CONVERTER: TypeConverter? = null

    @JvmStatic
    var globalTypeConverter: TypeConverter
        get() {
            if (GLOBAL_TYPE_CONVERTER == null) {
                synchronized(TypeConverter::class.java) {
                    if (GLOBAL_TYPE_CONVERTER == null) {
                        val mapper = TypeConverter.from(Jackson.defaultObjectMapper())
                        GLOBAL_TYPE_CONVERTER = mapper
                    }
                }
            }

            return GLOBAL_TYPE_CONVERTER!!
        }
        set(value) {
            synchronized(TypeConverter::class.java) {
                GLOBAL_TYPE_CONVERTER = value
            }
        }

    @JvmStatic
    fun <T: Any> tryConvert(element: Any, clazz: Class<T>) = globalTypeConverter.tryConvert(element, clazz)

    @JvmStatic
    fun <T: Any> tryConvert(element: Any, clazz: TypeToken<T>) = globalTypeConverter.tryConvert(element, clazz)

    @JvmStatic
    fun <T: Any> convert(element: Any, clazz: Class<T>) = globalTypeConverter.convert(element, clazz)

    @JvmStatic
    fun <T: Any> convert(element: Any, clazz: TypeToken<T>) = globalTypeConverter.convert(element, clazz)

    @JvmStatic
    fun mapify(element: Any) = globalTypeConverter.mapify(element)
}