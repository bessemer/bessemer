package bessemer.cornerstone.util

import com.google.common.collect.BoundType
import com.google.common.collect.Range
import java.lang.IllegalStateException

object _Math {
    @JvmStatic
    fun isEven(number: Int): Boolean {
        return number % 2 == 0
    }

    fun bound(number: Int, range: Range<Int>): Int {
        if(range.contains(number)) {
            return number;
        }

        if(range.hasLowerBound()) {
            if(range.lowerBoundType() == BoundType.CLOSED && number < range.lowerEndpoint()) {
                return range.lowerEndpoint()
            }
            if(range.lowerBoundType() == BoundType.OPEN && number <= range.lowerEndpoint()) {
                return range.lowerEndpoint() + 1
            }
        }

        if(range.hasUpperBound()) {
            if(range.upperBoundType() == BoundType.CLOSED && number > range.upperEndpoint()) {
                return range.upperEndpoint()
            }
            if(range.upperBoundType() == BoundType.OPEN && number >= range.upperEndpoint()) {
                return range.upperEndpoint() - 1
            }
        }

        throw IllegalStateException("Bound function in illegal state - should have returned before this.")
    }
}