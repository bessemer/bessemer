package bessemer.cornerstone.time

import java.time.Duration

object _Durations {
    val ZERO = Duration.ZERO
    val MAX_VALUE = Duration.ofSeconds(Long.MAX_VALUE)
}