package bessemer.cornerstone.domain

enum class RelativePosition {
    BEFORE, AFTER
}