package bessemer.cornerstone.domain

data class Referent<T>(
        val isPresent: Boolean,
        val value: T?) {

    companion object {
        @JvmStatic
        fun <T> absent() = Referent<T>(false, null)

        @JvmStatic
        fun <T> present(x: T?) = Referent(true, x)
    }
}