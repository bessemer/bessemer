package bessemer.cornerstone.domain

import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression._Expressions
import java.io.Serializable

data class Ordering(val expression: Expression, val order: OrderEnum) : Serializable {
    companion object {
        @JvmStatic
        fun of(fieldName: String, order: OrderEnum): Ordering {
            return Ordering(_Expressions.field(fieldName), order)
        }

        @JvmStatic
        fun of(expression: Expression, order: OrderEnum): Ordering {
            return Ordering(expression, order)
        }

        @JvmStatic
        fun ascending(fieldName: String): Ordering {
            return of(fieldName, OrderEnum.ASCENDING)
        }

        @JvmStatic
        fun ascending(expression: Expression): Ordering {
            return of(expression, OrderEnum.ASCENDING)
        }

        @JvmStatic
        fun descending(fieldName: String): Ordering {
            return of(fieldName, OrderEnum.DESCENDING)
        }

        @JvmStatic
        fun descending(expression: Expression): Ordering {
            return of(expression, OrderEnum.DESCENDING)
        }
    }
}

enum class OrderEnum {
    ASCENDING, DESCENDING
}