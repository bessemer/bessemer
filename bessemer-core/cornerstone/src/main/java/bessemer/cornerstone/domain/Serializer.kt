package bessemer.cornerstone.domain

import bessemer.cornerstone.collection._Maps
import bessemer.cornerstone.collection._Sets

interface Serializer<T, N> {
    @JvmDefault
    fun serialize(element: T): N {
        return this.serialize(_Sets.of(element)).getValue(element)
    }

    fun serialize(elements: Set<T>): Map<T, N>

    @JvmDefault
    fun deserialize(element: N): T {
        return this.deserialize(_Sets.of(element)).getValue(element)
    }

    fun deserialize(serialized: Set<N>): Map<N, T>

    companion object {
        fun <T, N> passthrough(): Serializer<T, N> {
            return object : Serializer<T, N> {
                override fun serialize(elements: Set<T>): Map<T, N> {
                    return _Maps.mapValues(elements) { element -> element as N }
                }

                override fun deserialize(serialized: Set<N>): Map<N, T> {
                    return _Maps.mapValues(serialized) { element -> element as T }
                }
            }
        }
    }
}