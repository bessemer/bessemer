package bessemer.cornerstone.domain

import bessemer.cornerstone.function._Functions

interface Outcome<RESULT, ERROR> {
    val result: RESULT
    val error: ERROR

    fun isError(): Boolean

    fun <T, N> map(lFunc: (RESULT) -> T, rFunc: (ERROR) -> N): Outcome<T, N> =
            if (this.isError()) {
                error(rFunc(this.error))
            } else {
                success(lFunc(this.result))
            }

    fun <T> resolve(lFunc: (RESULT) -> T, rFunc: (ERROR) -> T): T {
        var mappedOutcome = map(lFunc, rFunc)

        return if(mappedOutcome.isError()) {
            mappedOutcome.error
        }
        else {
            mappedOutcome.result
        }
    }

    fun <T> mapSuccess(mapper: (RESULT) -> T) = map(mapper, _Functions::identity)

    fun <T> mapError(mapper: (ERROR) -> T) = map(_Functions::identity, mapper)

    companion object {
        @JvmStatic
        fun <T, N> success(value: T): Outcome<T, N> = Success(value)

        @JvmStatic
        fun <T, N> error(value: N): Outcome<T, N> = Error(value)

        @JvmStatic
        fun <T> propagate(outcome: Outcome<T, Exception>): T {
            if(outcome.isError()) {
                throw outcome.error
            }

            return outcome.result
        }
    }
}

fun <T> Outcome<T, Exception>.propagate() = Outcome.propagate(this)

private data class Success<T, N>(override val result: T) : Outcome<T, N> {
    override val error: N
        get() = throw UnsupportedOperationException()

    override fun isError() = false
}

private  data class Error<T, N>(override val error: N) : Outcome<T, N> {
    override val result: T
        get() = throw UnsupportedOperationException()

    override fun isError() = true
}