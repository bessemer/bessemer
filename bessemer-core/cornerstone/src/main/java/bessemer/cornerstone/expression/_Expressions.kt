package bessemer.cornerstone.expression

import bessemer.cornerstone.collection._Iterables
import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Streams
import java.util.function.Predicate
import java.util.stream.Stream

interface Expression

interface ExpressionContext

interface ExpressionResolver {
    fun resolveExpression(query: Expression): String
}

interface CustomExpression<T : ExpressionContext> : Expression {
    fun resolve(context: T): String
}

enum class OperationType {
    NOT, EQUALS, AND, OR, CONTAINS, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS, GREATER_THAN_EQUALS, IS_EMPTY
}

data class OperationExpression(val type: OperationType, val arguments: List<Expression>) : Expression

data class ExpressionField(val field: Any) : Expression

data class ExpressionValue(val value: Any?) : Expression

object _Expressions {
    @JvmField
    val NULL: Expression = object : Expression {}

    @JvmField
    val TRUE: ExpressionValue = ExpressionValue(true)

    @JvmField
    val FALSE: ExpressionValue = ExpressionValue(false)

    @JvmStatic
    fun field(field: Any): ExpressionField {
        return ExpressionField(field)
    }

    @JvmStatic
    fun value(value: Any): ExpressionValue {
        if(value is Boolean) {
            return if(value) {
                _Expressions.TRUE
            } else {
                _Expressions.FALSE
            }
        }

        return ExpressionValue(value)
    }

    @JvmStatic
    fun not(expression: Expression): Expression {
        if (expression == NULL) {
            return NULL
        }

        return OperationExpression(OperationType.NOT, _Lists.of(expression));
    }

    @JvmStatic
    fun equals(expressions: List<Expression>): Expression {
        return OperationExpression(OperationType.EQUALS, expressions)
    }

    @JvmStatic
    fun equals(vararg expressions: Expression): Expression {
        return equals(_Lists.of(*expressions))
    }

    @JvmStatic
    fun equals(rawField: Any, rawValue: Any): Expression {
        return equals(_Lists.of(field(rawField), value(rawValue)))
    }

    @JvmStatic
    fun equals(field: Expression, rawValue: Any): Expression {
        return equals(_Lists.of(field, value(rawValue)))
    }

    @JvmStatic
    fun equals(field1: ExpressionField, field2: ExpressionField): Expression {
        return equals(_Lists.of(field1, field2))
    }

    @JvmStatic
    fun lessThanOrEqual(rawField: String, rawValue: Any): Expression {
        return OperationExpression(OperationType.LESS_THAN_EQUALS, _Lists.of(_Expressions.field(rawField), _Expressions.value(rawValue)))
    }

    @JvmStatic
    fun greaterThan(field: String, value: Any): Expression {
        return operation(field, OperationType.GREATER_THAN, value)
    }

    @JvmStatic
    fun greaterThan(first: Expression, second: Expression): Expression {
        return operation(first, OperationType.GREATER_THAN, second)
    }

    @JvmStatic
    fun lessThan(field: String, value: Any): Expression {
        return operation(field, OperationType.LESS_THAN, value)
    }

    @JvmStatic
    fun lessThan(first: Expression, second: Expression): Expression {
        return operation(first, OperationType.LESS_THAN, second)
    }

    @JvmStatic
    fun greaterThanOrEqual(rawField: String, rawValue: Any): Expression {
        return OperationExpression(OperationType.GREATER_THAN_EQUALS, _Lists.of(_Expressions.field(rawField), _Expressions.value(rawValue)))
    }

    @JvmStatic
    fun contains(expression: Any, collection: Collection<*>): Expression {
        return contains(_Expressions.field(expression), collection)
    }

    @JvmStatic
    fun contains(field: ExpressionField, collection: Collection<*>): Expression =
            when {
                collection.isEmpty() -> _Expressions.value(false)
                collection.size == 1 -> equals(field, _Iterables.first(collection)!!)
                else -> OperationExpression(OperationType.CONTAINS, _Lists.of(field, _Expressions.value(collection)))
            }

    @JvmStatic
    fun and(vararg expressions: Expression) = and(_Lists.of(*expressions))

    @JvmStatic
    fun and(expressions: Iterable<Expression>) = and(_Streams.from(expressions))

    @JvmStatic
    fun and(expressions: Stream<Expression>): Expression {
        val result = expressions
                .filter(Predicate.isEqual<Any>(NULL).negate())
                .flatMap<Expression> { expression ->
                    if (expression is OperationExpression && expression.type == OperationType.AND) {
                        return@flatMap expression.arguments.stream()
                    }

                    return@flatMap _Lists.of(expression).stream()
                }
                .collect(_Lists.collect())

        if (result.isEmpty()) {
            return NULL
        }

        val first = _Iterables.first(result)
        if (result.size == 1 && first != null) {
            return first
        }
        else {
            return OperationExpression(OperationType.AND, result)
        }
    }

    @JvmStatic
    fun or(vararg expressions: Expression) = and(_Lists.of(*expressions))

    @JvmStatic
    fun or(expressions: Iterable<Expression>) = and(_Streams.from(expressions))

    @JvmStatic
    fun or(expressions: Stream<Expression>): Expression {
        val result = expressions
                .filter(Predicate.isEqual<Any>(NULL).negate())
                .flatMap<Expression> { expression ->
                    if (expression is OperationExpression && expression.type == OperationType.OR) {
                        return@flatMap expression.arguments.stream()
                    }

                    return@flatMap _Lists.of(expression).stream()
                }
                .collect(_Lists.collect())

        if (result.isEmpty()) {
            return NULL
        }

        val first = _Lists.first(result)
        if (result.size == 1 && first != null) {
            return first
        }
        else {
            return OperationExpression(OperationType.OR, result)
        }
    }

    @JvmStatic
    fun operation(field: String, type: OperationType, value: Any): Expression {
        return OperationExpression(type, _Lists.of(_Expressions.field(field), _Expressions.value(value)))
    }

    @JvmStatic
    fun operation(field1: Expression, type: OperationType, field2: Expression): Expression {
        return OperationExpression(type, _Lists.of(field1, field2))
    }

    @JvmStatic
    fun isEmpty(rawField: String): Expression {
        return OperationExpression(OperationType.IS_EMPTY, _Lists.of(_Expressions.field(rawField)))
    }
}