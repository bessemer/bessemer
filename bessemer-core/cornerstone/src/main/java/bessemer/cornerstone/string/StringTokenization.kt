package bessemer.cornerstone.string

import bessemer.cornerstone.collection.SingleEntryIterator
import com.google.common.base.Preconditions
import org.apache.commons.lang3.StringUtils
import java.util.NoSuchElementException

data class StringToken(val value: String, val index: Int, val length: Int)

class StringTokenIterator(
        source: String?,
        private val tokenPrefix: String,
        private val tokenSuffix: String) : SingleEntryIterator<StringToken>() {
    private val source: String

    private var currentLocation = 0

    init {
        Preconditions.checkNotNull(tokenPrefix)
        Preconditions.checkNotNull(tokenSuffix)

        this.source = StringUtils.defaultString(source)
    }

    @Throws(NoSuchElementException::class)
    override fun generateNext(): StringToken {
        if (currentLocation >= source.length - 1) {
            throw NoSuchElementException()
        }

        val tokenPrefixIndex = source.indexOf(tokenPrefix, currentLocation)
        if (tokenPrefixIndex == -1) {
            throw NoSuchElementException()
        }

        currentLocation = tokenPrefixIndex + tokenPrefix.length

        val tokenSuffixIndex = source.indexOf(tokenSuffix, currentLocation)
        if (tokenSuffixIndex == -1) {
            throw NoSuchElementException()
        }

        // If we don't want tokens to overlap, we should add the token suffix to the current location as well
        // Right now we default to overlap but that's probably bad - we default to it tho because we're using
        // This for words, which do have placeholder overlap
        // Perhaps this should be an option?
        currentLocation = tokenSuffixIndex

        val value = source.substring(tokenPrefixIndex + tokenPrefix.length, tokenSuffixIndex)
        return StringToken(value, tokenPrefixIndex, tokenPrefix.length + value.length + tokenSuffix.length)
    }
}

class StringTokenIterable(
        private val source: String?, private val tokenPrefix: String, private val tokenSuffix: String) : Iterable<StringToken> {
    override fun iterator(): Iterator<StringToken> {
        return StringTokenIterator(source, tokenPrefix, tokenSuffix)
    }
}