package bessemer.cornerstone.string

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Streams
import org.apache.commons.lang3.StringUtils
import java.util.*
import java.util.stream.Stream

object _Strings {
    fun toString(tokenValue: Any): String? {
        return toString(tokenValue, null)
    }

    fun toString(tokenValue: Any?, defaultValue: String?): String? {
        return tokenValue?.toString() ?: defaultValue
    }

    @JvmStatic
    fun isEmpty(s: String?) = StringUtils.isEmpty(s)

    @JvmStatic
    fun isBlank(s: String?) = StringUtils.isBlank(s)

    @JvmStatic
    fun splitPair(target: String?, separator: String): Optional<Pair<String, String>> {
        val partitionStream = _Streams.findOnly(_Streams.partition(split(target, separator), 2))
        if(partitionStream == null) {
            return Optional.empty()
        }

        val partition = partitionStream.collect(_Lists.collect())
        if(partition.size != 2) {
            return Optional.empty()
        }

        return Optional.of(partition[0] to partition[1])
    }

    @JvmStatic
    fun split(source: String?, separator: String) = split(source, separator, separator)

    @JvmStatic
    fun split(source: String?, tokenPrefix: String, tokenSuffix: String) = tokenize(source, tokenPrefix, tokenSuffix).map { it.value }

    @JvmStatic
    fun tokenize(source: String?, separator: String) = tokenize(source, separator, separator)

    @JvmStatic
    fun tokenize(source: String?, tokenPrefix: String, tokenSuffix: String): Stream<StringToken> {
        return _Streams.from(StringTokenIterable(source, tokenPrefix, tokenSuffix))
    }

    /**
     * Replace all occurrences of a substring within a string with
     * another string.
     * @param inString `String` to examine
     * @param oldPattern `String` to replace
     * @param newPattern `String` to insert
     * @return a `String` with the replacements
     */
    fun replace(inString: String, oldPattern: String, newPattern: String?): String {
        if (isEmpty(inString) || isEmpty(oldPattern) || newPattern == null) {
            return inString
        }
        var index = inString.indexOf(oldPattern)
        if (index == -1) {
            // no occurrence -> can return input as-is
            return inString
        }

        var capacity = inString.length
        if (newPattern.length > oldPattern.length) {
            capacity += 16
        }
        val sb = StringBuilder(capacity)

        var pos = 0  // our position in the old string
        val patLen = oldPattern.length
        while (index >= 0) {
            sb.append(inString.substring(pos, index))
            sb.append(newPattern)
            pos = index + patLen
            index = inString.indexOf(oldPattern, pos)
        }

        // append any characters to the right of a match
        sb.append(inString.substring(pos))
        return sb.toString()
    }
}