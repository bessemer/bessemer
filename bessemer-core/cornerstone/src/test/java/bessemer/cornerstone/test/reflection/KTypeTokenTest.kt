package bessemer.cornerstone.test.reflection

import bessemer.cornerstone.reflection.TypeToken
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class KTypeTokenTest {
    @Test
    fun testTypeToken() {
        run {
            val type = TypeToken.create<String>()
            Assertions.assertEquals("kotlin.String", type.type.toString())
            Assertions.assertEquals("class java.lang.String", type.javaType.toString())
        }

        run {
            val type = TypeToken.create<List<String>>()
            Assertions.assertEquals("kotlin.collections.List<kotlin.String>", type.type.toString())
            Assertions.assertEquals("java.util.List<? extends java.lang.String>", type.javaType.toString())
        }
    }
}