package bessemer.cornerstone.test.collection;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Iterables;
import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Sets;

public class IterablesTestJava {
    @Test
    public void testFirst() {
        Assertions.assertEquals(Optional.empty(), _Iterables.first(null));
        Assertions.assertEquals(Optional.empty(), _Iterables.first(_Lists.empty()));
        Assertions.assertEquals(Optional.empty(), _Iterables.first(_Sets.empty()));
        Assertions.assertEquals(Optional.of(1), _Iterables.first(_Lists.of(1)));
        Assertions.assertEquals(Optional.of(1), _Iterables.first(_Lists.of(1, 2, 3)));
        Assertions.assertEquals(Optional.of(3), _Iterables.first(_Lists.of(3, 2, 1)));
    }

    @Test
    public void testOnly() {
        Assertions.assertEquals(Optional.empty(), _Iterables.only(null));
        Assertions.assertEquals(Optional.empty(), _Iterables.only(_Lists.empty()));
        Assertions.assertEquals(Optional.empty(), _Iterables.only(_Sets.empty()));
        Assertions.assertEquals(Optional.of(1), _Iterables.only(_Lists.of(1)));
        Assertions.assertThrows(IllegalArgumentException.class, () -> _Iterables.only(_Lists.of(1, 2, 3)));
        Assertions.assertThrows(IllegalArgumentException.class, () -> _Iterables.only(_Lists.of(3, 2, 1)));
    }

    @Test
    public void testTake() {
        Assertions.assertEquals(_Lists.empty(), _Iterables.take(_Lists.empty(), 1));
        Assertions.assertEquals(_Lists.empty(), _Iterables.take(_Lists.empty(), 3));
        Assertions.assertEquals(_Lists.empty(), _Iterables.take(_Lists.empty(), 0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> _Iterables.take(_Lists.empty(), -1));
        Assertions.assertEquals(_Lists.of(1), _Iterables.take(_Lists.of(1, 2, 3), 1));
        Assertions.assertEquals(_Lists.of(1, 2, 3), _Iterables.take(_Lists.of(1, 2, 3), 3));
        Assertions.assertEquals(_Lists.of(1, 2), _Iterables.take(_Lists.of(1, 2), 3));
        Assertions.assertEquals(_Lists.empty(), _Iterables.take(_Lists.of(1, 2, 3), 0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> _Iterables.take(_Lists.of(1, 2, 3), -1));
    }
}
