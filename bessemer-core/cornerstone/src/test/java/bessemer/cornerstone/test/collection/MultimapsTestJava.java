package bessemer.cornerstone.test.collection;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.google.common.collect.Multimap;

import bessemer.cornerstone.util._Tuples;
import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Multimaps;
import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Multimaps;
import bessemer.cornerstone.util._Tuples;
import kotlin.Pair;

public class MultimapsTestJava {
	@Test
	public void testCollectors() {
		List<Pair<String, Integer>> data = _Lists.of(
				_Tuples.pair("one", 10),
				_Tuples.pair("two", 10),
				_Tuples.pair("two", 10),
				_Tuples.pair("three", 10),
				_Tuples.pair("three", 10),
				_Tuples.pair("three", 10)
		);

		{
			Multimap<String, Integer> multimap = data.stream().collect(_Multimaps.collect());
			Assertions.assertEquals(1, multimap.get("one").size());
			Assertions.assertEquals(2, multimap.get("two").size());
			Assertions.assertEquals(3, multimap.get("three").size());
		}

		{
			Multimap<String, Integer> multimap = data.stream().collect(_Multimaps.collectSet());
			Assertions.assertEquals(1, multimap.get("one").size());
			Assertions.assertEquals(1, multimap.get("two").size());
			Assertions.assertEquals(1, multimap.get("three").size());
		}

	}
}
