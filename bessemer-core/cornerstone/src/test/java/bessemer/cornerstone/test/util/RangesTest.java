package bessemer.cornerstone.test.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.google.common.collect.Range;

import bessemer.cornerstone.util._Ranges;

public class RangesTest {
    @Test
    public void testSize() {
        Assertions.assertEquals(4, _Ranges.size(Range.open(0, 5)));
        Assertions.assertEquals(6, _Ranges.size(Range.closed(0, 5)));
        Assertions.assertEquals(5, _Ranges.size(Range.closedOpen(0, 5)));
        Assertions.assertEquals(0, _Ranges.size(Range.closedOpen(0, 0)));
    }

    @Test
    public void testLimit() {
        Assertions.assertEquals(_Ranges.EMPTY_INTEGERS, _Ranges.limit(Range.closed(0, 0), 0));
        Assertions.assertEquals(_Ranges.EMPTY_INTEGERS, _Ranges.limit(Range.closed(0, 50), 0));
        Assertions.assertEquals(_Ranges.EMPTY_INTEGERS, _Ranges.limit(Range.atLeast(0), 0));
        Assertions.assertEquals(Range.openClosed(0, 0), _Ranges.limit(Range.greaterThan(0), 0));
        Assertions.assertEquals(Range.closedOpen(0, 50), _Ranges.limit(Range.atLeast(0), 50));
        Assertions.assertEquals(Range.openClosed(0, 50), _Ranges.limit(Range.greaterThan(0), 50));
        Assertions.assertThrows(IllegalArgumentException.class, () -> _Ranges.limit(Range.all(), 50));
        Assertions.assertEquals(Range.closedOpen(50, 100), _Ranges.limit(Range.closed(50, 100), 50));
        Assertions.assertEquals(Range.greaterThan(0), _Ranges.limit(Range.greaterThan(0), null));
        Assertions.assertEquals(Range.greaterThan(0), _Ranges.limit(Range.openClosed(0, 50), null));
        Assertions.assertEquals(Range.atLeast(0), _Ranges.limit(Range.closed(0, 50), null));
    }

    @Test
    public void testOffset() {
        Assertions.assertEquals(Range.atLeast(0), _Ranges.offset(Range.greaterThan(0), null));
        Assertions.assertEquals(Range.atLeast(0), _Ranges.offset(Range.atLeast(50), null));
        Assertions.assertEquals(Range.atLeast(25), _Ranges.offset(Range.atLeast(0), 25));
        Assertions.assertEquals(Range.atLeast(25), _Ranges.offset(Range.atLeast(50), 25));
        Assertions.assertEquals(Range.closed(50, 50), _Ranges.offset(Range.closed(0, 0), 50));
        Assertions.assertEquals(Range.closed(50, 54), _Ranges.offset(Range.openClosed(0, 5), 50));
        Assertions.assertEquals(Range.closed(50, 54), _Ranges.offset(Range.closedOpen(0, 5), 50));
        Assertions.assertEquals(Range.closed(50, 55), _Ranges.offset(Range.closed(0, 5), 50));
        Assertions.assertEquals(Range.closed(50, 53), _Ranges.offset(Range.open(0, 5), 50));
        Assertions.assertEquals(Range.closed(0, 4), _Ranges.offset(Range.openClosed(0, 5), null));
        Assertions.assertEquals(Range.closed(0, 3), _Ranges.offset(Range.open(0, 5), null));
        Assertions.assertEquals(Range.closed(0, 4), _Ranges.offset(Range.closedOpen(0, 5), null));
        Assertions.assertEquals(Range.closed(0, 5), _Ranges.offset(Range.closed(0, 5), null));
    }
}
