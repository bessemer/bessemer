package bessemer.cornerstone.test.data;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Sets;
import bessemer.cornerstone.data.DataSet;
import bessemer.cornerstone.data.PagingDataSet;
import bessemer.cornerstone.data._DataSets;

public class DataSetTestJava {
    @Test
    public void testEmpty() {
        PagingDataSet<String> dataSet =_DataSets.empty();
        Assertions.assertEquals(0, dataSet.size());
        Assertions.assertEquals(_Lists.empty(), dataSet.toList());
        Assertions.assertEquals(_Sets.empty(), dataSet.toSet());
        Assertions.assertEquals(_Lists.empty(), dataSet.stream().collect(_Lists.collect()));

        Assertions.assertEquals(0, dataSet.pageLexicon().entries().size());
        Assertions.assertEquals(0, dataSet.pageLexicon().keys().size());
    }

    @Test
    public void testFromList() {
        List<Integer> sampleList = _Lists.of(1, 2, 3, 4, 5);
        DataSet<Integer> dataSet = _DataSets.from(sampleList);

        Assertions.assertEquals(5, dataSet.size());
        Assertions.assertEquals(sampleList, dataSet.toList());
        Assertions.assertEquals(_Sets.coerce(sampleList), dataSet.toSet());
        Assertions.assertEquals(sampleList, dataSet.stream().collect(_Lists.collect()));
    }
}
