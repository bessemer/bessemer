package bessemer.cornerstone.test.util;

import java.util.Map;
import java.util.Objects;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import bessemer.cornerstone.collection._Maps;
import bessemer.cornerstone.util.TypeConverter;
import bessemer.cornerstone.util._TypeConverters;

public class TypeConvertersTest {
	public static class TypeConvertersTestDto {
		private String field1;
		private String field2;
		private Map<String, Long> field3;

		private TypeConvertersTestDto() {
			// For serialization
		}

		public TypeConvertersTestDto(String field1, String field2, Map<String, Long> field3) {
			this.field1 = field1;
			this.field2 = field2;
			this.field3 = field3;
		}

		public String getField1() {
			return field1;
		}

		public String getField2() {
			return field2;
		}

		public Map<String, Long> getField3() {
			return field3;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			TypeConvertersTestDto that = (TypeConvertersTestDto) o;
			return Objects.equals(field1, that.field1) &&
					Objects.equals(field2, that.field2) &&
					Objects.equals(field3, that.field3);
		}

		@Override
		public int hashCode() {
			return Objects.hash(field1, field2, field3);
		}
	}

	@Test
	public void testTypeConverterConfiguration() {
		TypeConverter globalTypeConverter = _TypeConverters.getGlobalTypeConverter();
		Assertions.assertSame(globalTypeConverter, _TypeConverters.getGlobalTypeConverter());

		TypeConverter newTypeConverter = TypeConverter.from(new ObjectMapper());
		_TypeConverters.setGlobalTypeConverter(newTypeConverter);
		Assertions.assertSame(newTypeConverter, _TypeConverters.getGlobalTypeConverter());

		_TypeConverters.setGlobalTypeConverter(globalTypeConverter);
	}

	@Test
	public void testTypeConverters() {
		TypeConvertersTestDto test = new TypeConvertersTestDto("field1", "field2", _Maps.of("one", 1L, "two", 2L, "three", 3L));
		Map<String, Object> mapRepresentation = _Maps.of(
				"field1", "field1",
				"field2", "field2",
				"field3", _Maps.of("one", 1L, "two", 2L, "three", 3L));

		Assertions.assertEquals(mapRepresentation, _TypeConverters.mapify(test));
		Assertions.assertEquals(test, _TypeConverters.convert(mapRepresentation, TypeConvertersTestDto.class));
		Assertions.assertEquals("{\"One\":1}", _TypeConverters.convert(_Maps.of("One", 1), String.class));
	}
}