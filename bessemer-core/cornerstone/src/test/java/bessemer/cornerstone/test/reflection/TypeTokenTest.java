package bessemer.cornerstone.test.reflection;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.reflection.TypeToken;
import bessemer.cornerstone.reflection.TypeToken;

public class TypeTokenTest {
	@Test
	public void testTypeToken() {
		{
			TypeToken<String> type = new TypeToken<String>() {};
			Assertions.assertEquals("kotlin.String", type.getType().toString());
			Assertions.assertEquals("class java.lang.String", type.getJavaType().toString());
		}

		{
			TypeToken<List<String>> type = new TypeToken<List<String>>() {};
			Assertions.assertEquals("kotlin.collections.List<kotlin.String>", type.getType().toString());
			Assertions.assertEquals("java.util.List<java.lang.String>", type.getJavaType().toString());
		}
	}
}