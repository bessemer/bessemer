package bessemer.cornerstone.test.collection;

import java.util.Collection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Collections;
import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Sets;

public class CollectionsTestJava {
	@Test
	public void testIsEmpty() {
		Assertions.assertTrue(_Collections.isEmpty(null));
		Assertions.assertTrue(_Collections.isEmpty(_Lists.empty()));
		Assertions.assertTrue(_Collections.isEmpty(_Sets.empty()));
		Assertions.assertTrue(_Collections.isEmpty((Collection<Object>) _Lists.empty()));
		Assertions.assertFalse(_Collections.isEmpty(_Lists.of(1)));
		Assertions.assertFalse(_Collections.isEmpty(_Sets.of(1)));
	}

	@Test
	public void testRemoveAll() {
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Lists.empty(), _Lists.empty()));
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Sets.empty(), _Lists.empty()));
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Sets.empty(), _Sets.empty()));
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Lists.empty(), _Sets.empty()));
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Lists.of(1), _Lists.of(1)));
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Lists.of(1), _Lists.of(1, 2, 3)));
		Assertions.assertEquals(_Lists.of(2, 3), _Collections.removeAll(_Lists.of(1, 2, 3), _Lists.of(1)));
		Assertions.assertEquals(_Lists.of(2), _Collections.removeAll(_Lists.of(1, 2, 3), _Lists.of(1, 3)));
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Sets.of(1), _Lists.of(1)));
		Assertions.assertEquals(_Lists.empty(), _Collections.removeAll(_Sets.of(1), _Lists.of(1, 2, 3)));
		Assertions.assertEquals(_Lists.of(2, 3), _Collections.removeAll(_Sets.of(1, 2, 3), _Lists.of(1)));
		Assertions.assertEquals(_Lists.of(2), _Collections.removeAll(_Sets.of(1, 2, 3), _Lists.of(1, 3)));
	}
}