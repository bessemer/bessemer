package bessemer.cornerstone.test.collection;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Streams;
import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Streams;
import kotlin.Pair;

public class StreamsTestJava {
	@Test
	public void testPredicatePartition() {
		{
			List<Boolean> test = _Lists.of();
			Pair<Stream<Boolean>, Stream<Boolean>> results = _Streams.partition(test.stream(), x -> x);

			Assertions.assertEquals(0, results.getFirst().collect(_Lists.collect()).size());
			Assertions.assertEquals(0, results.getSecond().collect(_Lists.collect()).size());
		}

		{
			List<Boolean> test = _Lists.of(true, true, true, true, true);
			Pair<Stream<Boolean>, Stream<Boolean>> results = _Streams.partition(test.stream(), x -> x);

			Assertions.assertEquals(5, results.getFirst().collect(_Lists.collect()).size());
			Assertions.assertEquals(0, results.getSecond().collect(_Lists.collect()).size());
		}

		{
			List<Boolean> test = _Lists.of(false);
			Pair<Stream<Boolean>, Stream<Boolean>> results = _Streams.partition(test.stream(), x -> x);

			Assertions.assertEquals(0, results.getFirst().collect(_Lists.collect()).size());
			Assertions.assertEquals(1, results.getSecond().collect(_Lists.collect()).size());
		}

		{
			List<Boolean> test = _Lists.of(true, true, false, true, false, true, true);
			Pair<Stream<Boolean>, Stream<Boolean>> results = _Streams.partition(test.stream(), x -> x);

			Assertions.assertEquals(5, results.getFirst().collect(_Lists.collect()).size());
			Assertions.assertEquals(2, results.getSecond().collect(_Lists.collect()).size());
		}
	}
}