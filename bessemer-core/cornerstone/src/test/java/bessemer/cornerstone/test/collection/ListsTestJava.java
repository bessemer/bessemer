package bessemer.cornerstone.test.collection;


import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Sets;
import bessemer.cornerstone.collection._Streams;
import bessemer.cornerstone.function._Predicates;
import bessemer.cornerstone.util._Math;
import bessemer.cornerstone.util._Tuples;

public class ListsTestJava {
	@Test
	public void testBuilders() {
		Assertions.assertTrue(_Lists.of().isEmpty());
		Assertions.assertSame(_Lists.of(), _Lists.of());

		List<String> singleElement = _Lists.of("Hello");
		Assertions.assertEquals(1, singleElement.size());
		Assertions.assertEquals("Hello", singleElement.get(0));

		List<List<String>> nestedList = _Lists.of(singleElement);
		Assertions.assertEquals(1, nestedList.size());

		List<String> copy = _Lists.copy(singleElement);
		Assertions.assertEquals(1, copy.size());
		Assertions.assertEquals("Hello", copy.get(0));
		Assertions.assertSame(singleElement, copy);

		Assertions.assertThrows(NullPointerException.class, () -> _Lists.of((Object) null));

		// TODO test mutable builders
	}

	@Test
	public void testCoerce() {
		List<Integer> list = _Lists.of(1, 2, 3);
		List<Integer> list2 = _Lists.ofMutable(1, 2, 3);
		Set<Integer> set = _Sets.of(1, 2, 3);

		Assertions.assertSame(list, _Lists.coerce(list));
		Assertions.assertNotSame(list, _Lists.coerce(list2));
		Assertions.assertEquals(list, _Lists.coerce(list2));
		Assertions.assertEquals(list, _Lists.coerce(set));
	}

	@Test
	public void testDefaulted() {
		List<Integer> list = _Lists.of(1, 2, 3);
		Assertions.assertEquals(list, _Lists.defaulted(list));
		Assertions.assertEquals(_Lists.empty(), _Lists.defaulted(null));
	}

	@Test
	public void testFirst() {
		List<Integer> list = _Lists.of(1, 2, 3);
		Assertions.assertEquals(Optional.of(1), _Lists.first(list));
		Assertions.assertEquals(Optional.empty(), _Lists.first(_Lists.empty()));
		Assertions.assertEquals(Optional.empty(), _Lists.first(_Sets.empty()));
	}

	@Test
	public void testLast() {
		List<Integer> list = _Lists.of(1, 2, 3);
		Assertions.assertEquals(Optional.of(3), _Lists.last(list));
		Assertions.assertEquals(Optional.empty(), _Lists.last(_Lists.empty()));
	}

	@Test
	public void testRest() {
		List<Integer> list = _Lists.of(1, 2, 3);
		Assertions.assertEquals(_Lists.of(2, 3), _Lists.rest(list));
		Assertions.assertEquals(_Lists.empty(), _Lists.rest(_Lists.empty()));
	}

	@Test
	public void testOnly() {
		Assertions.assertEquals(Optional.of(1), _Lists.only(_Lists.of(1)));
		Assertions.assertEquals(Optional.empty(), _Lists.only(_Lists.empty()));
		Assertions.assertThrows(IllegalArgumentException.class, () -> _Lists.only(_Lists.of(1, 2, 3)));
	}

	@Test
	public void testTake() {
		Assertions.assertEquals(_Lists.of(1), _Lists.take(_Lists.of(1), 1));
		Assertions.assertEquals(_Lists.of(1, 2), _Lists.take(_Lists.of(1, 2, 3), 2));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.take(_Lists.of(1, 2, 3), 1000));
		Assertions.assertEquals(_Lists.empty(), _Lists.take(_Lists.empty(), 1000));
		Assertions.assertEquals(_Lists.empty(), _Lists.take(_Lists.of(1, 2, 3), 0));
		Assertions.assertThrows(IllegalArgumentException.class, () -> _Lists.take(_Lists.of(1, 2, 3), -1));

		Assertions.assertEquals(_Lists.of(1), _Lists.take(_Lists.of(1).stream(), 1));
		Assertions.assertEquals(_Lists.of(1, 2), _Lists.take(_Lists.of(1, 2, 3).stream(), 2));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.take(_Lists.of(1, 2, 3).stream(), 1000));
		Assertions.assertEquals(_Lists.empty(), _Lists.take(_Lists.empty().stream(), 1000));
		Assertions.assertEquals(_Lists.empty(), _Lists.take(_Lists.of(1, 2, 3).stream(), 0));
		Assertions.assertThrows(IllegalArgumentException.class, () -> _Lists.take(_Lists.of(1, 2, 3).stream(), -1));
	}

	@Test
	public void testReverse() {
		Assertions.assertEquals(_Lists.of(1), _Lists.reverse(_Lists.of(1)));
		Assertions.assertEquals(_Lists.of(3, 2, 1), _Lists.reverse(_Lists.of(1, 2, 3)));
		Assertions.assertEquals(_Lists.empty(), _Lists.reverse(_Lists.empty()));
	}

	@Test
	public void testAppend() {
		Assertions.assertEquals(_Lists.empty(), _Lists.append(_Lists.empty()));
		Assertions.assertEquals(_Lists.of(1), _Lists.append(_Lists.of(1)));
		Assertions.assertEquals(_Lists.of(1), _Lists.append(_Lists.empty(), 1));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.append(_Lists.empty(), 1, 2, 3));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.append(_Lists.of(1, 2), 3));
		Assertions.assertThrows(NullPointerException.class, () -> _Lists.append(_Lists.of(1, 2, 3), (Object) null));
	}

	@Test
	public void testConcat() {
		Assertions.assertEquals(_Lists.empty(), _Lists.concat(_Lists.empty(), _Lists.empty(), _Lists.empty()));
		Assertions.assertEquals(_Lists.of(1, 1), _Lists.concat(_Lists.of(1), _Lists.empty(), _Lists.of(1), _Lists.empty()));
		Assertions.assertEquals(_Lists.of(1, 1, 2), _Lists.concat(_Lists.of(1), _Lists.empty(), _Lists.of(1, 2), _Lists.empty()));
		Assertions.assertEquals(_Lists.of(1, 2), _Lists.concat(_Lists.empty(), _Lists.of(1), _Lists.of(2)));
	}

	@Test
	public void testRemoveAll() {
		Assertions.assertEquals(_Lists.empty(), _Lists.removeAll(_Lists.empty(), 1, 2, 3));
		Assertions.assertEquals(_Lists.empty(), _Lists.removeAll(_Lists.of(1, 2, 3), 1, 2, 3));
		Assertions.assertEquals(_Lists.empty(), _Lists.removeAll(_Lists.of(1, 1, 1), 1));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.removeAll(_Lists.of(1, 2, 3), 4, 5, 6));
		Assertions.assertEquals(_Lists.of(2), _Lists.removeAll(_Lists.of(1, 2, 3), 1, 3, 4));
	}

	@Test
	public void testFilter() {
		Assertions.assertEquals(_Lists.empty(), _Lists.filter(_Lists.empty(), _Predicates.alwaysTrue()));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.filter(_Lists.of(1, 2, 3), _Predicates.alwaysTrue()));
		Assertions.assertEquals(_Lists.empty(), _Lists.filter(_Lists.of(1, 2, 3), _Predicates.alwaysFalse()));
		Assertions.assertEquals(_Lists.of(1, 2), _Lists.filter(_Lists.of(1, 1, 2, 1, 2, 1), _Predicates.matchDuplicates()));
		Assertions.assertEquals(_Lists.of(2, 4), _Lists.filter(_Lists.of(1, 2, 3, 4, 5), _Math::isEven));
	}

	@Test
	public void testPartition() {
		Assertions.assertEquals(_Lists.empty(), _Lists.partition(_Lists.empty(), 2));
		Assertions.assertThrows(IllegalArgumentException.class, () -> _Lists.partition(_Lists.empty(), 0));
		Assertions.assertThrows(IllegalArgumentException.class, () -> _Lists.partition(_Lists.empty(), -1));
		Assertions.assertEquals(_Lists.of(_Lists.of(1), _Lists.of(2), _Lists.of(3)), _Lists.partition(_Lists.of(1, 2, 3), 1));
		Assertions.assertEquals(_Lists.of(_Lists.of(1, 2), _Lists.of(3)), _Lists.partition(_Lists.of(1, 2, 3), 2));
		Assertions.assertEquals(_Lists.of(_Lists.of(1, 2, 3)), _Lists.partition(_Lists.of(1, 2, 3), 5));

		Assertions.assertEquals(_Tuples.pair(_Lists.empty(), _Lists.empty()), _Lists.partition(_Lists.empty(), _Predicates.alwaysTrue()));
		Assertions.assertEquals(_Tuples.pair(_Lists.of(1, 2, 3), _Lists.empty()), _Lists.partition(_Lists.of(1, 2, 3), _Predicates.alwaysTrue()));
		Assertions.assertEquals(_Tuples.pair(_Lists.empty(), _Lists.of(1, 2, 3)), _Lists.partition(_Lists.of(1, 2, 3), _Predicates.alwaysFalse()));
		Assertions.assertEquals(_Tuples.pair(_Lists.of(2, 4, 6), _Lists.of(1, 3, 5)), _Lists.partition(_Lists.of(1, 2, 3, 4, 5, 6), _Math::isEven));
	}

	@Test
	public void testSort() {
		Assertions.assertEquals(_Lists.empty(), _Lists.sort(_Lists.empty()));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.sort(_Lists.of(1, 2, 3)));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Lists.sort(_Lists.of(3, 1, 2)));
		Assertions.assertEquals(_Lists.of(3, 2, 1), _Lists.sort(_Lists.of(3, 1, 2), Comparator.<Integer>naturalOrder().reversed()));
	}

	@Test
	public void testCollect() {
		Assertions.assertEquals(_Lists.empty(), _Streams.empty().collect(_Lists.collect()));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Streams.of(1, 2, 3).collect(_Lists.collect()));
		Assertions.assertEquals(_Lists.empty(), _Streams.empty().collect(_Lists.collectMutable()));
		Assertions.assertEquals(_Lists.of(1, 2, 3), _Streams.of(1, 2, 3).collect(_Lists.collectMutable()));

		List<Integer> mutableList = _Streams.<Integer>empty().collect(_Lists.collectMutable());
		mutableList.add(1);
		Assertions.assertEquals(_Lists.of(1), mutableList);

		mutableList = _Streams.of(1, 2, 3).collect(_Lists.collectMutable());
		mutableList.add(4);
		Assertions.assertEquals(_Lists.of(1, 2, 3, 4), mutableList);

		//TODO there are two other collect methods we should probably call here
	}
}