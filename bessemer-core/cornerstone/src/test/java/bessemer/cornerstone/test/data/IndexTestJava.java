package bessemer.cornerstone.test.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Sets;
import bessemer.cornerstone.data.PagingLexicon;
import bessemer.cornerstone.data._Indexes;

public class IndexTestJava {
    @Test
    public void testEmpty() {
        PagingLexicon<Long, String> lexicon = _Indexes.empty();
        Assertions.assertEquals(0, lexicon.size());
        Assertions.assertEquals(_Lists.empty(), lexicon.toList());
        Assertions.assertEquals(_Sets.empty(), lexicon.toSet());
        Assertions.assertEquals(_Lists.empty(), lexicon.stream().collect(_Lists.collect()));
        Assertions.assertEquals(0, lexicon.entries().size());
        Assertions.assertEquals(0, lexicon.keys().size());
        Assertions.assertEquals(0, lexicon.pageLexicon().entries().size());
        Assertions.assertEquals(0, lexicon.pageLexicon().keys().size());
    }
}
