package bessemer.cornerstone.test.function;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.function._Functions;

public class FunctionTest {
    @Test
    public void testIdentity() {
        List<Integer> expectedList = _Lists.of(1, 2, 3);
        List<Integer> testList = expectedList.stream().map(_Functions::identity).collect(_Lists.collect());
        Assertions.assertEquals(expectedList, testList);
    }
}