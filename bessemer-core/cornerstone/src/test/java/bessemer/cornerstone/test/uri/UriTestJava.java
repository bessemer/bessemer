package bessemer.cornerstone.test.uri;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Maps;
import bessemer.cornerstone.uri.UriAuthority;
import bessemer.cornerstone.uri.UriContainer;
import bessemer.cornerstone.uri.UriLocation;
import bessemer.cornerstone.uri._Uris;

public class UriTestJava {
	@Test
	public void test() {
		{
			List<UriContainer> emptyUris = _Lists.of(
					_Uris.parse(null),
					_Uris.parse(""),
					_Uris.parse("                     "),
					_Uris.parse("/"),
					_Uris.parse("//////"));

			emptyUris.forEach(uri -> Assertions.assertEquals(UriContainer.EMPTY, uri));
		}

		{
			Assertions.assertEquals(_Uris.parse("http://localhost:8080"), _Uris.parse("http://localhost:8080//////"));
		}

		{
			UriContainer uri =
					_Uris.build()
							.scheme("http")
							.authority(new UriAuthority("localhost", 8080))
							.location(new UriLocation(
									_Lists.of("test", "url"),
									_Maps.of("param1", _Lists.of("value1", "value2"))));

			Assertions.assertEquals("http://localhost:8080/test/url?param1=value1&param1=value2", uri.toString());
		}

		{
			UriContainer blah = _Uris.parse("localhost:8080");
			System.out.println(blah);

			UriContainer blah2 = _Uris.parse("mailto:John.Doe@example.com");
			System.out.println(blah2);
		}

		{
			UriContainer blah = _Uris.parse("https://via.placeholder.com/100x100.png");
			System.out.println(blah);
		}
	}
}