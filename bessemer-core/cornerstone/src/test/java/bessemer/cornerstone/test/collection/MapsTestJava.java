package bessemer.cornerstone.test.collection;

import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Maps;
import bessemer.cornerstone.collection._Sets;
import bessemer.cornerstone.collection._Streams;
import bessemer.cornerstone.domain.Referent;
import bessemer.cornerstone.util._Tuples;

public class MapsTestJava {
    @Test
    public void testBuilders() {
        Assertions.assertEquals(_Maps.empty(), _Maps.empty());
        Assertions.assertEquals(_Maps.empty(), _Maps.of());
        Assertions.assertNotEquals(_Maps.empty(), _Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1")), _Maps.of(1, "1"));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")), _Maps.of(1, "1", 2, "2"));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"), _Tuples.pair(3, "3")), _Maps.of(1, "1", 2, "2", 3, "3"));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"), _Tuples.pair(3, "3"), _Tuples.pair(4, "4")), _Maps.of(1, "1", 2, "2", 3, "3", 4, "4"));

        Assertions.assertThrows(NullPointerException.class, () -> _Maps.of(1, "1", 2, null));
        Assertions.assertThrows(NullPointerException.class, () -> _Maps.of(1, "1", null, "2"));

        Assertions.assertEquals(_Maps.empty(), _Maps.emptyMutable());
        Assertions.assertEquals(_Maps.empty(), _Maps.ofMutable());

        Map<Integer, String> mutableMap = _Maps.emptyMutable();
        mutableMap.put(1, "1");
        Assertions.assertEquals(_Maps.of(1, "1"), mutableMap);

        mutableMap = _Maps.ofMutable();
        mutableMap.put(1, "1");
        Assertions.assertEquals(_Maps.of(1, "1"), mutableMap);

        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1")), _Maps.ofMutable(1, "1"));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")), _Maps.ofMutable(1, "1", 2, "2"));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"), _Tuples.pair(3, "3")), _Maps.ofMutable(1, "1", 2, "2", 3, "3"));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"), _Tuples.pair(3, "3"), _Tuples.pair(4, "4")), _Maps.ofMutable(1, "1", 2, "2", 3, "3", 4, "4"));

        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")), _Maps.from(_Lists.of(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"))));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")), _Maps.from(_Streams.of(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"))));

        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")), _Maps.fromMutable(_Lists.of(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"))));
        Assertions.assertEquals(_Maps.from(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")), _Maps.fromMutable(_Streams.of(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"))));

        Assertions.assertNull(_Maps.ofMutable(1, "1", 2, null).get(2));
        Assertions.assertEquals("2", _Maps.ofMutable(1, "1", null, "2").get(null));

        mutableMap = _Maps.fromMutable(_Lists.of(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")));
        mutableMap.put(3, "3");
        Assertions.assertEquals(_Maps.of(1, "1", 2, "2", 3, "3"), mutableMap);

        mutableMap = _Maps.fromMutable(_Streams.of(_Tuples.pair(1, "1"), _Tuples.pair(2, "2")));
        mutableMap.put(3, "3");
        Assertions.assertEquals(_Maps.of(1, "1", 2, "2", 3, "3"), mutableMap);
    }

    @Test
    public void testAccessors() {
        Map<Integer, String> map = _Maps.ofMutable(1, "1", 2, "2", null, "3", 4, null);
        Assertions.assertEquals(Referent.present("1"), _Maps.getReferent(map, 1));
        Assertions.assertEquals(Referent.present("2"), _Maps.getReferent(map, 2));
        Assertions.assertEquals(Referent.absent(), _Maps.getReferent(map, 3));
        Assertions.assertEquals(Referent.present(null), _Maps.getReferent(map, 4));
        Assertions.assertEquals(Referent.present("3"), _Maps.getReferent(map, null));

        Assertions.assertEquals(
                _Streams.empty().collect(_Sets.collect()),
                _Maps.stream(_Maps.empty()).collect(_Sets.collect()));

        Assertions.assertEquals(
                _Streams.of(_Tuples.pair(1, "1"), _Tuples.pair(2, "2"), _Tuples.pair(null, "3"), _Tuples.pair(4, null)).collect(_Sets.collect()),
                _Maps.stream(map).collect(_Sets.collect()));
    }

    @Test
    public void testAppend() {
        Map<Integer, String> existingMap = _Maps.of(1, "1", 2, "2", 3, "3");
        Assertions.assertEquals(_Maps.of(1, "1", 2, "2", 3, "3", 4, "4"), _Maps.append(existingMap, 4, "4"));
        Assertions.assertEquals(_Maps.of(1, "1", 2, "2", 3, "3"), existingMap);
        Assertions.assertThrows(IllegalArgumentException.class, () -> _Maps.append(existingMap, 3, "3"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> _Maps.append(existingMap, 2, "Two"));
    }

    @Test
    public void testMapValues() {
        Map<Integer, String> expected = _Maps.of(1, "1", 2, "2", 3, "3");
        Map<Integer, String> result = _Maps.mapValues(_Streams.of(1, 2, 3), Object::toString);
        Assertions.assertEquals(expected, result);
    }
}
