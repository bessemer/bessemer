package bessemer.cornerstone.test.repository;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.collection._Sets;
import bessemer.cornerstone.collection._Streams;
import bessemer.cornerstone.data.PagingLexicon;
import bessemer.cornerstone.repository.Repository;
import bessemer.cornerstone.repository._Repositories;
import kotlin.Pair;

public class RepositoryTestJava {
    static class TestEntity {
        private final int key;

        public TestEntity(int key) {
            this.key = key;
        }

        public int getKey() {
            return key;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestEntity that = (TestEntity) o;
            return key == that.key;
        }

        @Override
        public int hashCode() {
            return Objects.hash(key);
        }
    }

    private Repository<TestEntity, Integer> repository = _Repositories.empty(TestEntity::getKey);

    @Test
    public void testSave() {
        TestEntity entity = repository.save(new TestEntity(1));
        List<TestEntity> entities = repository.save(_Lists.of(new TestEntity(1), new TestEntity(2), new TestEntity(3)));
        entities = repository.save(new TestEntity(1), new TestEntity(2), new TestEntity(3));
        entities = repository.save(_Sets.of(new TestEntity(1), new TestEntity(2), new TestEntity(3)));

        Assertions.assertEquals(3, repository.findAll().size());

    }

    @Test
    public void testDelete() {
        repository.save(new TestEntity(1), new TestEntity(2), new TestEntity(3));

        repository.delete(new TestEntity(1));
        repository.delete(_Lists.of(new TestEntity(1), new TestEntity(2), new TestEntity(3)));
        repository.delete(_Sets.of(new TestEntity(1), new TestEntity(2), new TestEntity(3)));

        Assertions.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void testRemove() {
        repository.remove(1);
        repository.remove(_Lists.of(1, 1, 1));
        repository.remove(_Sets.of(1, 1, 1));
        repository.removeAll();
    }

    @Test
    public void testFind() {
        TestEntity entity = repository.find(1);
        Optional<TestEntity> optionalEntity = repository.findOptional(1);
        Stream<Pair<Integer, TestEntity>> streamResults = repository.findEach(_Streams.of(1, 1, 1));
        Map<Integer, TestEntity> results = repository.findEach(_Lists.of(1, 1, 1));
        results = repository.findEach(_Sets.of(1, 1, 1));
    }

    @Test
    public void testFindAll() {
        PagingLexicon<Integer, TestEntity> results = repository.findAll();

        Assertions.assertEquals(0, results.size());
        Assertions.assertEquals(_Lists.empty(), results.toList());
        Assertions.assertEquals(_Sets.empty(), results.toSet());
        Assertions.assertEquals(_Lists.empty(), results.stream().collect(_Lists.collect()));
        Assertions.assertEquals(0, results.entries().size());
        Assertions.assertEquals(0, results.keys().size());
        Assertions.assertEquals(0, results.pageLexicon().entries().size());
        Assertions.assertEquals(0, results.pageLexicon().keys().size());
    }
}
