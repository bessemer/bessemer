package bessemer.cornerstone.test.schema;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.collection._Lists;
import bessemer.cornerstone.reflection.TypeToken;
import bessemer.cornerstone.schema.SchemaType;
import bessemer.cornerstone.schema.TypeResolver;
import bessemer.cornerstone.util._Tuples;
import kotlin.Pair;

public class TypeResolverTest {
	@Test
	public void testTypeResolver() {
		List<Pair<TypeToken<?>, SchemaType>> typesToResolve = _Lists.of(
				_Tuples.pair(new TypeToken<Object>(){}, SchemaType.Any),
				_Tuples.pair(new TypeToken<Byte>(){}, SchemaType.Byte),
				_Tuples.pair(new TypeToken<Boolean>(){}, SchemaType.Boolean),
				_Tuples.pair(new TypeToken<Character>(){}, SchemaType.Character),
				_Tuples.pair(new TypeToken<String>(){}, SchemaType.String),
				_Tuples.pair(new TypeToken<Short>(){}, SchemaType.Number),
				_Tuples.pair(new TypeToken<Integer>(){}, SchemaType.Number),
				_Tuples.pair(new TypeToken<Long>(){}, SchemaType.Number),
				_Tuples.pair(new TypeToken<Double>(){}, SchemaType.Number),
				_Tuples.pair(new TypeToken<Float>(){}, SchemaType.Number),
				_Tuples.pair(new TypeToken<BigInteger>(){}, SchemaType.Number),
				_Tuples.pair(new TypeToken<BigDecimal>(){}, SchemaType.Number),
				_Tuples.pair(new TypeToken<Date>(){}, SchemaType.Instant),
				_Tuples.pair(new TypeToken<Instant>(){}, SchemaType.Instant),
				_Tuples.pair(new TypeToken<List<String>>(){}, SchemaType.List(SchemaType.String)),
				_Tuples.pair(new TypeToken<ArrayList<String>>(){}, SchemaType.List(SchemaType.String)),
				_Tuples.pair(new TypeToken<Set<String>>(){}, SchemaType.Set(SchemaType.String)),
				_Tuples.pair(new TypeToken<HashSet<String>>(){}, SchemaType.Set(SchemaType.String)),
				_Tuples.pair(new TypeToken<Map<String, String>>(){}, SchemaType.Map(SchemaType.String, SchemaType.String)),
				_Tuples.pair(new TypeToken<HashMap<String, String>>(){}, SchemaType.Map(SchemaType.String, SchemaType.String)),
				_Tuples.pair(new TypeToken<Map<String, List<String>>>(){}, SchemaType.Map(SchemaType.String, SchemaType.List(SchemaType.String))),
				_Tuples.pair(new TypeToken<HashMap<String, List<HashSet<String>>>>(){}, SchemaType.Map(SchemaType.String, SchemaType.List(SchemaType.Set(SchemaType.String)))),
				_Tuples.pair(new TypeToken<SchemaType>(){}, SchemaType.of(SchemaType.class)),
				_Tuples.pair(new TypeToken<List<SchemaType>>(){}, SchemaType.List(SchemaType.of(SchemaType.class))),
				_Tuples.pair(new TypeToken<Map<SchemaType, String>>(){}, SchemaType.Map(SchemaType.of(SchemaType.class), SchemaType.String))
		);

		typesToResolve.forEach(request -> {
			SchemaType result = TypeResolver.DEFAULT.resolve(request.getFirst());
			Assertions.assertEquals(request.getSecond(), result);
		});
	}
}