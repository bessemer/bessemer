package bessemer.cornerstone.test.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.domain.Outcome;
import bessemer.cornerstone.exception._Exceptions;
import kotlin.Unit;

public class ExceptionTest {
    static class TestCheckedException extends Exception {
        public static TestCheckedException INSTANCE = new TestCheckedException();
    }

    public void methodThatSucceeds() throws TestCheckedException {

    }

    public void methodThatFails() throws TestCheckedException {
        throw TestCheckedException.INSTANCE;
    }

    @Test
    public void testTryOutcome() {
        {
            Outcome<Unit, Exception> actualResult = _Exceptions.tryOutcome(() -> {
                this.methodThatSucceeds();
            });

            Assertions.assertEquals(Outcome.success(Unit.INSTANCE), actualResult);
        }

        {
            Outcome<Unit, Exception> actualResult = _Exceptions.tryOutcome(() -> {
                this.methodThatFails();
            });

            Assertions.assertEquals(Outcome.error(TestCheckedException.INSTANCE), actualResult);
        }

        {
            Outcome<Integer, Exception> actualResult = _Exceptions.tryOutcome(() -> {
                this.methodThatSucceeds();
                return 1;
            });

            Assertions.assertEquals(Outcome.success(1), actualResult);
        }

        {
            Outcome<Integer, Exception> actualResult = _Exceptions.tryOutcome(() -> {
                this.methodThatFails();
                return 1;
            });

            Assertions.assertEquals(Outcome.error(TestCheckedException.INSTANCE), actualResult);
        }
    }

    @Test
    public void testTryPropagate() {
        {
            Assertions.assertDoesNotThrow(() -> {
                _Exceptions.tryPropagate(() -> {
                    this.methodThatSucceeds();
                });
            });
        }

        {
            Assertions.assertThrows(TestCheckedException.class, () -> {
                _Exceptions.tryPropagate(() -> {
                    this.methodThatFails();
                });
            });
        }

        {
            Integer actualResult = _Exceptions.tryPropagate(() -> {
                this.methodThatSucceeds();
                return 1;
            });

            Assertions.assertEquals(1, actualResult);
        }

        {
            Assertions.assertThrows(TestCheckedException.class, () -> {
                _Exceptions.tryPropagate(() -> {
                    this.methodThatFails();
                    return 1;
                });
            });
        }
    }

    @Test
    public void testToUnchecked() {

    }
}
