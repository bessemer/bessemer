package bessemer.cornerstone.test.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import bessemer.cornerstone.domain.Outcome;

public class OutcomeTest {
    @Test
    public void testBuilders() {
        Assertions.assertEquals(Outcome.success(1), Outcome.success(1));
        Assertions.assertEquals(Outcome.error(1), Outcome.error(1));
        Assertions.assertNotEquals(Outcome.success(1), Outcome.error(1));
        Assertions.assertEquals(Outcome.success(null), Outcome.success(null));
        Assertions.assertFalse(Outcome.success(1).isError());
        Assertions.assertTrue(Outcome.error(1).isError());
    }

    @Test
    public void testResolve() {
    }

    @Test
    public void testMap() {

    }
}