package bessemer.spring.jpa

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class BessemerSpringJpaConfiguration {

}