package bessemer.spring.jpa

interface DataSourceReference {
    companion object {
        @JvmField
        val DEFAULT = DefaultDataSourceReference()
    }
}

class DefaultDataSourceReference: DataSourceReference
data class NamedDataSourceReference(val name: String): DataSourceReference

enum class TransactionPropagationType(val integerValue: Int) {
    /**
     * Support a current transaction; create a new one if none exists.
     * Analogous to the EJB transaction attribute of the same name.
     *
     * This is typically the default setting of a transaction definition,
     * and typically defines a transaction synchronization scope.
     */
    REQUIRED(0),

    /**
     * Support a current transaction; execute non-transactionally if none exists.
     * Analogous to the EJB transaction attribute of the same name.
     *
     * **NOTE:** For transaction managers with transaction synchronization,
     * `PROPAGATION_SUPPORTS` is slightly different from no transaction
     * at all, as it defines a transaction scope that synchronization might apply to.
     * As a consequence, the same resources (a JDBC `Connection`, a
     * Hibernate `Session`, etc) will be shared for the entire specified
     * scope. Note that the exact behavior depends on the actual synchronization
     * configuration of the transaction manager!
     *
     * In general, use `PROPAGATION_SUPPORTS` with care! In particular, do
     * not rely on `PROPAGATION_REQUIRED` or `PROPAGATION_REQUIRES_NEW`
     * *within* a `PROPAGATION_SUPPORTS` scope (which may lead to
     * synchronization conflicts at runtime). If such nesting is unavoidable, make sure
     * to configure your transaction manager appropriately (typically switching to
     * "synchronization on actual transaction").
     * @see org.springframework.transaction.support.AbstractPlatformTransactionManager.setTransactionSynchronization
     *
     * @see org.springframework.transaction.support.AbstractPlatformTransactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION
     */
    SUPPORTS(1),

    /**
     * Support a current transaction; throw an exception if no current transaction
     * exists. Analogous to the EJB transaction attribute of the same name.
     *
     * Note that transaction synchronization within a `PROPAGATION_MANDATORY`
     * scope will always be driven by the surrounding transaction.
     */
    MANDATORY(2),

    /**
     * Create a new transaction, suspending the current transaction if one exists.
     * Analogous to the EJB transaction attribute of the same name.
     *
     * **NOTE:** Actual transaction suspension will not work out-of-the-box
     * on all transaction managers. This in particular applies to
     * [org.springframework.transaction.jta.JtaTransactionManager],
     * which requires the `javax.transaction.TransactionManager` to be
     * made available it to it (which is server-specific in standard Java EE).
     *
     * A `PROPAGATION_REQUIRES_NEW` scope always defines its own
     * transaction synchronizations. Existing synchronizations will be suspended
     * and resumed appropriately.
     * @see org.springframework.transaction.jta.JtaTransactionManager.setTransactionManager
     */
    REQUIRES_NEW(3),

    /**
     * Do not support a current transaction; rather always execute non-transactionally.
     * Analogous to the EJB transaction attribute of the same name.
     *
     * **NOTE:** Actual transaction suspension will not work out-of-the-box
     * on all transaction managers. This in particular applies to
     * [org.springframework.transaction.jta.JtaTransactionManager],
     * which requires the `javax.transaction.TransactionManager` to be
     * made available it to it (which is server-specific in standard Java EE).
     *
     * Note that transaction synchronization is *not* available within a
     * `PROPAGATION_NOT_SUPPORTED` scope. Existing synchronizations
     * will be suspended and resumed appropriately.
     * @see org.springframework.transaction.jta.JtaTransactionManager.setTransactionManager
     */
    NOT_SUPPORTED(4),

    /**
     * Do not support a current transaction; throw an exception if a current transaction
     * exists. Analogous to the EJB transaction attribute of the same name.
     *
     * Note that transaction synchronization is *not* available within a
     * `PROPAGATION_NEVER` scope.
     */
    NEVER(5),

    /**
     * Execute within a nested transaction if a current transaction exists,
     * behave like [.PROPAGATION_REQUIRED] otherwise. There is no
     * analogous feature in EJB.
     *
     * **NOTE:** Actual creation of a nested transaction will only work on
     * specific transaction managers. Out of the box, this only applies to the JDBC
     * [org.springframework.jdbc.datasource.DataSourceTransactionManager]
     * when working on a JDBC 3.0 driver. Some JTA providers might support
     * nested transactions as well.
     * @see org.springframework.jdbc.datasource.DataSourceTransactionManager
     */
    NESTED(6)
}

data class TransactionExecutionDetail(
        val dataSource: DataSourceReference = DataSourceReference.DEFAULT,
        val readOnly: Boolean = false,
        val propagationType: TransactionPropagationType = TransactionPropagationType.REQUIRED) {

    companion object {
        @JvmField
        val DEFAULT = TransactionExecutionDetail()

        @JvmField
        val READ_ONLY = TransactionExecutionDetail(readOnly = true)
    }
}