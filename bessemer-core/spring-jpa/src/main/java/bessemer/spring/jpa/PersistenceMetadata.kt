package bessemer.spring.jpa

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import org.springframework.transaction.PlatformTransactionManager
import javax.persistence.EntityManagerFactory

interface PersistenceMetadata {
    fun getEntityManagerFactory(): EntityManagerFactory

    fun getEntityManagerFactory(dataSource: DataSourceReference): EntityManagerFactory
}

@Component
class SimplePersistenceMetadata
    @Autowired
    constructor(
        private val applicationContext: ApplicationContext
    ): PersistenceMetadata {

    override fun getEntityManagerFactory() = getEntityManagerFactory(DataSourceReference.DEFAULT)

    override fun getEntityManagerFactory(dataSource: DataSourceReference): EntityManagerFactory {
        if (DataSourceReference.DEFAULT == dataSource) {
            return applicationContext.getBean(EntityManagerFactory::class.java)
        }
        else if (dataSource is NamedDataSourceReference) {
            val dataSourceName = dataSource.name
            return applicationContext.getBean(dataSourceName, EntityManagerFactory::class.java)
        }
        else {
            throw IllegalArgumentException()
        }
    }
}