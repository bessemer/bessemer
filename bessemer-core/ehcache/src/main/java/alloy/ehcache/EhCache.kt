package alloy.ehcache

import bessemer.cornerstone.collection._Lists
import net.sf.ehcache.Cache
import net.sf.ehcache.CacheManager
import java.util.*

object EhCache {
    @JvmStatic
    @JvmOverloads
    fun <T: Any, N: Any> of(cacheName: String, keyPrefix: String? = null): EhCacheIndex<T, N> = EhCacheIndex(cacheName)

    @JvmStatic
    fun clearAllCaches() {
        clearCaches(_Lists.of(*CacheManager.getInstance().cacheNames))
    }

    @JvmStatic
    fun clearCaches(cacheNames: List<String>) {
        val cm = CacheManager.getInstance()
        for (cacheName in cacheNames) {
            val c = cm.getCache(cacheName)
            if (c != null) {
                c.removeAll()
                c.flush()
            }
        }
    }

    @JvmSynthetic
    fun getCache(cacheName: String): Cache? {
        val cm = CacheManager.getInstance()
        return cm.getCache(cacheName)
    }

    @JvmName("getCache")
    @JvmStatic
    fun getCacheOptional(cacheName: String) = Optional.ofNullable(getCache(cacheName))
}