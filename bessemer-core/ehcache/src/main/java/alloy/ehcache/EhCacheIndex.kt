package alloy.ehcache

import bessemer.cornerstone.data.MutableIndex
import net.sf.ehcache.Element
import java.util.stream.Stream

class EhCacheIndex<T: Any, N: Any>(
        private val cacheName: String,
        private val keyPrefix: String? = null): MutableIndex<T, N> {

    override fun findEach(keys: Stream<T>): Stream<Pair<T, N>> {
        val cache = EhCache.getCache(cacheName)!!

        val results: Stream<Pair<T, N>> = keys.map<Pair<T, N?>> {
            val element = cache.get(this.wrapCacheKeyIfNecessary(it))
            if (element != null) {
                it to element.objectValue as N
            }
            else {
                null
            }
        }.filter {
            it != null
        }.map {
            it as Pair<T, N>
        }

        return results
    }

    override fun put(elements: Stream<Pair<T, N>>) {
        val cache = EhCache.getCache(cacheName)!!
        elements.forEach { cache.put(Element(this.wrapCacheKeyIfNecessary(it.first), it.second)) }
    }

    override fun remove(keys: Stream<T>) {
        val cache = EhCache.getCache(cacheName)!!
        keys.forEach { cache.remove(this.wrapCacheKeyIfNecessary(it)) }
    }

    override fun removeAll() {
        val cache = EhCache.getCache(cacheName)!!
        cache.removeAll()
    }

    private fun wrapCacheKeyIfNecessary(key: T): Any {
        return if(keyPrefix != null) { keyPrefix to key } else { key }
    }
}