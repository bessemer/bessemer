package alloy.search

import bessemer.cornerstone.collection._Lists
import bessemer.cornerstone.collection._Sets
import bessemer.cornerstone.data.Lexicon
import bessemer.cornerstone.domain.Ordering
import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression.ExpressionResolver
import java.math.BigDecimal

data class SearchResultDto<T>(val data: List<T>)

interface SearchResolver: ExpressionResolver {
    fun resolveField(field: SearchField): String = field.key
}

interface FacetDetail {
    val facet: SearchFacet
    val facetDefinition: FacetDefinition
    val active: Boolean
    val activeValues: Set<String>
}

interface FacetDefinition {
    val key: String
    val facetCategory: FacetCategory
}

interface FacetCategory {
    val tags: String
}


interface SomeFacetInterface

data class SearchCriteria(
        val queryExpression: Expression,
        val page: Int,
        val pageSize: Int?,
        val sorts: List<Ordering>,
        val filterExpressions: Set<Expression>,
        val searchFields: Set<SearchQueryField>,
        val facets: Set<FacetDetail>) {

    companion object {
        @JvmStatic
        fun builder() = SearchCriteria.Builder()
    }

    class Builder {
        private lateinit var queryExpression: Expression
        private var page: Int = 0
        private var pageSize: Int? = null
        private var sorts: List<Ordering> = _Lists.empty()
        private var filterExpressions: Set<Expression> = _Sets.empty()
        private var searchFields: Set<SearchQueryField> = _Sets.empty()
        private var facets: Set<FacetDetail> = _Sets.empty()

        fun queryExpression(queryExpression: Expression) = apply { this.queryExpression = queryExpression }
        fun page(page: Int) = apply { this.page = page }
        fun pageSize(pageSize: Int?) = apply { this.pageSize = pageSize }
        fun sorts(sorts: List<Ordering>) = apply { this.sorts = sorts }
        fun filterExpressions(filterExpressions: Set<Expression>) = apply { this.filterExpressions = filterExpressions }
        fun searchFields(searchFields: Set<SearchQueryField>) = apply { this.searchFields = searchFields }
        fun facets(facets: Set<FacetDetail>) = apply { this.facets = facets }

        fun build() = SearchCriteria(
                queryExpression,
                page,
                pageSize,
                sorts,
                filterExpressions,
                searchFields,
                facets
        )
    }
}

interface SearchQueryField {
    val field: SearchField
    val boost: BigDecimal?
}

interface SearchIndexDetail {
    val idField: SearchField

    val namespace: SearchIndexNamespace?
}

data class SearchIndexNamespace(val field: SearchField, val value: String)

data class SearchIndexDetailImpl(
        override val idField: SearchField,
        override val namespace: SearchIndexNamespace?) : SearchIndexDetail

interface SearchService<T: Any> {
    val searchIndexDetail: SearchIndexDetail

    val backingLexicon: Lexicon<Long, T>
}

data class SearchField(val key: String)

interface SearchProvider {
    fun <T: Any> performSearch(searchCriteria: SearchCriteria, searchService: SearchService<T>): SearchResultDto<T>
}

interface SearchQueryService<T> {
    fun search(searchCriteria: SearchCriteria): SearchResultDto<T>
}