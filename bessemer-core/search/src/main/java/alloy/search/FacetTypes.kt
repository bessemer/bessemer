package alloy.search

import bessemer.cornerstone.collection._Sets
import bessemer.cornerstone.expression.Expression

interface SearchFacet

// JOHN - determine SomeFacetInterface
data class SearchQueryFacet(
        val query: Expression,
        val facets: Set<SomeFacetInterface>): SearchFacet

data class SearchBucketFacet(val limit: Int? = null)

enum class SearchRangeFacetBoundingMode {
    LOWER, UPPER, EDGE, OUTER, ALL
}

// JOHN - determine SomeFacetInterface
data class SearchRangeFacet(
        val field: SearchField,
        val start: Int? = null,
        val end: Int? = null,
        val gap: Int,
        val hardEnd: Boolean = true,
        val boundingMode: Set<SearchRangeFacetBoundingMode> = _Sets.of(SearchRangeFacetBoundingMode.LOWER),
        val facets: Set<SomeFacetInterface> = _Sets.empty())

interface SearchFieldFacet: SearchFacet {
    val field: SearchField
}

interface SearchIntervalFacet: SearchFacet {
    val field: SearchField
}
