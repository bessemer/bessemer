package alloy.search.expression

import bessemer.cornerstone.expression.Expression
import bessemer.cornerstone.expression.ExpressionContext
import bessemer.cornerstone.expression.ExpressionField
import java.util.function.BiFunction

object SolrExpressions {
    fun search(search: String): Expression {
        return SearchExpression(search)
    }
}

interface SearchExpressionContext : ExpressionContext {

}