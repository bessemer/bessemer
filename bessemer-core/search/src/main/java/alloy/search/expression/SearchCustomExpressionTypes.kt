package alloy.search.expression

import bessemer.cornerstone.expression.CustomExpression

class SearchExpression(private val query: String) : CustomExpression<SearchExpressionContext> {
    override fun resolve(context: SearchExpressionContext): String {
        return "($query)"
    }
}